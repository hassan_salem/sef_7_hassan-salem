<?php
require_once 'includes/config.php';
require_once 'includes/DB.php';
require_once 'includes/MySQLWrapper.php';
require_once 'includes/Helper.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Rent a DVD</title>

</head>
<body>
	<?php
if (isset($_GET['error'])) {
    echo "<div style='background-color:red; padding:10px; font-size:24px;'>";
    echo $_GET['error'];
    echo "</div>";
}

if (isset($_GET['done'])) {
    echo "<div style='background-color:green; padding:10px; font-size:24px;'>";
    echo "done";
    echo "</div>";
}
?>

<?php
if (isset($_GET['action'])) {
    $action = $_GET['action'];
} else {
    $action = 'rentDVD';
}

switch ($action) {
case 'rentDVD':
    include 'forms/Order.php';
    break;

default:

    break;
}

?>
</body>
</html>