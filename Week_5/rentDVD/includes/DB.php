<?php

/**
 * Database class
 *
 */
class DB {
    // holds the connection.
    private $connection;

    // instance of the db object.
    private static $instance;

    /**
     * return the instance of the connection
     * if not exists create one and return it.
     * @return [type] [description]
     */
    public static function getInstance() {
        if (!self::$instance) {
            // if instance not exist so create instance.
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * the connection to the database
     * will take place at __construct.
     */
    function __construct() {
        // use constants from includes/config.php.
        $this->connection = new mysqli(
            HOST, HOST_USER, HOST_USER_PASSWORD, DATABASE_NAME
        );

        if (mysqli_connect_error()) {
            die('Cannot connect to server : ' . mysqli_connect_error());
        }
    }

    public function __destruct() {
        mysqli_close($this->connection);
    }

    // return the connection.
    function getConnection() {
        return $this->connection;
    }

    /**
     * insert data in the database.
     * first it will construct the query
     * by geting the table name, and values as associative array
     * the key is the column name and the value is the real value.
     * then execute the query.
     * @param  [type] $table  [description]
     * @param  [type] $values [description]
     * @return [type]         [description]
     */
    function insert($table, $values) {
        if (count($values) > 0 && $table) {
            $vals = implode(',', $values);

            // get columns name from the array.
            $cols = implode(',', array_keys($values));
            // building the query.
            $query = "INSERT INTO {$table}({$cols}) VALUES({$vals})";

            if ($this->connection->query($query)) {
                return true;
            } else {
                return false;
            }

        } else {
            return "Table name and/or values is missed.";
        }
    }

    function update($table, $sets, $id) {
        if ($table && $sets && $id) {

            // array holds all sets columns and their values.
            $set = array();
            foreach ($sets as $key => $value) {
                array_push($set, "{$key} = '" . $value . "'");
            }
            $query = "UPDATE {$table} set implode(',', $set)";

            if ($this->connection->query($query)) {
                return true;
            } else {
                return false;
            }

        } else {
            return "Table name, sets and id are required.";
        }
    }

    function delete($table, $where) {
        if ($table && $where) {
            $query = "DELETE FROM {$table} WHERE {$where}";

            if ($this->connection->query($query)) {
                return true;
            } else {
                return false;
            }

        } else {
            return "Table name, sets and id are required.";
        }
    }

    function select($table, $where, $what = ['*']) {
        if ($table) {

            if ($where != '') {
                $where = " WHERE " . $where;
            } else {
                $where = '';
            }
            // array holds all sets columns and their values.

            $query = "SELECT implode(',', {$what} $where)";

            $result = $this->connection->query($query);
            if ($result->num_rows > 0) {
                return $result;
            } else {
                return false;
            }

        } else {
            return "Table name is required";
        }
    }

    function customQuery($query) {
        if ($query) {
            $result = $this->connection->query($query);
            if ($result->num_rows > 0) {
                return $result;
            } else {
                return false;
            }

        } else {
            return "error";
        }
    }

}