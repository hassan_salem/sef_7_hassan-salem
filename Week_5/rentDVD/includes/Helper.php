<?php
/**
 * helper methods
 */
class Helper {

    function __construct() {

    }

    /**
     * convert data table to a select list
     * with a specific name.
     * @param  [type] $table [description]
     * @param  [type] $name  [description]
     * @return [type]        [description]
     */
    public static function tableToSelectList($table, $name) {
        if ($table) {
            $result = "<select name = {$name}>";
            while ($row = $table->fetch_array()) {
                $result .= "<option value={$row[0]}>{$row[1]}</option>";
            }
            $result .= "</select>";
            echo $result;
        } else {
            echo "<select><option>no data</option></select>";
        }

    }

    /**
     * check if data sent is the required data
     * by sending to it the required fileds and the data
     * if there is an error it will return an array
     * contains the error message.
     * @param  [type] $required [description]
     * @param  [type] $data     [description]
     * @return [type]           [description]
     */
    public static function checkIfRequiredSent($required, $data) {
        $notSentData = array_diff($required, array_keys($data));
        if (count($notSentData) > 0) {
            return array(
                'type' => 'error',
                'text' => implode(', ', $notSentData) . 'are required',
            );
        } else {
            foreach ($required as $value) {
                if (!$data[$value]) {
                    return array(
                        'type' => 'error',
                        'text' => $value . ' is required',
                    );
                }
            }
        }
        return array('type' => 'done', 'text' => 'added');
    }

    /**
     * clean all recieved variables.
     */
    public static function cleanDataArray(&$data) {
        foreach ($data as $key => $value) {
            $data[$key] = addslashes($value);
        }
    }
}
?>