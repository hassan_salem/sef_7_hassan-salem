<?php

/**
 * MySQLWrapper is the class that handle
 * all database operation.
 * connection open and close.
 * insert, update, delete and select.
 *
 */
class MySQLWrapper {
    private $connection;

    function __construct() {
        // hold the instance connection in the connection variable.
        //
        $this->connection = DB::getInstance();
    }

    function delete($table, $id) {

    }

    /**
     * return all films that are in the inventory.
     * with their ids.
     * id, title.
     * @return [type] [description]
     */
    function getAllFilms() {
        $query = 'select
        inventory.inventory_id as id,
        film.title
        from film left join inventory on inventory.film_id = film.film_id and store_id=1';

        return $this->connection->customQuery($query);
    }

    /**
     * get all customers from database
     * by concatination of their first and last names.
     * return them as : id, customer_name.
     * @return [type] [description]
     */
    function getAllCustomers() {
        $query = 'select c.customer_id as id, concat_ws(" ", c.first_name, c.last_name) as customer_name from customer as c';
        return $this->connection->customQuery($query);
    }

    /**
     * save the order in rent table.
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    function saveRent($data) {
        // specify the required fields for this insert.
        $requiredData = array('customer_id', 'inventory_id', 'return_date', 'staff_id');

        //die($data['return_date']);
        // helper::checkIfRequiredSent return error if required filed
        // is not sent.
        $done = Helper::checkIfRequiredSent($requiredData, $data);
        if ($done['type'] == 'error') {
            return $done;
        } else {
            // fix date tiem format :
            $data['return_date'] = date(
                'Y-m-d',
                strtotime(str_replace('/', '-', $data['return_date']))
            );
            return $this->connection->insert('rental', $data);
        }

    }

}