<?php
require_once 'includes/config.php';
require_once 'includes/DB.php';
require_once 'includes/MySQLWrapper.php';
require_once 'includes/Helper.php';

// clean the post data.
Helper::cleanDataArray($_POST);

// ceate the wrapper.
$wrapper = new MySQLWrapper();

// save the rental.
$result = $wrapper->saveRent($_POST);

if ($result === true) {
    header("Location:/" . DIRECTORY . "/?done=true");
} else {
    header("Location:/" . DIRECTORY . "/?error=$result[text]");
}
?>