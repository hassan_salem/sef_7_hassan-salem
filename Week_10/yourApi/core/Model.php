<?php
namespace Irradiate {
	class Model {

		/**
		 * creating a new instance of Model with data
		 *
		 * @param [type] $data [description]
		 */
		function __construct($data = null) {
			if ($data) {
				foreach ($data as $key => $value) {
					$this->$key = Helper::clearVar($value);
				}
			}
		}

		/**
		 * connect to mysql database.
		 * @return mysqliObject
		 */
		public static function connect() {

			$mysqli = new \mysqli(
				Config::server(),
				Config::dbUser(),
				Config::dbUserPassword(),
				Config::dbName()
			);

			// error handling.
			if ($mysqli->connect_error) {
				die("Connection failed: " . $mysqli->connect_error);
			} else {
				return $mysqli;
			}
		}

		/**
		 * save the model in database
		 * if it is a new model so insert in database
		 * if it is a model with id so update it.
		 * @return [type] [description]
		 */
		public function save() {
			$query = "";

			/**
			 * if the model has an id
			 * so the save will update the record
			 * if it does not has an id
			 * so it will insert a new record.
			 */
			$idCol = self::getPrimary();
			if (!isset($this->$idCol)) {
				// create insert query
				$query = $this->buildInsertQuery();
			} else {
				// create update query.
				$query = $this->buildUpdateQuery();
			}

			$connection = self::connect();
			$result = $connection->query($query);

			// if there is no id and query is succeed
			// so return the inserted id.
			if (!isset($this->$idCol) && $result) {
				$id = $connection->insert_id;
				$inserted = get_called_class()::find($id);
				echo Helper::toJson($inserted, 200);
				return false;
			}

			if ($result === true) {
				echo Helper::toJson(['code' => 200, 'messages' => 'success'], 200);
				return true;
			} else {
				echo (Helper::toJson(['code' => 300, 'messages' => $result], 300));
				return false;
			}
			$connection->close();

		}

		/**
		 * delete a record from database.
		 * @param  [type] $id [description]
		 * @return [type]     [description]
		 */
		public static function delete($id, $key = null) {
			if (!$key) {
				$key = self::getPrimary();
			}

			$connection = self::connect();
			$query = self::buildDeleteQuery($key, $id);
			$result = $connection->query($query);
			return $connection->affected_rows;
		}

		/**
		 * delete the item with all its depandencies.
		 * @param  [type] $col   [description]
		 * @param  [type] $value [description]
		 * @return [type]        [description]
		 */
		public static function cascadeDelete($col, $value) {
			$connection = self::connect();
			$query = self::buildDeleteQuery($col, $value);
			$constraints = self::getConstraints();

			if ($constraints->num_rows > 0) {
				while ($row = $constraints->fetch_assoc()) {
					$tableName = $row['TABLE_NAME'];
					$columnName = $row['COLUMN_NAME'];
					$classn = 'Irradiate\\' . ucfirst($tableName);
					$classn::delete($value, $columnName);
				}
			}
			$result = $connection->query($query);
			if ($connection->error) {
				echo Helper::toJson(['error_code' => '300', 'message' => $connection->error], 300);
			} else {
				echo Helper::toJson(['affected_rows' => $connection->affected_rows]);
			}
			$connection->close();
		}

		/**
		 * get constraints.
		 * @return [type] [description]
		 */
		public static function getConstraints() {
			$query = "SELECT TABLE_NAME,COLUMN_NAME  FROM information_schema.KEY_COLUMN_USAGE WHERE REFERENCED_TABLE_NAME = '" .
			self::tableName() .
			"' AND REFERENCED_COLUMN_NAME = '" . self::getPrimary() .
			"' AND TABLE_SCHEMA = '" . Config::dbName() . "'";
			$connection = self::connect();
			$result = $connection->query($query);
			$connection->close();
			return $result;
		}

		/**
		 * find item by ID
		 * then return new instance of that item.
		 * @param  [type] $id [description]
		 * @return [type]     [description]
		 */
		public static function find($data, $where = null) {

			if (gettype($data) == 'string' || gettype($data) == 'integer') {
				$query = self::buildSelectQuery(['*'], [self::getPrimary() => $data]);
			} elseif (gettype($data) == 'array') {
				$query = self::buildSelectQuery($data, $where);
				//echo $query;
			} else {
				return;
			}
			$connection = self::connect();
			// build the select query.

			$result = $connection->query($query);

			$model = null;
			$models = array();
			$className = get_called_class();
			if ($result->num_rows > 0) {

				// add the properties to the created object.
				while ($row = $result->fetch_assoc()) {
					$model = new $className($row);
					array_push($models, $model);
				}
			} elseif ($connection->error) {
				echo Helper::toJson(['error_code' => '404', 'message' => $connection->error], 400);
			}
			echo Helper::toJson($models, 200);
			$connection->close();
		}

		/**
		 * return all the data from the data table
		 * as an array of instances.
		 * @return [type] [description]
		 */
		public static function all() {
			$connection = self::connect();
			$query = self::buildSelectQuery(['*']);
			$result = $connection->query($query);
			$model = null;
			$className = get_called_class();
			$models = array();
			if ($result->num_rows > 0) {
				while ($row = $result->fetch_assoc()) {
					$model = new $className($row);
					array_push($models, $model);
				}
				echo Helper::toJson($models, 200);
			} elseif ($result->error) {
				echo Helper::toJson(['error_code' => '400', 'message' => $result->error], 400);
			}
			$connection->close();
		}

		/**
		 * build query for delete
		 * @param  [type] $id [description]
		 * @return [type]     [description]
		 */
		private static function buildDeleteQuery($col, $value) {
			$query = "DELETE FROM " . self::tableName() . " WHERE " . $col . " = '" . $value . "'";
			return $query;
		}

		/**
		 * build query for inset a new record.
		 * @return [type] [description]
		 */
		private function buildInsertQuery() {
			// get the column names from the object.
			$columns = array_keys((array) $this);
			// get the values of each column.
			$values = array_values((array) $this);

			// build the query.
			$query = "INSERT INTO " . self::tableName() . " (" .
			implode(',', $columns) . ") values('" .
			implode('\',\'', $values) . "')";
			return $query;
		}

		/**
		 * build query for update.
		 * @return [type] [description]
		 */
		private function buildUpdateQuery() {
			// get columns names
			$columns = array_keys((array) $this);

			// get the values to be updated
			$sets = array();

			array_map(function ($col) use (&$sets) {
				// remove the id from sets.
				if ($col != self::getPrimary()) {
					array_push($sets, $col . " = '" . $this->$col . "'");
				}
			}, $columns);

			$primaryCol = self::getPrimary();

			// create the query.
			$query = "UPDATE " .
			self::tableName() .
			" SET " .
			implode(',', $sets) .
			" where " .
			self::getPrimary() .
			" = '" .
			$this->$primaryCol . "'";
			return $query;
		}

		/**
		 * build query for select data
		 * @param  array $what  array of what data to select
		 * @param  array $where associative array with conditions
		 * @return string        query as string.
		 */
		private static function buildSelectQuery($what, $where = null) {
			$whereStr = '';
			if ($where) {
				$wheres = array();
				foreach ($where as $key => $value) {
					array_push($wheres, $key . ' = ' . "'" . $value . "'");
				}
				$whereStr = ' WHERE ' . implode(' and ', $wheres);
			}

			$query = "SELECT " . implode(',', $what) . " FROM " . self::tableName() . ' ' . $whereStr;
			return $query;
		}

		public function table($table) {
			$this->config['table'] = $table;
		}

		/**
		 * get the called class short name.
		 * or get the table name from the called model.
		 * @param  [type] $tableName [description]
		 * @return [type]            [description]
		 */
		public static function tableName($tableName = null) {

			if (isset(get_called_class()::$tableName)) {
				return get_called_class()::$tableName;
			} else {
				$reflect = new \ReflectionClass(get_called_class());
				$table = $reflect->getShortName();
				return strtolower($table);
			}
		}

		/**
		 * get the name of the primary key column.
		 * @return [type] [description]
		 */
		public static function getPrimary() {
			if (isset(get_called_class()::$primaryColumn)) {
				return get_called_class()::$primaryColumn;
			} else {
				return self::tableName() . '_id';
			}

		}

	}
}