<?php
require_once 'core/Route.php';
use Irradiate\Route;

/**
 * register the not found called classes.
 */
spl_autoload_register(function ($classname) {
	$cName = $classname;
	//echo $classname . "\n";

	if (count(explode('\\', $classname)) > 0) {
		$cName = explode('\\', $classname)[1];
	}
	if (file_exists('core/' . ($cName) . '.php')) {
		require_once 'core/' . ($cName) . '.php';
	} else if (file_exists('models/' . ($cName) . '.php')) {
		require_once 'models/' . ($cName) . '.php';
	} else if (file_exists(($cName) . '.php')) {
		require_once $cName . '.php';
	} else if (file_exists('controllers/' . Route::version() . '/' . $cName . '.php')) {
		require_once 'controllers/' . Route::version() . '/' . $cName . '.php';
	}
});
