<?php

function createModelContent($className) {
	return '<?php
namespace Irradiate {
	/**
	 * you can change table name by add
	 * public static $tableName = "your table name";
	 */
	class ' . $className . ' extends Model {

		function __construct($data = null) {
			parent::__construct($data);
		}

	}
}';
}

function createControllerContent($className, $class) {
	return '<?php
namespace Irradiate {
	class ' . $className . ' extends Controller {
		function __construct() {

		}

		public function readAll() {
			if (isset($data["fields"])) {
				$fields = explode(",", $data["fields"]);
				$fields = Helper::clearVar($fields);
				' . $class . '::find($fields);
			} else {
				' . $class . '::all();
			}
		}

		public function create($data) {
			$' . $class . ' = new ' . $class . '($data);
			$' . $class . '->save();
		}

		public function update($data) {
			$this->validate($data, ["' . strtolower($class) . '_id" => "required|numeric"]);
			$' . $class . ' = new ' . $class . '($data);
			$' . $class . '->save();
		}

		public function delete($data) {
			$this->validate($data, ["' . strtolower($class) . '_id" => "required|numeric"]);
			' . $class . '::cascadeDelete("' . strtolower($class) . '_id", $data["' . strtolower($class) . '_id"]);
		}
	}
}';
}