<?php
namespace Irradiate {
	/**
	 * helpers methods
	 */
	class Helper {

		function __construct() {

		}

		/**
		 * convert array to json.
		 * @param  array $data array of the data to be converted.
		 * @return json object
		 */
		public static function toJson($data, $responseCode = null) {
			header('Content-Type: application/json');
			if ($responseCode) {
				http_response_code($responseCode);
			}
			return json_encode($data);
		}
		/**
		 * return array item or flase if not exists.
		 * @param  [type] $array [description]
		 * @param  [type] $index [description]
		 * @return [type]        [description]
		 */
		public function getArrayItem($array, $index) {
			if (isset($array[$index])) {
				return $array[$index];
			} else {
				return false;
			}
		}

		/**
		 * escape vars
		 * @param  [type] $var [description]
		 * @return [type]      [description]
		 */
		public static function clearVar($var) {
			if (gettype($var) == 'array') {
				return array_map(function ($item) {
					$item = str_replace('"', '', $item);
					$item = str_replace("'", '', $item);
					// TODO: esape mysql_real_escape...
					return addslashes($item);
				}, $var);
			} elseif (gettype($var) == 'string') {
				$var = str_replace('"', '', $var);
				$var = str_replace("'", '', $var);
				// TODO: esape mysql_real_escape...
				return addslashes($var);
			}
		}

		
		public static function responseError($code, $code1, $message) {
			http_response_code($code);
			return Helper::toJson(['code' => $code1, 'message' => $message]);
		}

	}
}