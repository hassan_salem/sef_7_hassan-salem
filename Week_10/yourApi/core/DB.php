<?php
namespace Irradiate {
	/**
	 * db connection
	 */
	class DB {

		/**
		 * connect to mysql database.
		 * @return mysqliObject
		 */
		public static function connect() {

			$mysqli = new \mysqli(
				Config::server(),
				Config::dbUser(),
				Config::dbUserPassword(),
				Config::dbName()
			);

			// error handling.
			if ($mysqli->connect_error) {
				die("Connection failed: " . $mysqli->connect_error);
			} else {
				return $mysqli;
			}
		}

	} //
}