<?php
namespace Irradiate {
	class Controller {

		//handle the errors as array.
		private $errors = array();

		/**
		 * validate request data.
		 * validate keys are : required, min, max, numeric
		 * validate key separetor is |
		 * validate key value separator is :
		 * @param  associative array $request data sent from request
		 * @param  associative array $rules   rules to set for every key
		 * @return array          array of the errors ocured.
		 */
		public function validate($request, $rules) {
			// fetching all rules.
			foreach ($rules as $key => $value) {

				// get all rules.
				$itemRules = explode('|', $value);

				// traverse all the rules.
				foreach ($itemRules as $itemRule) {

					// check the rules.
					switch ($itemRule) {

					// if the rule title is 'required'
					case 'required':
						if (!isset($request[$key])) {
							array_push($this->errors, [$key => ' required.']);
						}
						break;

					// if the rule title is 'required'
					case 'numeric':
						if (isset($request[$key])) {
							if (!is_numeric($request[$key])) {
								array_push($this->errors, [$key => ' has to be numeric.']);
							}
						}
						break;

					// if the rule is not 'required' nor 'numeric'
					default:

						// if the rule contains ':'
						if (strpos($itemRule, ':') > 0) {

							// rule property.
							$subRule = explode(':', $itemRule)[0];

							// rule value.
							$subRuleValue = explode(':', $itemRule)[1];

							// check if subrule is max or min.
							switch ($subRule) {
							case 'max':
								if (isset($request[$key])) {
									if (strlen($request[$key]) > $subRuleValue) {
										array_push($this->errors, [$key => ' kannot be more than ' . $subRuleValue]);
									}
								}
								break;
							case 'min':
								if (isset($request[$key])) {
									if (strlen($request[$key]) < $subRuleValue) {
										array_push($this->errors, [$key => ' kannot be less than ' . $subRuleValue]);
									}
								}
								break;

							}
						}
						break;

					}
				}
			}
			// if there is any regestered errors. so kill operation with message.
			if (count($this->errors)) {
				http_response_code(403);
				die(Helper::toJson(['code' => 403, 'messages' => $this->errors, 'description' => 'missing arguments']));
			}
			//return $this->errors;
		}
	}
}