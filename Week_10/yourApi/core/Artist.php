<?php
require_once 'core/artistConfig.php';
require_once 'core/Helper.php';
use Irradiate\Helper;

/**
 * Artist to create models, controllers
 * and routes.
 */
class Artist {

	private $controllerDirectory;
	private $modelDirectory;
	private $routeFile;

	/**
	 * set directories, and all command parts
	 * at creation of the instance.
	 * @param string $controllerDirectory [description]
	 * @param string $modelDirectory      [description]
	 * @param string $routeFile           [description]
	 */
	function __construct(
		$argv,
		$controllerDirectory = 'controllers/v1/',
		$modelDirectory = 'models/',
		$routeFile = 'routes.php'
	) {
		$this->controllerDirectory = $controllerDirectory;
		$this->modelDirectory = $modelDirectory;
		$this->routeFile = $routeFile;
		// php artist make:model Model
		// php artist make:controller Controller
		// php artist make:route type path controller ?create

		// get the command from argv array.
		$this->command = Helper::getArrayItem($argv, 1);
		// if command exists.
		if ($this->command !== false) {
			switch ($this->command) {
			// if command is make:model or make:controller, get the name.
			case 'make:model':
			case 'make:controller':
				$this->name = Helper::getArrayItem($argv, 2);
				break;
			case 'make:route':
				$this->type = Helper::getArrayItem($argv, 2);
				$this->path = Helper::getArrayItem($argv, 3);
				$this->controller = Helper::getArrayItem($argv, 4);
				$this->createFiles = Helper::getArrayItem($argv, 5);
				break;
			}
		} else {
			echo 'hi';
		}

	}

	/**
	 * execute the given command.
	 * @param  [type] $argv [description]
	 * @return [type]       [description]
	 */
	public function execute() {
		switch ($this->command) {
		case 'make:model':
			if ($this->name) {
				$this->createModel();
			} else {
				die('Model name is missing.');
			}
			break;
		case 'make:controller':
			if ($this->name) {
				$this->createController();
			} else {
				die('Controller name is missing.');
			}
			break;
		case 'make:route':
			if ($this->type && $this->path && $this->controller) {
				$this->createRoute();
				if ($this->createFiles == 'true') {
					$controller = explode('@', $this->controller)[0];
					$method = explode('@', $this->controller)[1];
					$this->createModel($this->className($controller));
					$this->createController($controller);
				}
			} else {
				die('missing args.');
			}
			break;
		case 'make:allRoutes':

			break;
		}

	}

	/**
	 * method to create a new model.
	 * @param  [type] $xName [description]
	 * @return [type]        [description]
	 */
	private function createModel($name = null) {
		if (!$name) {
			$name = $this->name;
		}
		$content = createModelContent($name);
		$this->createFile($this->modelDirectory . $name . '.php', $content);
	}

	/**
	 * method to create new controller
	 * @param  [type] $xName [description]
	 * @return [type]        [description]
	 */
	private function createController($name) {
		if (!$name) {
			$name = $this->name;
		}
		$content = createControllerContent($name, $this->className($name));
		$this->createFile($this->controllerDirectory . $name . '.php', $content);
	}

	/**
	 * method to create a route.
	 * @return [type] [description]
	 */
	private function createRoute() {
		// php artist make:route get path controller@abc
		if ($this->path[0] != '/') {
			$this->path = '/' . $this->path;
		}

		// build the route string.
		$route = 'Route::' . $this->type . "('" . $this->path . "', '" . $this->controller . "');\n";

		// check if the route is exists.
		if ($this->routeExists($route)) {
			echo "This route is already defined\n";
		} else {
			// append the route string to routes file.
			$this->appendFile($this->routeFile, $route);
		}
	}

	/**
	 * append text to a file.
	 * @param  [type] $file    [description]
	 * @param  [type] $content [description]
	 * @return [type]          [description]
	 */
	public function appendFile($file, $content) {
		if (file_exists($file)) {
			file_put_contents($file, $content, FILE_APPEND);
		}
	}

	/**
	 * create file with content.
	 * or overwrite it if exists.
	 * @param  [type] $file    [description]
	 * @param  [type] $content [description]
	 * @return [type]          [description]
	 */
	public function createFile($file, $content) {
		if (!file_exists($file)) {
			file_put_contents($file, $content);
		} else {
			return false;
		}
	}

	/**
	 * check if the route string is exists in
	 * the routes file.
	 * @param  [type] $route [description]
	 * @return [type]        [description]
	 */
	public function routeExists($route) {
		$justTheRoute = substr($route, 0, strpos($route, ',') + 1);
		if (strpos(file_get_contents($this->routeFile), $justTheRoute) !== false) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * get class name from camel case style word.
	 * @param  [type] $string [description]
	 * @return [type]         [description]
	 */
	public function className($string) {
		$str = preg_replace('/([a-z0-9])?([A-Z])/', '$1:$2', $string);
		return explode(':', $str)[1];
	}
}