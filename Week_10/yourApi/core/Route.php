<?php
namespace Irradiate {

	class Route {
		/**
		 * holds all the routes.
		 * @var array
		 */
		private static $routes = array(
			'GET' => array(),
			'POST' => array(),
			'DELETE' => array(),
			'PATCH' => array(),
			'PUT' => array(),
		);

		/**
		 * execute the routes.
		 * @return [type] [description]
		 */
		public static function apply() {
			$requestMethod = $_SERVER['REQUEST_METHOD'];
			if (!self::allowedVersion()) {
				return null;
			}

			/**
			 * get all the routes for the request method
			 * @var [type]
			 */
			$requestMethodRoutes = self::$routes[$requestMethod];

			if (isset($requestMethodRoutes[self::query()])) {
				// if method of the route is string
				if (gettype($requestMethodRoutes[self::query()]) == 'string') {
					// split the method parts using @ char.
					$methodParts = explode('@', $requestMethodRoutes[self::query()]);
					$class = __namespace__ . '\\' . $methodParts[0];
					$method = $methodParts[1];

					$object = new $class();
					parse_str(file_get_contents("php://input", 'r'), $data);
					if ($requestMethod == 'GET') {
						$object->$method(self::queryParameters());
					} elseif ($requestMethod == 'POST') {
						$object->$method($data);
					} elseif ($requestMethod == 'DELETE') {
						$object->$method($data);
					} elseif ($requestMethod == 'PUT') {
						$object->$method($data);
					}
				} else {
					$requestMethodRoutes[self::query()]();
				}
			}
		}

		/**
		 * return the version given from user
		 * @return [type] [description]
		 */
		public static function version() {
			return self::urlParameters('version');
		}

		/**
		 * return true if the version is allowed.
		 * @return [type] [description]
		 */
		public static function allowedVersion() {
			$version = self::version();
			return in_array($version, Config::allowed_versions());
		}

		/**
		 * return the query from get request.
		 * @return [type] [description]
		 */
		public static function query() {
			return self::urlParameters('query');
		}

		/**
		 * return query parameters.
		 * @return [type] [description]
		 */
		public static function queryParameters() {
			return $_GET;
		}

		/**
		 * add a get route.
		 * @param  [type] $url     [description]
		 * @param  [type] $control [description]
		 * @return [type]          [description]
		 */
		public static function get($url, $control) {
			self::addRequest('GET', $url, $control);
		}

		/**
		 * add a post route.
		 * @param  [type] $url     [description]
		 * @param  [type] $control [description]
		 * @return [type]          [description]
		 */
		public static function post($url, $control) {
			self::addRequest('POST', $url, $control);
		}

		/**
		 * add a put route.
		 * @param  [type] $url     [description]
		 * @param  [type] $control [description]
		 * @return [type]          [description]
		 */
		public static function put($url, $control) {
			self::addRequest('PUT', $url, $control);
		}

		public static function delete($url, $control) {
			self::addRequest('DELETE', $url, $control);
		}

		/**
		 * print the routes.
		 * @return [type] [description]
		 */
		public static function printRoute() {
			var_dump(self::$routes);
		}

		/**
		 * get the parameters from the url
		 * @param  [type] $query [description]
		 * @return [type]        [description]
		 */
		public static function urlParameters($query) {

			if (count($_SERVER['PATH_INFO']) > 0) {
				$urlData = explode('/', $_SERVER['PATH_INFO']);
				switch ($query) {
				case 'version':
					if (count($urlData) > 1) {
						return $urlData[1];
					} else {
						return null;
					}
				case 'query':
					if (count($urlData) > 2) {
						return implode('/', array_splice($urlData, 2));
					} else {
						return null;
					}

				}
			}
		}

		/**
		 * add the request.
		 * @param [type] $type    [description]
		 * @param [type] $url     [description]
		 * @param [type] $control [description]
		 */
		private static function addRequest($type, $url, $control) {
			self::$routes[$type][trim($url, '/')] = $control;
		}

		/**
		 * shit
		 * @param  [type] $delimiters [description]
		 * @param  [type] $string     [description]
		 * @return [type]             [description]
		 */
		private static function multiexplode($delimiters, $string) {

			$ready = str_replace($delimiters, $delimiters[0], $string);
			$launch = explode($delimiters[0], $ready);
			return $launch;
		}

	}
}
