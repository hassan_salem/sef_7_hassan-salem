<div class="col-xs-12 col-sm-9">
    <h6>Reading Model Records</h6>

    <p>
        Read all or part of the data from any table in the selected database.
    </p>

    <h6>Request URL:</h6>

    <p>Use the following url for reading your database records. This method will return a JSON array of all returned rows.</p>
<pre class="prettyprint">GET|POST http://localhost/yourApi/v1/:modelName</pre>
    <p>Use the following url for reading spacific columns in the database.</p>
<pre class="prettyprint">GET|POST http://localhost/yourApi/v1/:modelName?fields=:field1,:field2</pre>
    <br>
    <h6>Required Parameters:</h6>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th style="width: 100px;">Name</th>
                <th style="width: 50px;">Type</th>
                <th>Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>apikey</td>
                <td>string</td>
                <td>Generated by SmsNica and located in your dashboard settings.</td>
            </tr>
            <tr>
                <td>accesstoken</td>
                <td>string</td>
                <td>Generated by SmsNica and located in your dashboard settings.</td>
            </tr>
            <tr>
                <td>num</td>
                <td>numeric</td>
                <td>Page number. If not present default to 1</td>
            </tr>
            <tr>
                <td>id</td>
                <td>numeric</td>
                <td>Message/SMS outbox id. If not present default to 1</td>
            </tr>
            </tbody>
        </table>
    </div>
    <!-- Nav tabs -->
    <h6>Sample Snippets:</h6>
    <ul class="nav nav-tabs">
        <li class="active"><a href="#php" data-toggle="tab">PHP</a></li>
        <li><a href="#jquery" data-toggle="tab">jQuery.ajax()</a></li>
        <li><a href="#getjson" data-toggle="tab">jQuery.getJSON()</a></li>
        <li><a href="#node" data-toggle="tab">NodeJs</a></li>
        <li><a href="#ruby" data-toggle="tab">Ruby</a></li>
    </ul>
    <br>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="php">
<pre class="prettyprint linenums">
// Get cURL resource
$curl = curl_init();
// Set some options
curl_setopt_array($curl, array(
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_SSL_VERIFYPEER => 0,
    CURLOPT_URL => 'https://www.smsnica.com/api/v1/read/outbox/page/:num',
    CURLOPT_POST => 1,
    CURLOPT_POSTFIELDS => array(
        'accesstoken' => {{accesstoken}},
        'apikey' => {{apikey}},
        'num' => {{num}},
    )
));
// Send the request & save response to $resp
$resp = curl_exec($curl);
// Close request to clear up some resources
curl_close($curl);
</pre>
        </div>
        <div class="tab-pane" id="jquery">
<pre class="prettyprint lang-js linenums">
(function($){
    $.ajax({
        type: 'POST',
        url: 'https://www.smsnica.com/api/v1/read/outbox/message/:id',
        dataType: 'json',
        data: {
            accesstoken: "{{accesstoken}}",
            apikey: "{{apikey}}",
            id: "{{id}}",
        },
        success: function(data){
            console.log(data);
        },
        complete: function(){
            //Implement your logic
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log(errorThrown);
        }
    });
})(jQuery);
</pre>
        </div>
        <div class="tab-pane" id="getjson">
<pre class="prettyprint lang-js linenums">
    (function($){
        $.getJSON( 'https://www.smsnica.com/api/v1/read/outbox/page/:num', {
            accesstoken: "{{accesstoken}}",
            apikey: "{{apikey}}",
            num: "{{num}}",
        })
            .done(function( data ) {
                console.log(data);
            });
    })(jQuery);
</pre>
        </div>
        <div class="tab-pane" id="node">
<pre class="prettyprint lang-js linenums">
var request = require('request'); // https://www.npmjs.org/package/request

var data = {
    url: 'https://www.smsnica.com/api/v1/read/outbox/message/{{id}}',
    form: {
        apikey: '{{apikey}}',
        accesstoken: '{{accesstoken}}'
    }
};

request.post(data, function (err, res, body) {
    console.log('body:', body);
});</pre>
        </div>
        <div class="tab-pane" id="ruby">
<pre class="prettyprint lang-rb linenums">
require "net/https"
require "uri"

uri = URI.parse('https://www.smsnica.com/api/v1/read/outbox/page/:num')

http = Net::HTTP.new(uri.host, uri.port)

request = Net::HTTP::Post.new(uri.request_uri)
request.set_form_data(
        accesstoken => "{{accesstoken}}",
        apikey => "{{apikey}}"
)

response = http.request(request)
</pre>
        </div>
    </div>
    <br>
    <h6>Single Sample Response:</h6>
<pre class="prettyprint linenums">
{
    "transactionStatus": 1,
    "response": "Message sent",
    "processId": "MjEzMA==",
    "number": "50583240355",
    "message": "Hola, cuales son sus horarios de atención al cliente?",
    "carrier": "movistar",
    "credits": 999,
    "dateTime": {
        "date": "<?php echo date('d/m/Y g:i A'); ?>",
        "timezone_type": 3,
        "timezone": "America/Managua"
    },
    "code": 200
}
</pre>
    <br>
    <h6>Collection Sample Response:</h6>
<pre class="prettyprint linenums">
{
    "transactionStatus":1,
    "messages":{
        "0":{
            "id":401,
            "number":"50583240355",
            "message":"Envía #dulce y recibiras un postre gratis.",
            "carrier":"movistar",
            "dateTime":{
                "date":"<?php echo date('d/m/Y g:i A'); ?>",
                "timezone_type":3,
                "timezone":"America\/Managua"
            },
            "status":"0"
        },
        "1":{
            "id":400,
            "number":"50583240355",
            "message":"Donde será el bacanal esta noche?",
            "carrier":"movistar",
            "dateTime":{
                "date":"<?php echo date('d/m/Y g:i A'); ?>",
                "timezone_type":3,
                "timezone":"America\/Managua"
            },
            "status":"1"
        },
        "2":{
            "id":399,
            "number":"50583240355",
            "message":"Les ofrecemlos sandalias brasileñas y accesorios de mano.",
            "carrier":"unknown",
            "dateTime":{
                "date":"<?php echo date('d/m/Y g:i A'); ?>",
                "timezone_type":3,
                "timezone":"America\/Managua"
            },
            "status":"1"
        },
        "3":{
            "id":398,
            "number":"50583240355",
            "message":"Cuando empiezan las clases? Me gustaríia saber el horario.",
            "carrier":"claro",
            "dateTime":{
                "date":"<?php echo date('d/m/Y g:i A'); ?>",
                "timezone_type":3,
                "timezone":"America\/Managua"
            },
            "status":"0"
        },
        "4":{
            "id":397,
            "number":"50583240355",
            "message":"A que hora cierran? Quisiera visitar su negocio.",
            "carrier":"claro",
            "dateTime":{
                "date":"<?php echo date('d/m/Y g:i A'); ?>",
                "timezone_type":3,
                "timezone":"America\/Managua"
            },
            "status":"0"
        },
        "5":{
            "id":396,
            "number":"50583240355",
            "message":"?",
            "carrier":"La reunión sera el día de mañana, a las 2:00 pm. Por favor ser puntual. ",
            "dateTime":{
                "date":"<?php echo date('d/m/Y g:i A'); ?>",
                "timezone_type":3,
                "timezone":"America\/Managua"
            },
            "status":"0"
        },
        "6":{
            "id":395,
            "number":"50583240355",
            "message":"Este fin de semana promoción en platos fuertes al 2x1. Aprovechen esta gran promoción.",
            "carrier":"movistar",
            "dateTime":{
                "date":"<?php echo date('d/m/Y g:i A'); ?>",
                "timezone_type":3,
                "timezone":"America\/Managua"
            },
            "status":"0"
        },
        "7":{
            "id":394,
            "number":"50583240355",
            "message":"Nueva colección de trajes de baño para damas, descuento por compras de $50 a mas.",
            "carrier":"movistar",
            "dateTime":{
                "date":"<?php echo date('d/m/Y g:i A'); ?>",
                "timezone_type":3,
                "timezone":"America\/Managua"
            },
            "status":"0"
        },
        "8":{
            "id":393,
            "number":"50583240355",
            "message":"FAV26C",
            "carrier":"Hoy se estará regalando tragos a las primeras 100 personas que lleguen antes de las 10 pm. ",
            "dateTime":{
                "date":"<?php echo date('d/m/Y g:i A'); ?>",
                "timezone_type":3,
                "timezone":"America\/Managua"
            },
            "status":"0"
        },
        "9":{
            "id":392,
            "number":"50583240355",
            "message":"Clases de baile par adultos, a las personas que se inscriban una semana antes les daremos 10% de descuento.",
            "carrier":"unknown",
            "dateTime":{
                "date":"<?php echo date('d/m/Y g:i A'); ?>",
                "timezone_type":3,
                "timezone":"America\/Managua"
            },
            "status":"1"
        }
    },
    "paginator":{
        "pageCount":24,
        "itemCountPerPage":10,
        "first":1,
        "current":2,
        "last":24,
        "previous":1,
        "next":3,
        "pagesInRange":{
            "1":1,
            "2":2,
            "3":3,
            "4":4,
            "5":5,
            "6":6,
            "7":7,
            "8":8,
            "9":9,
            "10":10
        },
        "firstPageInRange":1,
        "lastPageInRange":10,
        "currentItemCount":10,
        "totalItemCount":240,
        "firstItemNumber":11,
        "lastItemNumber":20
    },
    "code":200
}
</pre>
    <br>

    </div><!-- /.col-xs-12 main -->