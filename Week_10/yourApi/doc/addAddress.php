<div class="col-xs-12 col-sm-9">
    <h6>Adding records to address</h6>

    <p>

    </p>

    <h6>Request URL:</h6>

    <p>Use the following url for adding new address.</p>
<pre class="prettyprint">POST http://localhost/yourApi/v1/Address</pre>

<br>
    <h6>Required Parameters:</h6>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th style="width: 100px;">Name</th>
                <th style="width: 50px;">Type</th>
                <th>Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>address</td>
                <td>string</td>
                <td>address string.</td>
            </tr>
            <tr>
                <td>district</td>
                <td>string</td>
                <td></td>
            </tr>
            <tr>
                <td>city_id</td>
                <td>int</td>
                <td>id of the city.</td>
            </tr>
            <tr>
                <td>phone</td>
                <td>string</td>
                <td>phone number.</td>
            </tr>
            </tbody>
        </table>
    </div>
    <br>

    <h6>Optional Parameters:</h6>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th style="width: 100px;">Name</th>
                <th style="width: 50px;">Type</th>
                <th>Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>address2</td>
                <td>string</td>
                <td>address 2.</td>
            </tr>
            <tr>
                <td>postal_code</td>
                <td>string</td>
                <td>postal code</td>
            </tr>

            </tbody>
        </table>
    </div>
    <br>

    <h6>Sample Response: with response code 200</h6>
    <p>return the added customer</p>
<pre class="prettyprint linenums">
  {
    "address_id": "1",
    "address": "47 MySakila Drive",
    "address2": null,
    "district": "Alberta",
    "city_id": "300",
    "postal_code": "",
    "phone": "",
    "last_update": "2014-09-25 22:30:27"
  }
</pre>
    <br>

    <h6>Sample Response: with response code 400</h6>
<pre class="prettyprint linenums">
  {
    "error_code": "401",
    "error":"model not found"
  }
</pre>
    <br>

    </div><!-- /.col-xs-12 main -->