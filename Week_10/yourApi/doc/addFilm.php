<div class="col-xs-12 col-sm-9">
    <h6>Adding records to film</h6>

    <p>

    </p>

    <h6>Request URL:</h6>

    <p>Use the following url for adding new film.</p>
<pre class="prettyprint">POST http://localhost/yourApi/v1/film</pre>

<br>
    <h6>Required Parameters:</h6>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th style="width: 100px;">Name</th>
                <th style="width: 50px;">Type</th>
                <th>Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>title</td>
                <td>string</td>
                <td>film title.</td>
            </tr>
            <tr>
                <td>language_id</td>
                <td>int</td>
                <td>id of the film language from language table.</td>
            </tr>
            </tbody>
        </table>
    </div>
    <br>

    <h6>Optional Parameters:</h6>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th style="width: 100px;">Name</th>
                <th style="width: 50px;">Type</th>
                <th>Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>description</td>
                <td>text</td>
                <td>description of the movie</td>
            </tr>
            <tr>
                <td>release_year</td>
                <td>year(4)</td>
                <td>postal code</td>
            </tr>
            <tr>
                <td>original_language_id</td>
                <td>int</td>
                <td>language id from languages</td>
            </tr>
            <tr>
                <td>rental_duration</td>
                <td>int</td>
                <td>default 3</td>
            </tr>
            <tr>
                <td>rental_rate</td>
                <td>decimal(4,2)</td>
                <td></td>
            </tr>
            <tr>
                <td>length</td>
                <td>int</td>
                <td></td>
            </tr>
            <tr>
                <td>rating</td>
                <td>enum('G', 'PG', 'PG-13', 'R', 'NC-17')</td>
                <td></td>
            </tr>
            <tr>
                <td>special_features</td>
                <td>set('Trailers', 'Commentaries', 'Deleted Scenes',</td>
                <td></td>
            </tr>


            </tbody>
        </table>
    </div>
    <br>

    <h6>Sample Response: with response code 200</h6>
    <p>return the added film</p>
<pre class="prettyprint linenums">
  {
  "film_id": "1",
  "title": "ACADEMY DINOSAUR",
  "description": "A Epic Drama of a Feminist And a Mad Scientist who must Battle a Teacher in The Canadian Rockies",
  "release_year": "2006",
  "language_id": "1",
  "original_language_id": null,
  "rental_duration": "6",
  "rental_rate": "0.99",
  "length": "86",
  "replacement_cost": "20.99",
  "rating": "PG",
  "special_features": "Deleted Scenes,Behind the Scenes",
  "last_update": "2006-02-15 05:03:42"
}
</pre>
    <br>

    <h6>Sample Response: with response code 400</h6>
<pre class="prettyprint linenums">
  {
    "error_code": "401",
    "error":"model not found"
  }
</pre>
    <br>

    </div><!-- /.col-xs-12 main -->