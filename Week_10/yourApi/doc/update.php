<div class="col-xs-12 col-sm-9">
    <h6>Updating models</h6>

    <p>

    </p>

    <h6>Request URL:</h6>

    <p>Use the following url for updating models.</p>
<pre class="prettyprint">PUT http://localhost/yourApi/v1/:modelName</pre>

<br>
    <h6>Required Parameters:</h6>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th style="width: 100px;">Name</th>
                <th style="width: 50px;">Type</th>
                <th>Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>id</td>
                <td>int</td>
                <td>id of the row you want to update.</td>
            </tr>
            </tbody>
        </table>
    </div>
    <br>

<br>
    <h6>Optional Parameters:</h6>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th style="width: 100px;">Name</th>
                <th style="width: 50px;">Type</th>
                <th>Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>column name</td>
                <td>string</td>
                <td>data you want to update.</td>
            </tr>
            </tbody>
        </table>
    </div>
    <br>


    <h6>Sample Response: with response code 200</h6>
    <p>return the added actor</p>
<pre class="prettyprint linenums">
{
  "affected_rows":1
}
</pre>
    <br>

    <h6>Sample Response: with response code 400</h6>
<pre class="prettyprint linenums">
  {
    "error_code": "401",
    "error":"model not found"
  }
</pre>
    <br>

    </div><!-- /.col-xs-12 main -->