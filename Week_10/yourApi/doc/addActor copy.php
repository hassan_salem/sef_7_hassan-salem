<div class="col-xs-12 col-sm-9">
    <h6>Adding records to actor</h6>

    <p>

    </p>

    <h6>Request URL:</h6>

    <p>Use the following url for adding new actor.</p>
<pre class="prettyprint">POST http://localhost/yourApi/v1/actor</pre>

<br>
    <h6>Required Parameters:</h6>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th style="width: 100px;">Name</th>
                <th style="width: 50px;">Type</th>
                <th>Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>first_name</td>
                <td>string</td>
                <td>actor first name.</td>
            </tr>
            <tr>
                <td>last_name</td>
                <td>string</td>
                <td>actor last name.</td>
            </tr>
            </tbody>
        </table>
    </div>
    <br>


    <h6>Sample Response: with response code 200</h6>
    <p>return the added actor</p>
<pre class="prettyprint linenums">
{
  "actor_id": "1",
  "first_name": "mohammad",
  "last_name": "zein",
  "last_update": "2016-05-20 16:00:58"
}
</pre>
    <br>

    <h6>Sample Response: with response code 400</h6>
<pre class="prettyprint linenums">
  {
    "error_code": "401",
    "error":"model not found"
  }
</pre>
    <br>

    </div><!-- /.col-xs-12 main -->