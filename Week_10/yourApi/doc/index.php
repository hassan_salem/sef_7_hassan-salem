<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Place favicon.ico and apple-touch-icon(s) in the root directory -->

    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="Flat-UI-master/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="Flat-UI-master/css/flat-ui.css">
    <link rel="stylesheet" href="css/prettify.css">
    <link rel="stylesheet" href="css/main.css">
    <script src="js/vendor/modernizr-2.8.0.min.js"></script>
    <link href="/img/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon">
    <noscript>
        <style type="text/css">
            .prettyprint{
                color: #FFFFFF;
                padding-left:60px
            }
        </style>
    </noscript>
</head>
<body>
<div class="page-container">

<!-- top navbar -->
<div class="navbar navbar-default" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="offcanvas" data-target=".sidebar-nav">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">

            </a>
        </div>
    </div>
</div>

<div class="container">
<!--[if lt IE 8]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<h3 class="hidden-xs">YourAPI Documentation</h3>
<hr>
<!-- sidebar -->
<div class="row row-offcanvas row-offcanvas-left">
    <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
        <ul class="nav">
            <li><a href=index.php>Introduction</a></li>
            <li><a href="?p=read">Read</a></li>
            <li><a href="?p=addCustomer">add customer</a></li>
            <li><a href="?p=addAddress">add Address</a></li>
            <li><a href="?p=addFilm">add film</a></li>
            <li><a href="?p=addActor">add actor</a></li>
            <li><a href="?p=deleteActor">delete actor</a></li>
            <li><a href="?p=deleteAddress">delete address</a></li>
            <li><a href="?p=deleteCustomer">delete customer</a></li>
            <li><a href="?p=deleteFilm">delete film</a></li>
            <li><a href="?p=deletePayment">delete payment</a></li>
            <li><a href="?p=deleteRental">delete rental</a></li>
            <li><a href="?p=deleteStaff">delete staff</a></li>
            <li><a href="?p=deleteStore">delete store</a></li>
            <li><a href="?p=update">Update</a></li>
        </ul>
    </div>
    <!-- main area -->
<?php
if (isset($_GET['p'])) {
	switch ($_GET['p']) {
	case 'read':
		include 'read.php';
		break;
	case 'addCustomer':
		include 'addCustomer.php';
		break;
	case 'addAddress':
		include 'addAddress.php';
		break;
	case 'addFilm':
		include 'addFilm.php';
		break;
	case 'addActor':
		include 'addActor.php';
		break;
	case 'deleteActor':
		include 'deleteActor.php';
		break;
	case 'deleteAddress':
		include 'deleteAddress.php';
		break;
	case 'deleteCustomer':
		include 'deleteCustomer.php';
		break;
	case 'deleteFilm':
		include 'deleteFilm.php';
		break;
	case 'deletePayment':
		include 'deletePayment.php';
		break;
	case 'deleteRental':
		include 'deleteRental.php';
		break;
	case 'deleteStaff':
		include 'deleteStaff.php';
		break;
	case 'deleteStore':
		include 'deleteStore.php';
		break;
	case 'update':
		include 'update.php';
		break;

	}
}
?>
</div>

</div><!--/.page-container-->

<script src="Flat-UI-master/js/jquery-1.8.3.min.js"></script>
<script src="Flat-UI-master/js/bootstrap.min.js"></script>
<script src="Flat-UI-master/bootstrap/js/google-code-prettify/prettify.js"></script>
<script src="js/plugins.js"></script>
<script src="js/main.js"></script>
<script type="text/javascript">
    //<!--
    prettyPrint();
    //-->
</script>
<script type="text/javascript">
    //<!--
    $(document).ready(function() { $('[data-toggle=offcanvas]').click(function() { $('.row-offcanvas').toggleClass('active');});});
    //-->
</script>
<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
     function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
     e=o.createElement(i);r=o.getElementsByTagName(i)[0];
     e.src='//www.google-analytics.com/analytics.js';
     r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
     ga('create','UA-XXXXX-X');ga('send','pageview');
</script>
</body>
</html>
