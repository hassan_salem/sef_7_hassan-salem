<div class="col-xs-12 col-sm-9">
    <h6>Deleting payments</h6>

    <p>

    </p>

    <h6>Request URL:</h6>

    <p>Use the following url for delete rental.</p>
<pre class="prettyprint">DELETE http://localhost/yourApi/v1/payment</pre>

<br>
    <h6>Required Parameters:</h6>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th style="width: 100px;">Name</th>
                <th style="width: 50px;">Type</th>
                <th>Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>rental_id</td>
                <td>int</td>
                <td>id of the payment to delete.</td>
            </tr>
            </tbody>
        </table>
    </div>
    <br>


    <h6>Sample Response: with response code 200</h6>
    <p>return the number of deleted rows</p>
<pre class="prettyprint linenums">
{
  "affected_rows": "1"
}
</pre>
    <br>

    <h6>Sample Response: with response code 400</h6>
<pre class="prettyprint linenums">
  {
    "error_code": "401",
    "error":"cannot delete"
  }
</pre>
    <br>

    </div><!-- /.col-xs-12 main -->