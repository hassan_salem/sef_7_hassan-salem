<div class="col-xs-12 col-sm-9">
    <h6>Adding records to customer</h6>

    <p>

    </p>

    <h6>Request URL:</h6>

    <p>Use the following url for adding new customers.</p>
<pre class="prettyprint">POST http://localhost/yourApi/v1/customer</pre>

<br>
    <h6>Required Parameters:</h6>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th style="width: 100px;">Name</th>
                <th style="width: 50px;">Type</th>
                <th>Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>store_id</td>
                <td>int</td>
                <td>store id from store table.</td>
            </tr>
            <tr>
                <td>first_name</td>
                <td>string</td>
                <td>customer first name</td>
            </tr>
            <tr>
                <td>last_name</td>
                <td>string</td>
                <td>customer last name.</td>
            </tr>
            <tr>
                <td>address_id</td>
                <td>int</td>
                <td>address id from address table.</td>
            </tr>
            <tr>
                <td>last_name</td>
                <td>string</td>
                <td>customer last name.</td>
            </tr>
            </tbody>
        </table>
    </div>
    <br>

    <h6>Optional Parameters:</h6>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th style="width: 100px;">Name</th>
                <th style="width: 50px;">Type</th>
                <th>Description</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>email</td>
                <td>string</td>
                <td>customer email address.</td>
            </tr>
            <tr>
                <td>active</td>
                <td>int</td>
                <td>customer status</td>
            </tr>

            </tbody>
        </table>
    </div>
    <br>

    <h6>Sample Response: with response code 200</h6>
    <p>return the added customer</p>
<pre class="prettyprint linenums">
  {
    "customer_id": "1",
    "store_id": "1",
    "first_name": "MARY",
    "last_name": "SMITH",
    "email": "MARY.SMITH@sakilacustomer.org",
    "address_id": "5",
    "active": "1",
    "create_date": "2006-02-14 22:04:36",
    "last_update": "2006-02-15 04:57:20"
  }
</pre>
    <br>

    <h6>Sample Response: with response code 400</h6>
<pre class="prettyprint linenums">
  {
    "error_code": "401",
    "error":"model not found"
  }
</pre>
    <br>

    </div><!-- /.col-xs-12 main -->