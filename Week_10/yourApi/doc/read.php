<div class="col-xs-12 col-sm-9">
    <h6>Reading Model Records</h6>

    <p>
        Read all or part of the data from any table in the selected database.
    </p>

    <h6>Request URL:</h6>

    <p>Use the following url for reading your database records. This method will return a JSON array of all returned rows.</p>
<pre class="prettyprint">GET http://localhost/yourApi/v1/:modelName</pre>
    <p>Use the following url for reading spacific columns in the database.</p>
<pre class="prettyprint">GET http://localhost/yourApi/v1/:modelName?fields=:field1,:field2</pre>
    <br>

    <h6>Sample Response: with response code 200</h6>
<pre class="prettyprint linenums">
[
  {
    "customer_id": "1",
    "store_id": "1",
    "first_name": "MARY",
    "last_name": "SMITH",
    "email": "MARY.SMITH@sakilacustomer.org",
    "address_id": "5",
    "active": "1",
    "create_date": "2006-02-14 22:04:36",
    "last_update": "2006-02-15 04:57:20"
  }
]
</pre>
    <br>

    <h6>Sample Response: with response code 400</h6>
<pre class="prettyprint linenums">
  {
    "error_code": "401",
    "error":"model not found"
  }
</pre>
    <br>

    </div><!-- /.col-xs-12 main -->