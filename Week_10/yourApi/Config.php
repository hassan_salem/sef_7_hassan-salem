<?php
namespace Irradiate {
	/**
	 * holds all the configuration variables for the project
	 * in array named $vars.
	 */
	class Config {
		public static $vars = array(
			/**
			 * holds the directory of the project.
			 */
			'Directory' => 'yourQuery',

			/**
			 * server name.
			 */
			'server' => 'localhost',

			/**
			 * database user name.
			 */
			'dbUser' => 'root',

			/**
			 * database user password.
			 */
			'dbUserPassword' => 'root',

			/**
			 * database name
			 */
			'dbName' => 'sakila',

			/**
			 * allowed versions
			 * so we have to add every version name
			 * to this array.
			 */
			'allowed_versions' => array(
				'v1', 'v2', 'v3',
			),

			/**
			 * current stable version.
			 */
			'current_version' => 'v1',
		);

		/**
		 * call for all settings variables.
		 * using magic method call.
		 * @param  [type] $name [description]
		 * @param  [type] $args [description]
		 * @return [type]       [description]
		 */
		public static function __callStatic($name, $args) {
			if (isset(self::$vars[$name])) {
				return self::$vars[$name];
			} else {
				return null;
			}
		}
	}
}