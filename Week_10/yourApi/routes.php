<?php
use Irradiate\Route;

Route::get('/', function () {});
Route::get('/customer', 'CustomerController@readAll');
Route::get('/address', 'AddressController@readAll');
Route::get('/film', 'FilmController@readAll');
Route::get('/actor', 'ActorController@readAll');
Route::delete('/customer', 'CustomerController@delete');
Route::post('/actor', 'ActorController@create');
Route::delete('/actor', 'ActorController@delete');
Route::get('/category', 'CategoryController@readAll');
Route::delete('/category', 'CategoryController@delete');
Route::post('/category', 'CategoryController@create');
Route::put('/category', 'CategoryController@update');
