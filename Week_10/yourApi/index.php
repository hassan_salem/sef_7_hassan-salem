<?php
use Irradiate\Route;
require_once 'core/autoload.php';
require_once 'routes.php';

Route::apply();