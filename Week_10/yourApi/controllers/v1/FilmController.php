<?php
namespace Irradiate {
	class FilmController extends Controller {
		function __construct() {

		}

		public function readAll() {
			echo Helper::toJson(Film::all());
		}

		public function create($data) {
			$Film = new Film($data);
			echo $Film->save();
		}

		public function update($data) {
			$this->validate($data, ["film_id" => "required|numeric"]);
			$Film = new Film($data);
			echo $Film->save();
		}

		public function delete($data) {
			$this->validate($data, ["film_id" => "required|numeric"]);
			Film::cascadeDelete("film_id", $data["film_id"]);
		}
	}
}