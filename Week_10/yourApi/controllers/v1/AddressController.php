<?php
namespace Irradiate {
	class AddressController extends Controller {
		function __construct() {

		}

		public function readAll() {
			echo Helper::toJson(Address::all());
		}

		public function create($data) {
			$Address = new Address($data);
			echo $Address->save();
		}

		public function update($data) {
			$this->validate($data, ["address_id" => "required|numeric"]);
			$Address = new Address($data);
			echo $Address->save();
		}

		public function delete($data) {
			$this->validate($data, ["address_id" => "required|numeric"]);
			Address::cascadeDelete("address_id", $data["address_id"]);
		}
	}
}