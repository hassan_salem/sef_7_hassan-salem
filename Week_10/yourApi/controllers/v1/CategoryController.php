<?php
namespace Irradiate {
	class CategoryController extends Controller {
		function __construct() {

		}

		public function readAll() {
			if (isset($data["fields"])) {
				$fields = explode(",", $data["fields"]);
				$fields = Helper::clearVar($fields);
				Category::find($fields);
			} else {
				Category::all();
			}
		}

		public function create($data) {
			$Category = new Category($data);
			$Category->save();
		}

		public function update($data) {
			$this->validate($data, ["category_id" => "required|numeric"]);
			$Category = new Category($data);
			$Category->save();
		}

		public function delete($data) {
			$this->validate($data, ["category_id" => "required|numeric"]);
			Category::cascadeDelete("category_id", $data["category_id"]);
		}
	}
}