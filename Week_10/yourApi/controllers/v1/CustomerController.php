<?php
namespace Irradiate {
	class CustomerController extends Controller {
		function __construct() {

		}

		public function readAll($data) {
			if (isset($data['fields'])) {
				$fields = explode(',', $data['fields']);
				$fields = Helper::clearVar($fields);
				Customer::find($fields);
			} else {
				Customer::all();
			}

		}

		public function create($data) {
			$Customer = new Customer($data);
			$Customer->save();
		}

		public function update($data) {
			$this->validate($data, ["customer_id" => "required|numeric"]);
			$Customer = new Customer($data);
			$Customer->save();
		}

		public function delete($data) {
			$this->validate($data, ["customer_id" => "required|numeric"]);
			Customer::cascadeDelete("customer_id", $data["customer_id"]);
		}
	}
}