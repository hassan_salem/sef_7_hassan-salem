<?php
namespace Irradiate {
	class ActorController extends Controller {
		function __construct() {

		}

		public function readAll() {
			echo Helper::toJson(Actor::all());
		}

		public function create($data) {
			$Actor = new Actor($data);
			echo $Actor->save();
		}

		public function update($data) {
			$this->validate($data, ["actor_id" => "required|numeric"]);
			$Actor = new Actor($data);
			echo $Actor->save();
		}

		public function delete($data) {
			$this->validate($data, ["actor_id" => "required|numeric"]);
			Actor::cascadeDelete("actor_id", $data["actor_id"]);
		}
	}
}