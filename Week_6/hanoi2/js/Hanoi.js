/**
 * Hanoi is the class that handle the pattern.
 * and the game size.
 */
function Hanoi(numberOfDisks = 3) {
    this.numberOfDisks = numberOfDisks;
    this.stepCounter = 1;
    this.steps = 0;
}

/**
 * create the towers, then fill them with disks.
 * also it will create the disks as divs and fill
 * the first tower with them.
 * @return {void}
 */
Hanoi.prototype.initGame = function(firstTower) {
    var tower = new Tower();
    tower.fillTower(this.numberOfDisks);
    this.tower = tower;

    var htmlDisks = '';
    for (var i = 1; i <= this.numberOfDisks; i++) {
        htmlDisks += '<div class="r r' + i + '">' + i + '</div>';
    }

    // reset all towers.
    document.getElementById('B').innerHTML = '&nbsp;';
    document.getElementById('C').innerHTML = '&nbsp;';

    firstTower.innerHTML = htmlDisks + '&nbsp;';
}

/**
 * returns the number of steps taken by the game to be solved.
 * @return {[type]} [description]
 */
Hanoi.prototype.getSteps = function() {
    return this.steps;
}

/**
 * apply the pattern one by one.
 * and increments the steps by one.
 * @return {int} number of steps taken by the script to solve the problem.
 */
Hanoi.prototype.step = function() {

    var result = true;
    if (this.stepCounter == 1) {
        result = this.tower.bidirectionalMove(
            this.tower.labels.A,
            this.tower.labels.C
        );
        this.stepCounter++;
    } else if (this.stepCounter == 2) {
        result = this.tower.bidirectionalMove(
            this.tower.labels.A,
            this.tower.labels.B
        );
        this.stepCounter++;
    } else {
        result = this.tower.bidirectionalMove(
            this.tower.labels.B,
            this.tower.labels.C
        );
        this.stepCounter = 1;
    }
    if (result != false) {
        this.steps++;
    }
    return result;
}