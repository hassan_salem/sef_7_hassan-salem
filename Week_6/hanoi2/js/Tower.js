/**
 * initialize the tower.
 * Add labels for tower indexes.
 * @return {[void]} [description]
 */
function Tower() {

    this.labels = {
        A: 0,
        B: 1,
        C: 2
    };
}

/**
 * Filling the Tower with disks according to the
 * numberOfDisks supplied.
 * also swap tower C and tower B if numberOfDisks
 * is an even number.
 * @param  {[int]} numberOfDisks [number of disks to fill the initial tower with]
 * @return {[void]}               [description]
 */
Tower.prototype.fillTower = function(numberOfDisks) {
    this.numberOfDisks = numberOfDisks;

    // check if number of disks is even so swap C and B
    if (numberOfDisks % 2 == 0) {
        this.labels.B = 2;
        this.labels.C = 1;
    }
    // array of 3 arrays, every array of them represent a single tower.
    this.towerStack = [
        [],
        [],
        []
    ];

    // fill the first array ( tower ) with numbers from 
    // 'numberOfDisks' to 1. eg: {3,2,1}
    for (var i = numberOfDisks; i > 0; i--) {
        this.towerStack[this.labels.A].push(i);
    }

}

/**
 * print the rout of the disk, eg: from A to B...
 * @param  {int} towerFrom        index of the source tower
 * @param  {int} towerDestination index of the destination tower
 * @return {void}                  print the result on the document.
 */
Tower.prototype.printRout = function(towerFrom, towerDestination) {
    // get the labels of the towers from their keys.
    var towerFromLabel = Object.keys(this.labels)[towerFrom];
    var towerDestinationLabel = Object.keys(this.labels)[towerDestination];

    //return (towerFromLabel + ' --> ' + towerDestinationLabel + '<br>');
    return [towerFromLabel, towerDestinationLabel];
}

/**
 * this function check if it can move from soucre to destination
 * if yes it moves. else it will move from destination to source.
 * @param  {int} towerFrom        source tower
 * @param  {int} towerDestination destination tower index
 * @return {result from the movDisk function}
 */
Tower.prototype.bidirectionalMove = function(towerFrom, towerDestination) {
    // if (this.getArrayTopValue(this.towerStack[towerFrom]) <
    //     this.getArrayTopValue(this.towerStack[towerDestination])) {
    //     return this.moveDisk(towerFrom, towerDestination);
    // } else {
    //     return this.moveDisk(towerDestination, towerFrom);
    // }
    // 
    if (this.getArrayTopValue(this.towerStack[towerDestination]) == 0) {
        return this.moveDisk(towerFrom, towerDestination);
    } else if (this.getArrayTopValue(this.towerStack[towerFrom]) == 0) {
        return this.moveDisk(towerDestination, towerFrom);
    } else if (this.getArrayTopValue(this.towerStack[towerFrom]) <
        this.getArrayTopValue(this.towerStack[towerDestination])) {
        return this.moveDisk(towerFrom, towerDestination);
    } else {
        return this.moveDisk(towerDestination, towerFrom);
    }
}

/**
 * move the disk from source tower to destination tower.
 * moveDisk is moving disks along towerStacks.
 * @param  {int} towerFrom        source tower
 * @param  {int} towerDestination destination tower
 * @return {bool}                  return true if didnt reach the end. else return false.
 */
Tower.prototype.moveDisk = function(towerFrom, towerDestination) {
    if (this.testIfFinished() != true) {
        var valueToMove = this.towerStack[towerFrom].pop();
        this.towerStack[towerDestination].push(valueToMove);
        return this.printRout(towerFrom, towerDestination);
    } else {
        return false;
    }
}

/**
 * return the top element of the tower
 * if tower is empty it will return a large number
 * I used this to let the bidirectionalMove function
 * able to move disk from source to it.
 * @param  {array} array any array
 * @return {int}       size of the disk or a large number if empty.
 */
Tower.prototype.getArrayTopValue = function(array) {
    if (array.length == 0) {
        return 0;
    } else if (array.length > 0) {
        return array[array.length - 1];
    } else {
        return 0;
    }
}

/**
 * to let the script know if it reach the last step
 * in another way. if the last tower is filled.
 * @return {boolean} if game finished return true, else return false
 */
Tower.prototype.testIfFinished = function() {
    if (this.towerStack[2].length == this.numberOfDisks) {
        return true;
    }
    return false;
}