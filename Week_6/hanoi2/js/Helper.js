/**
 * move html elements from div to another
 * @param  {array} array array of the source and destination 0 is cource
 * and 1 is the destination.
 * @return {void}
 */
function moveDivContent(sourceDiv, destinationDiv) {
    var topElementFromSource = sourceDiv.children[0];
    var allElementsFromDestination = destinationDiv.children;

    destinationDiv.innerHTML = topElementFromSource.outerHTML + destinationDiv.innerHTML;

    sourceDiv.removeChild(topElementFromSource);
    console.log(destinationDiv.innerHTML);

}