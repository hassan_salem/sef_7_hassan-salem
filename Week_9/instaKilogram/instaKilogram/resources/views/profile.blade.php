@extends('layouts.app')

@section('content')
<meta name="_token" content="{{ csrf_token() }}"/>
<div class="container" style="margin-top: 20px; margin-bottom: 20px;">
    <div class="row panel">
        <div class="col-md-4 bg_blur ">
            @if($followedBy->count() == 0 && $user->id != Auth::user()->id)
            <button id="follow" class="follow_btn hidden-xs" userId="{{$user->id}}">Follow</button>
            @endif
        </div>
        <div class="col-md-8  col-xs-12">
            <img src="{{asset('uploads')}}/{{$user->image}}" class="img-thumbnail picture hidden-xs" />
            <img src="http://lorempixel.com/output/people-q-c-100-100-1.jpg" class="img-thumbnail visible-xs picture_mob" />
            <div class="header">
                <h1>{{$user->name}} @if($user->id == Auth::user()->id)<a href="{{$user->id}}/edit">Edit</a>@endif </h1>
                <h4>{{$user->email}}</h4>
                <span>{{$user->bio}}</span>
            </div>
        </div>
    </div>

    <div class="row nav">
        <div class="col-md-4"></div>
        <div class="col-md-8 col-xs-12" style="margin: 0px;padding: 0px;">
            <div class="col-md-4 col-xs-4 well"><i class="fa fa-weixin fa-lg"></i> {{$user->comments->count()}}</div>
            <div class="col-md-4 col-xs-4 well"><i class="fa fa-heart-o fa-lg"></i> {{$user->likes->count()}}</div>
            <div class="col-md-4 col-xs-4 well"><i class="fa fa-align-justify fa-lg"></i> {{$user->posts->count()}}</div>
        </div>
    </div>

    <div id="posts"></div>
</div>



    <script src="{{asset('js')}}/profile.js"></script>
    <script src="{{asset('js')}}/home.js"></script>

@endsection