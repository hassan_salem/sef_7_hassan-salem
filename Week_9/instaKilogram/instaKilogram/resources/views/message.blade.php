<div class="chat_wrap" id="chat-wrapper-{{$user_id}}" userId="{{$user_id}}">
    <div class="toggle">
        <h3>Chat</h3>
    </div>
    <div class="chat">
        <header>
            <h3 class="toggle"><i id="uname{{$user_id}}">##chatusername##</i> <button class="close closeChat pull-right" userId="{{$user_id}}">X</button></h3>
        </header>
        <div class="chatArea" id="chatArea-{{$user_id}}">
            <ul id="chats-{{$user_id}}">
                @foreach($messages as $message)
                    <li>
                        <em>
                            @if($message->user_id == Auth::user()->id)
                                Me:
                                @else
                                {{$user_name}}:
                            @endif
                                {{$message->message}}
                        </em>
                    </li>
                @endforeach
            </ul>
        </div>
        <form method="post" id="chat-form-{{$user_id}}">
            <div style="background-color:white;">{{Auth::user()->name}}</div>
            <input type="text" name="chat_message" maxlength="140" placeholder="Message" required autocomplete="off">
            <input type="hidden" name="to_user_id" value="{{$user_id}}" />
            {!! csrf_field() !!}
        </form>
    </div>
</div>