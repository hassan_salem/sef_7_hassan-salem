@foreach($posts as $post)
    <div class="row postWrapper">
        <div class="postHeader">
            <div class="userInfo">
                <img src="{{asset('uploads')}}/{{$post->user->image}}" width="50" height="50" class="pull-left" />
                <div class="pull-left"><a href="profile/{{$post->user->id}}">{{$post->user->name}}</a> <button class="btn-link like" postId="{{$post->id}}">Like {{$post->likes->count()}}</button> </div>
            </div>
            <div class="postDate pull-right">@timeLapse($post->created_at)</div>
        </div>
        <div class="postContent">
            <img src="{{asset('uploads/posts')}}/{{$post->user->id}}/{{$post->image}}" width="100%" />
            <div>
                {{$post->text}}
            </div>
            <div class="comments" id="comments-{{$post->id}}">
                @foreach($post->comments as $comment)
                    <div>
                        {{$comment->user->name}}: {{$comment->comment}}
                    </div>
                @endforeach
            </div>
        </div>
        <div class="postActions">
            <textarea class="form-control"></textarea>
            <button postId="{{$post->id}}" class="btn pull-right comment">Add</button>
        </div>
    </div>
    <hr>
@endforeach
