@extends('layouts.app')

@section('content')
    <meta name="_token" content="{{ csrf_token() }}"/>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-2">
            <div id="form">
                <div id="formHeader">
                    <img src="{{asset('uploads')}}/{{Auth::user()->image}}" width="50" height="50" />
                    <textarea id="postText" class="form-control" id="postText"></textarea>
                </div>
                <div id="postImagePreview" class="text-center">
                    <img src="images/loading.gif" id="imageToUpload" width="300" height="270"/>
                </div>
                <div id="actions">
                    <input type="file" id="postImage" name="postImage" />

                    <button class="btn btn-sm btn-link" id="btnSelectImage">Select image..</button>
                    <button class="pull-right btn-link " id="btnPost">Post</button>
                </div>
            </div>

            <hr />

            <div id="follow">
                <ul>
                    @foreach($users as $user)
                    <li>
                        {{$user->name}} : <button class="follow btn-link" userId="{{$user->id}}">Follow..</button>
                    </li>
                    @endforeach
                </ul>
            </div>
            <input type="file" id="take-picture" accept="image/*">

            <div id="posts"></div>






        </div>
        <div class="col-md-2" style="border: 1px solid gray;">
            <ul id="friendsList">
                @foreach($friends as $friend)
                    <li userId="{{$friend->id}}" userName="{{$friend->name}}" id="friend-{{$friend->id}}">{{$friend->name}} <i class="userMessageCount fa fa-weixin fa-lg" id="msg-{{$friend->id}}">0</i> </li>
                @endforeach
            </ul>
        </div>




    </div>


</div>

<div id="chatBoxes"></div>


    <script src="{{URL::asset('js/home.js')}}"></script>
@endsection
