/**
 * Created by applepc on 5/11/16.
 */
$(document).ready(function() {

    var formData = new FormData();
    var userData = new FormData();
    var postData = new FormData();
    var commentData = new FormData();
    var intervals = [];

    var page = 1;

    // select image from buttun
    $('#btnSelectImage').on('click', function() {
        $('#postImage').click();
    });

    // add new post button.
    $('#btnPost').on('click', function() {
        formData.append('text', $('#postText').val());

        $.ajax({
            url: "postImageUpload",
            type: "POST",
            data: formData,
            async: false,
            success: function (msg) {
                $('#posts').prepend(msg);
                $('#postText').val('');
                $('#postImagePreview').css('display', 'none');
            },
            beforeSend: function(xhr){
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name=_token]').attr('content'));
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });

    // handle if image is selected.
    $('#postImage').on('change', function() {
        $('#postImagePreview').css('display', 'block');

        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#imageToUpload').attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
            formData.append('postImage', this.files[0]);
        }
    });

    // press on follow button.
    $('.follow').on('click', function() {
        userData.append('userId', $(this).attr('userId'));
        var parent = $(this).parent();
        $.ajax({
            url: "follow",
            type: "POST",
            data: userData,
            async: false,
            success: function (msg) {
                if (msg != 'error'){
                    parent.remove();
                }

            },
            beforeSend: function(xhr){
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name=_token]').attr('content'));
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });

    // click like button.
    $(document).on('click', '.like', function(){
        postData.append('postId', $(this).attr('postId'));
        var btn = $(this);
        $.ajax({
            url: "like",
            type: "POST",
            data: postData,
            async: false,
            success: function (msg) {
                btn.text('Like ' + msg);
            },
            beforeSend: function(xhr){
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name=_token]').attr('content'));
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });

    // click on comment button.
    $(document).on('click', '.comment', function(){
        var textarea = $(this).parent().find('textarea');
        var postId = $(this).attr('postId')
        var textValue = textarea.val();
        commentData.append('comment', textValue);
        commentData.append('postId', postId);
       // alert(postId);
        $.ajax({
            url: "comment",
            type: "POST",
            data: commentData,
            async: false,
            success: function (msg) {
                if(msg != 'error'){
                    $('#comments-' + postId).append('<div>' + msg + '</div>');
                    textarea.val('');
                }
            },
            beforeSend: function(xhr){
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name=_token]').attr('content'));
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });

    // first open.. no scroll bars available
    if ($("body").height() <= $(window).height()) {
        $.ajax({
            url: "getPostsList/" + page++,
            type: "GET",
            async: false,
            success: function (msg) {
                if(msg != 'error' && msg){
                    $('#posts').append(msg);
                }
            },
            beforeSend: function(xhr){
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name=_token]').attr('content'));
            },
        });
    }

    // handle when page is scrolling.
    $(document).on('scroll', function() {
       if($(window).scrollTop() + $(window).height() == $(document).height()){
           $.ajax({
               url: "getPostsList/" + page++,
               type: "GET",
               async: false,
               success: function (msg) {
                   if(msg != 'error'){
                       $('#posts').append(msg);
                   }
               },
               beforeSend: function(xhr){
                   xhr.setRequestHeader('X-CSRF-Token', $('meta[name=_token]').attr('content'));
               },
           });
       }
    });

    // when user click on a friend.
    $('#friendsList li').on('click', function() {
        var userId = $(this).attr('userId');
        var userName = $(this).attr('userName');
        $.get('getChat/' + userId, function(data){
            data = data.replace('##chatusername##', userName);
            $('#chatBoxes').append(data);

            intervals[userId] = setInterval(function(){

                $.getJSON("getMessage/" + userId, function (data) {
                    console.log(data);
                    if (data){
                        $('#chats-' + userId).append('<li><em>' + $('#uname' + userId).text() + ': ' + data['message'] + '</em></li>');
                        $("#chatArea-" + userId).animate({ scrollTop: $('#chatArea-' + userId).prop("scrollHeight")}, 1000);
                    }

                });
            }, 1000);
        })
    });

    // close a chat.
    $(document).on('click', '.closeChat', function(){
        var userId = $(this).attr('userId');
        clearInterval(intervals[userId]);
        $('#chat-wrapper-' + userId).remove();
    });

    // check messages every 2 seconds
    var checkMessages = function() {
        $.getJSON("checKmessages", function (data) {
            if (data.length<=0){
                $('.userMessageCount').text(0);
            }
            $.each(data, function (key, val) {
                $('#msg-' + val['user_id']).text(val['total']);
            });
        });
    }
    var interval = setInterval(checkMessages, 2000);


    // to drag message box
    var $dragging = null;
    // handle event mouse move.
    $(document.body).on("mousemove", function(e) {
        if ($dragging) {
            $dragging.offset({
                top: e.pageY,
                left: e.pageX
            });
        }
    });

    // when user press on the box.
    $(document.body).on("mousedown", ".chat_wrap", function (e) {
        $dragging = $(this);
    });

    // when mouseup .
    $(document.body).on("mouseup", function (e) {
        $dragging = null;
    });

    // submit message.
    $(document).on('submit', 'form', function(event){
        event.preventDefault();
        $.post( "sendMessage", $(this).serialize());
        var postData = $( this ).serializeArray();
        console.log(postData[0]['value']);
        $('#chats-' + postData[1]['value']).append('<li><em>' + 'me: ' + postData[0]['value'] + '</em></li>');
        $(this).find('input[type=text]').val('');
        $("#chatArea-" + postData[1]['value']).animate({
            scrollTop: $('#chatArea-' + postData[1]['value']).prop("scrollHeight")
        }, 1000);
    })





});