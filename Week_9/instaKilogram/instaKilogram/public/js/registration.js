/**
 * Created by applepc on 5/10/16.
 */
$(document).ready(function() {

    var x = 0,
        y = 0,
        height = width = 300;
    var formData = new FormData();

    $('#image').change(function() {
        $('#popupWrapper').css('display', 'block');

        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#popupImage').attr('src', e.target.result);
            }

            reader.readAsDataURL(this.files[0]);
            formData.append('image', this.files[0]);
            alert(this.files[0]);
        }

    });

    $('#imgContainer').on('scroll', function() {
        x = $(this).scrollLeft();
        y = $(this).scrollTop();
    });

    $('#cancle').on('click', function() {
        $('#popupWrapper').css('display', 'none');
    });


    $('#btnUpload').on('click', function() {
        $.ajax({
            url: "imageUpload",
            type: "POST",
            data: formData,
            async: false,
            success: function(msg) {
                $('#userProfileImage').val(msg);
                $('#popupWrapper').css('display', 'none');
            },
            beforeSend: function(xhr) {
                formData.append('x', x);
                formData.append('y', y);
                formData.append('width', width);
                formData.append('height', height);
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name=_token]').attr('content'));
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });




    // upload cam image

    function dataURItoBlob(dataURI) {
        // convert base64/URLEncoded data component to raw binary data held in a string
        var byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0)
            byteString = atob(dataURI.split(',')[1]);
        else
            byteString = unescape(dataURI.split(',')[1]);

        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

        // write the bytes of the string to a typed array
        var ia = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        return new Blob([ia], {
            type: mimeString
        });
    }

    navigator.getUserMedia = navigator.getUserMedia ||
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia ||
        navigator.msGetUserMedia;

    var localStream = null;

    var canvas = document.querySelector('#snapshot');
    var video = document.querySelector('#preview');

    var ctx = canvas.getContext('2d');

    var start = document.querySelector('#start');
    start.addEventListener('click', function() {
        $('#preview').css('display', 'block');
        startCaptureImage();
        return false;
    }, false);

    var capture = document.querySelector('#capture');
    capture.addEventListener('click', function() {
        if (!localStream) {
            return;
        }

        var img = document.querySelector('#snapshot');
        img.width = canvas.width = video.videoWidth;
        img.height = canvas.height = video.videoHeight;

        ctx.drawImage(video, 0, 0);

        $('#snapshot').css('display', 'none');
        $('#preview').css('display', 'none');

        $('#popupWrapper').css('display', 'block');

        $('#popupImage').attr('src', canvas.toDataURL('image/png'));



        formData.append('image', dataURItoBlob(canvas.toDataURL('image/png')));



    }, false);

    function errorCallback(err) {
        console.error('Failed', err);
    }

    function startCaptureImage() {
        navigator.getUserMedia({
            video: true
        }, function(data) {
            video.src = window.URL.createObjectURL(data);
            localStream = data;
        }, errorCallback);
    }


});