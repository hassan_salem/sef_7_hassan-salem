$(document).ready(function() {

    var page = 1;
    $('#follow').on('click', function() {
        var userData = new FormData();
        userData.append('userId', $(this).attr('userId'));
        var parent = $(this);
        $.ajax({
            url: "follow",
            type: "POST",
            data: userData,
            async: false,
            success: function (msg) {
                if (msg != 'error'){
                    parent.remove();
                }

            },
            beforeSend: function(xhr){
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name=_token]').attr('content'));
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });

    // first open.. no scroll bars available
    if ($("body").height() <= $(window).height()) {
        $.ajax({
            url: window.location.href + "/getUserPostsList/" + page++,
            type: "GET",
            async: false,
            success: function (msg) {
                if(msg != 'error'){
                    $('#posts').append(msg);
                }
            },
            beforeSend: function(xhr){
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name=_token]').attr('content'));
            },
        });
    }

    $(document).on('scroll', function() {
        if($(window).scrollTop() + $(window).height() == $(document).height()){
            $.ajax({
                url: window.location.href + "/getUserPostsList/" + page++,
                type: "GET",
                async: false,
                success: function (msg) {
                    if(msg != 'error'){
                        $('#posts').append(msg);
                    }
                },
                beforeSend: function(xhr){
                    xhr.setRequestHeader('X-CSRF-Token', $('meta[name=_token]').attr('content'));
                },
            });
        }
    });





});