<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    public function posts(){
        return $this->hasMany('App\Post', 'user_id');
    }

    public function comments(){
        return $this->hasMany('App\Comment', 'user_id');
    }

    public function likes(){
        return $this->hasMany('App\Like');
    }

    public function like(){
        //return $this->hasMany('App\Like', 'user_id');
        return $this->belongsToMany('App\User', 'likes', 'user_id', 'post_id');
    }

    public function followers()
    {
        return $this->belongsToMany('App\User', 'user_follows', 'follow_id', 'user_id');
    }

    public function messages(){
        return $this->hasMany('App\Message');
    }

    public function follow(){
        return $this->belongsToMany('App\User', 'user_follows', 'user_id', 'follow_id');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','image', 'phone', 'sex', 'bio', 'website'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


}
