<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	/**
	 * the comment is for whitch post ?
	 * @return [type] [description]
	 */
    public function post(){
        return $this->belongsTo('App\Post', 'post_id');
    }

    /**
     * the comment of the comment.
     * @return [type] [description]
     */
    public function comment(){
        return $this->belongsTo('App\Comment', 'comment_id');
    }

    /**
     * the user made the comment
     * @return [type] [description]
     */
    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }
}
