<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
	/**
	 * like user.
	 * @return [type] [description]
	 */
    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * like post
     * @return [type] [description]
     */
    public function post(){
        return $this->belongsTo('App\Post', 'post_id');
    }
}
