<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    if(!Auth::user()){
        return view('auth.register');
    }else{
        return redirect('/home');
    }

});

Route::auth();

Route::get('/home', 'PostsController@index');

Route::get('/test', 'TestController@likes');

Route::post('/imageUpload', 'UploadImageController@uploadProfile');

Route::post('/profile/{id}/imageUpload', 'UploadImageController@uploadProfile');

Route::post('/postImageUpload', 'PostsController@savePost');

Route::post('/follow', 'UsersController@follow');

Route::post('/like', 'PostsController@like');

Route::post('/profile/like', 'PostsController@like');

Route::post('/comment', 'PostsController@comment');

Route::post('/profile/comment', 'PostsController@comment');

Route::get('/getPostsList/{page}', 'PostsController@getPostsList');

Route::get('/profile/{id}', 'UsersController@profile');

Route::post('/profile/follow', 'UsersController@follow');

Route::get('/profile/{userId}/getUserPostsList/{page}', 'PostsController@getUserPostsList');

Route::get('/checKmessages', 'MessagesController@checkMessages');

Route::get('/getChat/{id}', 'MessagesController@getChat');

Route::get('/getMessage/{id}', 'MessagesController@getMessage');

Route::post('/sendMessage', 'MessagesController@sendMessage');

Route::get('/profile/{id}/edit', 'UsersController@editProfile');

Route::post('/profile/edit/do', 'UsersController@doEditProfile');

