<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\Auth;

class UploadImageController extends Controller
{
    /**
     * [uploadProfile description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function uploadProfile(Request $request){
        if($request->hasFile('image')){
            $fileExtension = 'png';
            if($request->file('image')->getClientOriginalExtension()){
                $fileExtension = $request->file('image')->getClientOriginalExtension();
            }



            $filename  = rand(100, 600000). '-' . time() . '.' . $fileExtension;
            // Todo : crop the image..
            $request->file('image')->move(public_path('uploads'), $filename);
            //vardump($request);
            echo $filename;
        }else{
            echo "error";
        }
    }

//    public function uploadPostImage(Request $request){
//        if($request->hasFile('postImage')){
//            $filename = rand(100, 6000000) . '-' . time() . '.' . $request->file('image')->getClientOriginalExtension();
//            $upload = $request->file('postImage')->move(public_path('uploads/posts/' . Auth::user()->id), $filename);
//            if($upload){
//                return $filename ;
//            } else {
//                return 'error';
//            }
//        }
//    }
}
