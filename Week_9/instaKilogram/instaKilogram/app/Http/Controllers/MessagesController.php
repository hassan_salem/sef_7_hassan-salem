<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;
use Auth;
use App\Message;
use DB;
/**
 * controll all messages operations
 */
class MessagesController extends Controller
{
    /**
     * return count of messages sent from every user 
     * to the loged in user
     * @return [type] [description]
     */
    public function checKmessages(){
        $messages = DB::table('messages')
            ->select('user_id', DB::raw('count(*) as total'))
            ->where('to_user_id', '=', Auth::user()->id)
            ->where('status', '=', '0')
            ->groupBy('user_id')
            ->get();


        return  $messages;
    }

    /**
     * return the all chat of the given user.
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function getChat($id){
        $messages = DB::table('messages')
            ->select('*')
            ->where('user_id', '=', $id)
            ->where('to_user_id', '=', Auth::user()->id)
            ->orWhere(function($query) use($id){
                $query->where('user_id', '=', Auth::user()->id)
                    ->where('to_user_id', '=', $id);
            })
            ->orderBy('id', 'ASC')
            ->get();
        DB::table('messages')->where('status', '=', '0')->update(['status' => '1']);
        return view('message', ['messages'=>$messages, 'user_id'=>$id, 'user_name' => User::find($id)->name]);
    }


    /**
     * get the new message.
     * @param  [type] $userId [description]
     * @return [type]         [description]
     */
    public function getMessage($userId){
        $messages = Message::where('user_id', '=', $userId)
            ->where('to_user_id', '=', Auth::user()->id)
            ->where('status', '=', '0')
            ->first();
        if($messages){
            $messages->status = 1;
            $messages->save();
        }

        return $messages;
        //return view('message', ['messages'=>$messages, 'user_id'=>$userId]);
    }

    /**
     * send message. insert the record
     * in the database
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function sendMessage(Request $request){
        $message = new Message();
        $message->message = $request->chat_message;
        $message->user_id = Auth::user()->id;
        $message->to_user_id = $request->to_user_id;
        $message->status = '0';
        $message->save();
    }
}
