<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Post;
use App\User;
use App\Comment;
use App\Like;
use Illuminate\Support\Facades\Auth;
use Config;
require_once ('functions.php');
class PostsController extends Controller
{

    // Todo : get/set configuration vars.
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * home page.
     * @return [type] [description]
     */
    public function index()
    {
        $users = User::whereNotIn('id', function($query){
            $query->select('follow_id')->from('user_follows')->where('user_id', '=', Auth::user()->id);
        })->where('id', '!=', Auth::user()->id)->get();

        $friends = User::find(Auth::user()->id)->follow;
        return view('home', ['users' => $users, 'friends' => $friends]);
    }

    /**
     * return posts lists from given page
     * @param  integer $page [description]
     * @return [type]        [description]
     */
    public function getPostsList($page = 1){
        $offset = Config::get('app.postsPerPage');
        $skip = ($page - 1) * $offset;

        $posts = Post::orderBy('id', 'DESC')->skip($skip)->take($offset)->whereIn('user_id', function($query){
            $query->select('follow_id')->from('user_follows')->where('user_id', '=', Auth::user()->id);
        })->orWhere('user_id', '=', Auth::user()->id)->get();


        $postsCount = Post::all()->count();
        $pagesCount = ceil($postsCount/$offset);
        return view('listPosts', ['posts' => $posts, 'page' => $page, 'pages' => $pagesCount]);
    }

    /**
     * get user posts.
     * @param  [type] $userId [description]
     * @param  [type] $page   [description]
     * @return [type]         [description]
     */
    public function getUserPostsList($userId, $page){
        $offset = Config::get('app.postsPerPage');
        $skip = ($page - 1) * $offset;
        $posts = Post::orderBy('id', 'DESC')->skip($skip)->take($offset)->Where('user_id', '=', $userId)->get();
        return view('listPosts', ['posts' => $posts, 'page' => $page]);
    }

    /**
     * save post in the database.
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function savePost(Request $request){
        // Todo : validation .
        if($request->hasFile('postImage')){
            $filename = rand(100, 6000000) . '-' . time() . '.' . $request->file('postImage')->getClientOriginalExtension();
            $upload = $request->file('postImage')->move(public_path('uploads/posts/' . Auth::user()->id), $filename);
            if($upload){
                $post = new Post();
                $post->image = $filename;
                $post->text = $request->text;
                $post->user_id = Auth::user()->id;
                $post->save();

                $result = '<div class="row postWrapper"><div class="postHeader"><div class="userInfo"><img src="uploads/'. Auth::user()->image .'" width="50" height="50" class="pull-left" /><div class="pull-left">' . Auth::user()->name . ' <button class="btn-link like" postId="' . $post->id . '">Like ' . $post->likes->count() . '</button></div></div><div class="postDate pull-right">4 hours ago</div></div> <div class="postContent"><img src="uploads/posts/' . Auth::user()->id . '/' . $filename . '" width="100%" /><div>' . $request->text . '</div><div class="comments" id="comments-' . $post->id . '"></div> </div><div class="postActions"><textarea class="form-control"></textarea><button postId="' . $post->id . '" class="btn pull-right  comment">Add</button></div></div> <hr>';
                echo $result ;
            } else {
                echo 'error';
            }
        }
    }

    /**
     * save like.
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function like(Request $request){
        $user = User::find(Auth::user()->id);
        $post = Post::find($request->postId);

        $likes = Like::where('user_id', '=', $user->id)->where('post_id', '=', $post->id)->get();
        if($likes->count() <= 0){
            $user->like()->save($post);
        }else{
            // Todo : delete the like..
            $user->like()->delete($post);
        }
        echo $post->likes->count();

    }

    /**
     * [comment description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function comment(Request $request){
        // Todo : validation.
        $post = Post::find($request->postId);
        $user = User::find(Auth::user()->id);
        $comment = new Comment();
        $comment->comment=$request->comment;
        $comment->user_id = $user->id;
        $comment->post_id = $post->id;

        $comment->save();
        echo $user->name . ": " . $request->comment;
        // Todo : save the comment in the right way ...

    }


}
