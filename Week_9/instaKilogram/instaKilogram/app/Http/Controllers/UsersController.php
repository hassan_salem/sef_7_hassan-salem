<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;
use Illuminate\Support\Facades\Auth;
use Redirect;
class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * user follow another user.
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function follow(Request $request){
        $yourUser = User::find($request->userId);
        $myUser = User::find(Auth::user()->id);

        echo $myUser->follow()->save($yourUser);
    }

    /**
     * show user profile.
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function profile($id){
        $user = User::find($id);

        $followedBy = $user->followers()->whereIn('user_id', [Auth::user()->id]);

        return view('profile', ['user'=>$user, 'followedBy' => $followedBy]);
    }

    /**
     * show edit profile form
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editProfile($id){
        $user = Auth::user();
        $userId = $user->id;
        if($userId == $id){
            return view('editProfile', ['userData' => $user]);
        }else{
            // Todo : create errors page.
            echo "no access";
        }
    }

    /**
     * save record.
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function doEditProfile(Request $request){
        $validation = array(
            'email' => 'required',
            'name' => 'required',
        );
        if($request->password){
            $validation['password'] = 'required|min:6';
        }
        $this->validate($request, $validation);

        $user = User::find(Auth::user()->id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->bio = $request->bio;
        $user->website = $request->website;
        $user->sex = $user->sex;
        $user->image = $request->userProfileImage;

        if($request->password){
            if($request->password != $request->password_confirmation){
                return Redirect::back()
                    ->withErrors(['password_confirmation' => 'passwords are not equal']);
            }else{
                if(bcrypt($request->old_password) == $user->password){
                    $user->password = $request->passwrod ;
                }else{
                    return Redirect::back()
                        ->withErrors(['old_password' => 'Old password is not correct']);
                }
            }
        }

        $user->save();
        return redirect('/home');
    }
}
