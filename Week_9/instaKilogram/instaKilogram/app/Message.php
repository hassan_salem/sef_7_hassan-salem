<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
	/**
	 * message user.
	 * @return [type] [description]
	 */
    public function user(){
        return $this->belongsTo('App\User');
    }
}
