<?php

function printL($row){
	$rowContent = array() ;
	// reg ex to filter the line into array.
	$regex = '/^(\S+) (\S+) (\S+) \[([^:]+):(\d+:\d+:\d+) ([^\]]+)\] \"(\S+) (.*?) (\S+)\" (\S+) (\S+) "([^"]*)" "([^"]*)"$/';
	preg_match($regex ,$row, $rowContent);
	$ip 			= $rowContent[1] ;
	$dateTime 		= date_format(date_create($rowContent[5]),'l, F d Y h:m:s');
	$responseType 	= $rowContent[8] ;
	$responseFile 	= $rowContent[9] ;
	$responseCode 	= $rowContent[10];
	echo "$ip -- $dateTime \"$responseType $responseFile \" $responseCode\n" ;
}
// put the file into an array.
$content = file("/var/log/apache2/access.log", FILE_SKIP_EMPTY_LINES);
// Parsing the array
foreach ($content as $row) {
	// print the log using the function printL
	printL($row);
}