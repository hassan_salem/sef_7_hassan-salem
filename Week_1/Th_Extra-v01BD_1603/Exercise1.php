<?php 
// The strategy is to deal on plan
// We suggest: for the last one because he can see everybody, 
// he should count all the white hats in front. 
// If they are even he call " White " else he call " Black "
// now the next one know that he and the rest have even whites or odd whites
// he have to count the hats in front him,
// if the previous (except the first because he might be wrong ) and the rest
// of hats are even then he is black if the first was even .
// example : w b w w b w b 
// by skipping first w and count whites we found w = 3 .. odd .
// he will say black .. so all of the group know that w are odd. 
// the next one count all the rest but the first. so he will find that they are odd 
// and he know that he and them are odd .. so he is black.
// function to give me the first guess.

// function to know if white is even or not.
function wIsEven($data){
    $count = 0 ;
    for ($i=1; $i < count($data) ; $i++) { 
        if ($data[$i] == "white") {
            $count ++ ;
        }
    }
    if ($count % 2 == 0) {
            return "white";
        }else{
            return "black";
        }
}

// function to count the white in front
function countWhiteFromIndex($index,$data) {
    $count = 0 ;
    for ($i=$index; $i < count($data); $i++) { 
        if ($data[$i] == "white") {
            $count ++;
        }
    }
    return $count;
}

// function to solve the question  :) , it will take the data as array then parse it.
function solveIt($data){
    $whites = 0;                // count how many whites in the array 
    $even = wIsEven($data);     // give me the first guess if w is even then w else b
    $answers = array($even);    // array holds the answers and its first item is the first guess
    
    // parse the data array from the second item
    for ($i=1; $i < count($data) ; $i++) { 
        // if the first guess is white . then white is even.
        if($even == "white") {
            if ((countWhiteFromIndex($i+1,$data)+$whites) % 2 == 0) {
                array_push($answers,"black");
            }else {
                array_push($answers,"white");
                $whites ++ ;
            }
            // else if the first guess is black . then white is black.
        }else { 
            
            if ((countWhiteFromIndex($i+1,$data)+$whites) % 2 == 0) {
                array_push($answers,"white");
                $whites ++ ;
            }else {
                array_push($answers,"black");
            }
        }
    }
    return $answers ;
}
// remove the unneeded argument ( first one )

array_shift($argv);
$solution = solveIt($argv);
foreach ($solution as $color) {
	echo $color."\n";
}

