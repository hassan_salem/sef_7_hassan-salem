<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Timelabel extends Model
{
    public function team() {
        return $this->belongsTo('App\Team');
    }

    public function user() {
        return $this->belongsToMany('App\User', 'user_timelabel');
    }

    public function teams() {
        return $this->belongsToMany('App\Team', 'user_timelabel');
    }
}
