<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    public function user() {
        return $this->belongsTo('App\User');
    }

    public function getDates()
    {
        return array('created_at', 'updated_at', 'time_comment');
    }

}
