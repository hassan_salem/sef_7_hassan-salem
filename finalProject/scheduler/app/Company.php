<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    public function user() {
        return $this->belongsTo('App\User');
    }

    public function teams() {
        return $this->hasMany('App\Team');
    }

    public function codes() {
        return $this->hasMany('App\Code');
    }

    public function users() {
        return $this->belongsToMany('App\User', 'user_company');
    }

}
