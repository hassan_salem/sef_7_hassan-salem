<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
use DB;
class User extends Authenticatable
{
    public function companies() {
        return $this->hasMany('App\Company');
    }

    public function teams() {
        return $this->belongsToMany('App\Team', 'user_team');
    }

    public function timelabels() {
        return $this->hasMany('App\Team', 'user_timelabel');
    }

    public function requests() {
        return $this->hasMany('App\Request');
    }

    public function pendingRequests() {
        $user = Auth::user();
        $recievedRequests = DB::table('requests')
            ->join('user_timelabel', 'user_timelabel_id', '=', 'user_timelabel.id')
            ->join('users as musers', 'musers.id', '=', 'requests.user_id')
            ->join('users', 'user_timelabel.user_id', '=', 'users.id')
            ->join('timelabels', 'user_timelabel.timelabel_id', '=', 'timelabels.id')
            ->where('user_timelabel.user_id', '=', $user->id)
            ->where('requests.status', '=', '0')
            ->get(
                [
                    'user_timelabel.date',
                    'user_timelabel.id',
                    'user_timelabel.timelabel_id',
                    'users.id as user_id',
                    'requests.user_id as suserid',
                    'musers.name',
                    'timelabels.label',
                    'requests.created_at',
                    'requests.status',
                    'requests.id as request_id'
                ]
            );
        return $recievedRequests;
    }

    public function myRequests() {
        $user = Auth::user();
        $recievedRequests = DB::table('requests')
            ->join('user_timelabel', 'user_timelabel_id', '=', 'user_timelabel.id')
            ->join('users as musers', 'musers.id', '=', 'requests.user_id')
            ->join('users', 'user_timelabel.user_id', '=', 'users.id')
            ->join('timelabels', 'user_timelabel.timelabel_id', '=', 'timelabels.id')
            ->where('requests.user_id', '=', $user->id)
            ->get(
                [
                    'user_timelabel.date',
                    'user_timelabel.id',
                    'user_timelabel.timelabel_id',
                    'users.id as user_id',
                    'requests.user_id as suserid',
                    'musers.name',
                    'timelabels.label',
                    'requests.created_at',
                    'requests.status',
                    'requests.id as request_id'
                ]
            );
        return $recievedRequests;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
