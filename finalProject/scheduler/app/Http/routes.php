<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::auth();

// home controller.
Route::get('/', 'HomeController@index');

// login.
Route::get('/login', function() {
    return view('login');
});

Route::get('/register', function() {
    return view('register');
});

/**
 * company operations.
 */
// create new company form
Route::get('/company', 'CompanyController@form');

// edit company form
Route::get('/company/{id}', 'CompanyController@form');

// view all companies in a table
Route::get('/companies', 'CompanyController@load');

// view one company.
Route::get('/companies/{id}', 'CompanyController@load');

// action for create a company
Route::post('/company', 'CompanyController@create');

// action for deleting a company
Route::delete('/company/{id}', 'CompanyController@delete');

// action for updating a company
Route::put('/company', 'CompanyController@update');

// action to create new codes.
Route::post('/codes', 'CompanyController@codes');



// add team form..
Route::get('/team', 'TeamController@form');

// edit team form..
Route::get('/team/{id}', 'TeamController@form');


// get one team.
Route::get('/teams/{id}', 'TeamController@load');
Route::get('/teams/{id}/{labelId}', 'TeamController@load');

// action for adding new team.
Route::post('/team', 'TeamController@create');

// action for deleting team.
Route::delete('/team', 'TeamController@delete');

// action for editing team.
Route::put('/team', 'TeamController@update');

// add user to a team.
Route::post('/teamUser', 'TeamController@addUser');

// remove user from a team.
Route::delete('/teamUser', 'TeamController@removeUser');





// get schedule for specific team.
Route::get('/schedule/{teamId}', 'ScheduleController@view');

// add new label.
Route::post('/label', 'ScheduleController@addLabel');

// update label.
Route::put('/label', 'ScheduleController@updateLabel');

// add new schedule form.
Route::post('/schedule', 'ScheduleController@createForm');

// create new schedule
Route::post('/newSchedule', 'ScheduleController@create');

// get user profile.
Route::get('/user', 'UserController@profile');

// get users.
Route::get('/users', 'UserController@load');
// get one team.
//Route::get('/users/{id}', 'UserController@load');
Route::post('/user', 'UserController@create');
Route::delete('/user/{id}', 'UserController@delete');
Route::put('/user', 'UserController@update');
Route::post('/userCode', 'UserController@code');

Route::get('/userId', 'UserController@getId');



// create request form.
Route::get('/request', 'RequestController@form');
Route::post('/request', 'RequestController@create');
Route::put('/request', 'RequestController@update');
Route::delete('/request', 'RequestController@delete');

Route::get('/requests', 'RequestController@load');
// get one team.
//Route::get('/requests/{id}', 'RequestController@load');


Route::put('/request', 'RequestController@update');

Route::get('/schedule', 'ScheduleController@form');
Route::get('/schedules', 'ScheduleController@load');
// get one team.
//Route::get('/schedules/{id}', 'ScheduleController@load');
//Route::post('/schedule', 'ScheduleController@create');
Route::delete('/schedule/{id}', 'ScheduleController@delete');
Route::put('/schedule', 'ScheduleController@update');

