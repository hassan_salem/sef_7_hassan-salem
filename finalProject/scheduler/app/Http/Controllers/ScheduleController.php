<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Team;
use App\Timelabel;
use Illuminate\Support\Facades\Redirect;
use DB;
use Session;
use App\User;
class ScheduleController extends Controller
{
    use Authorizable, Helper;

    /**
     * sending users for schedule form.
     *
     * @param $teamId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function form($teamId) {
        $team = Team::find($teamId);
        $users = $team->users;

        return $this->authorizeRoute($team, 'team.schedule', ['users' => $users], 'company');
    }

    /**
     * create a new label.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addLabel(Request $request) {
        //return $request->all();
        $team = Team::find($request->teamId);
        $teamCompany = $team->company;
        if($this->isOwner($teamCompany)) {
            $label = new Timelabel();
            $label->label = $request->label;
            $label->timein = $request->timein;
            $label->duration = $request->duration;
            $label->team_id = $request->teamId;
            $label->save();
            Session::put('message', 'added successfuly');
            Session::put('class', ' green');
            return Redirect::to('/teams/' . $request->teamId) ;
        } else {
            return view('errors.403');
        }

    }

    /**
     * updating new label.
     *
     * @param Request $request
     * @return mixed
     */
    public function updateLabel(Request $request) {
        //return $request->all();
        $label = Timelabel::find($request->labelId);
        $label->label = $request->label;
        $label->timein = $request->timein;
        $label->duration = $request->duration;
        $label->save();
        return Redirect::to('/teams/' . $request->teamId);
    }


    /**
     * creating the schedule for one week
     * starting with the day sent by the user.
     *
     * @param Request $request
     */
    public function create(Request $request) {
        $rows = array();
        foreach($request->label as $userId => $label) {
            // array holds record for every day.
            $rows = [
                ['user_id'=>$userId, 'team_id'=>$request->teamId, 'timelabel_id'=>$label['MON'], 'date'=>date('Y-m-d', strtotime($request->scheduleDate))],
                ['user_id'=>$userId, 'team_id'=>$request->teamId, 'timelabel_id'=>$label['TUE'], 'date'=>date('Y-m-d', strtotime($request->scheduleDate. ' + 1 days'))],
                ['user_id'=>$userId, 'team_id'=>$request->teamId, 'timelabel_id'=>$label['WED'], 'date'=>date('Y-m-d', strtotime($request->scheduleDate. ' + 2 days'))],
                ['user_id'=>$userId, 'team_id'=>$request->teamId, 'timelabel_id'=>$label['THU'], 'date'=>date('Y-m-d', strtotime($request->scheduleDate. ' + 3 days'))],
                ['user_id'=>$userId, 'team_id'=>$request->teamId, 'timelabel_id'=>$label['FRI'], 'date'=>date('Y-m-d', strtotime($request->scheduleDate. ' + 4 days'))],
                ['user_id'=>$userId, 'team_id'=>$request->teamId, 'timelabel_id'=>$label['SAT'], 'date'=>date('Y-m-d', strtotime($request->scheduleDate. ' + 5 days'))],
                ['user_id'=>$userId, 'team_id'=>$request->teamId, 'timelabel_id'=>$label['SUN'], 'date'=>date('Y-m-d', strtotime($request->scheduleDate. ' + 6 days'))]
            ];

            DB::table('user_timelabel')->insert($rows);

        }

        Session::put('message', 'Created successfuly');
        Session::put('class', ' green');

        return Redirect::to('/companies');

    }

    /**
     * creating the schedule as form for adding or
     * as a table just for viewing
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createForm(Request $request) {
        if(!$request->scheduleDate){
            $request->scheduleDate = $this->getThisMondayDate();
        }
        $team = Team::find($request->teamId);

        $labels = [];
        if($team) {
            $users = $team->users;
            if(!$request->isForm) {
                foreach($users as $user) {
                    $x = DB::table('user_timelabel')
                        ->join('users', 'users.id', '=', 'user_timelabel.user_id')
                        ->join('teams', 'teams.id', '=', 'user_timelabel.team_id')
                        ->join('timelabels', 'timelabels.id', '=', 'user_timelabel.timelabel_id')
                        ->where('user_timelabel.team_id', '=', $request->teamId)
                        ->where('user_timelabel.user_id', '=', $user->id)
                        ->whereBetween('date', [date('Y-m-d', strtotime($request->scheduleDate)), date('Y-m-d', strtotime($request->scheduleDate. '+ 7 days'))])
                        ->get(['users.id', 'users.name', 'label', 'date', 'timelabel_id', 'user_timelabel.id as s_id']) ;

                    if($x) {
                        array_push($labels, $x);
                    }

                }

                    return view('team.scheduletable', ['labels' => $labels, 'scheduleDate' => $request->scheduleDate, 'teamId' => $request->teamId]);


                //return $this->authorizeRoute($team, 'team.scheduletable', ['labels' => $labels, 'scheduleDate' => $request->scheduleDate, 'teamId' => $request->teamId], 'company');
            } else {
                $labels = $team->timelabels;
                return $this->authorizeRoute($team, 'team.schedule', ['users' => $users, 'labels' => $labels, 'scheduleDate' => $request->scheduleDate, 'teamId' => $request->teamId], 'company');
            }
        }


    }


}
