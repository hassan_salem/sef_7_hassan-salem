<?php
/**
 * Created by PhpStorm.
 * User: applepc
 * Date: 6/14/16
 * Time: 4:34 PM
 */

namespace App\Http\Controllers;
use Auth;

trait Authorizable
{
    public function isOwner($object) {
        if($object) {
            if($object->user_id == Auth::user()->id) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }





    private function objectKey($object, $key) {
        if($key){
            return $object->$key;
        }
        return $object;
    }

    public function authorizeRoute($object, $view, $vars = [], $key=null) {
        if(isset($object)) {
            if($this->isOwner($this->objectKey($object, $key))) {
                return view($view, $vars);
            } else {
                return view('errors.403'); // not authorized
            }
        } else {
            return view('errors.404');
        }
    }
}