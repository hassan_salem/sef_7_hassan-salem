<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Code;

class CodeController extends Controller
{
    use Helper;

    public function create($numberOfCodes) {
        if($numberOfCodes <= 30) {
            $code = $this->generateRandomString();
            echo $code;
        }

    }
}
