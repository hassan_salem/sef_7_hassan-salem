<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\Team;

use App\Company;

use App\Timelabel;

use Auth;

use DB;

use Redirect;

use Session;

class TeamController extends Controller
{
    use MResponse, Authorizable;


    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * return the create new team form.
     *
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function form($id = null) {

        $companies = Auth::user()->companies()->get();

        if($id) {
            $team = Team::with('company')->find($id);
            return $this->authorizeRoute(
                $team,
                'team.form',
                ['team' => $team, 'companies' => $companies],
                'company'
            );
        } else {
            return view('team.form', ['companies' => $companies]);
        }
    }

    /**
     * load teams.
     *
     * @param $id
     * @param null $labelId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function load($id, $labelId=null) {
        $team = Team::find($id);
        if($team){
            $teamCompany = $team->company;
            $teamUsers = $team->users()->paginate();
            $companyUsers = [];
            $labels = $team->timelabels()->paginate();
            $label = [];

            if($labelId) {
                $labelData = Timelabel::find($labelId);
                if($labelData->team->id == $team->id) {
                    $label = $labelData;
                }
            }
            if($teamCompany){
                $companyUsers = $teamCompany->users()->whereNotIn('users.id', $team->users()->lists('users.id'))->paginate();
            }
            return $this->authorizeRoute($teamCompany, 'team.profile', ['users'=>$teamUsers, 'companyUsers' => $companyUsers, 'teamId' => $id, 'labels' => $labels, 'label' => $label]);
        }


    }

    /**
     * add user to the team
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addUser(Request $request) {
        $team = Team::find($request->teamId);
        if($team) {
            $teamCompany = $team->company;
            // check if company belongs to admin.
            if(Auth::user()->id == $teamCompany->user_id) {
                // check if user belongs to the admin company.
                if($teamCompany->users()->find($request->userId)) {
                    try{
                        DB::table('user_team')->insert(
                            [
                                'user_id' => $request->userId,
                                'team_id' => $request->teamId
                            ]
                        );
                        Session::put('message', 'Added successfuly');
                        Session::put('class', ' green');
                        return Redirect::to('/teams/' . $request->teamId);
                    } catch(QueryException $e) {

                    }
                } else {
                    return view('errors.403');
                }
            } else {
                return view('errors.403');
            }
        } else {
            return view('errors.404');
        }
    }

    /**
     * remove user from team.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function removeUser(Request $request) {
        $team = Team::find($request->teamId);
        if($team) {
            $teamCompany = $team->company;
            // check if company belongs to admin.
            if(Auth::user()->id == $teamCompany->user_id) {
                // check if user belongs to the admin company.
                if($teamCompany->users()->find($request->userId)) {
                    try{
                        echo  $team->users()->where('user_id', $request->userId)->where('team_id', $request->teamId)->get();
                        //return Redirect::to('/teams/' . $request->teamId);
                    } catch(QueryException $e) {

                    }
                } else {
                    return view('errors.403');
                }
            } else {
                return view('errors.403');
            }
        } else {
            return view('errors.404');
        }
    }

    /**
     * create new team.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request) {
        $company = Company::find($request->company);
        if($this->isOwner($company)) {
            $team = new Team();
            $team->name = $request->name;
            $team->company_id = $company->id;
            $team->location = $request->location;
            $team->save();
            Session::put('message', 'Created successfuly');
            Session::put('class', ' green');
            return Redirect::to('/team');
        } else {
            return $this->notAuthorize();
        }
    }

    /**
     * update team.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Request $request) {
        $team = Team::find($request->id)->first();
        $company = null;
        if($team) {
            $company = $team->company;
        }
        if($this->isOwner($company)) {
            $team->name = $request->name;
            $team->location = $request->location;
            $team->company_id = $request->company;
            $team->save();
            Session::put('message', 'Created successfuly');
            Session::put('class', ' green');
            return Redirect::to('/team/' . $team->id);
        } else {
            return $this->notAuthorize();
        }
    }

    /**
     * delete team.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete(Request $request) {
        $this->validate($request, ['deleteConfirmation' => 'required']);

        $team = Team::find($request->id);
        if($team) {
            $company = $team->company;
            if($this->isOwner($company)) {
                $team->delete();
                Session::put('message', 'Deleted successfuly');
                Session::put('class', ' green');
            } else {
                return $this->notAuthorize();
            }
        } else {
            return $this->notFound();
        }
    }
}
