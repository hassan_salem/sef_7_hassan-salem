<?php
/**
 * Created by PhpStorm.
 * User: applepc
 * Date: 6/15/16
 * Time: 2:07 PM
 */

namespace App\Http\Controllers;


trait Helper
{

    /**
     * Generate random string as access code.
     * @param int $length
     * @return string
     */
    function generateRandomString($length = 5) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * get the date of the current week monday.
     * @return bool|string
     */
    public function getThisMondayDate() {
        $week = date('W');
        $year = date('Y');
        $mondayOfThisWeek = date(
            'Y-m-d',
            strtotime($year . 'W' . str_pad($week, 2, '0', STR_PAD_LEFT))
        );
        return $mondayOfThisWeek;
    }
}