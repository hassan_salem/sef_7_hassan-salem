<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\Code;
use Mockery\CountValidator\Exception;
use Redirect;
use DB;
use Auth;
use Session;
class UserController extends Controller
{
    public function profile() {
        return view('user.profile');
    }

    public function code(Request $request) {
        $code = Code::where('code', $request->code)->first();
        if($code) {
            $company = $code->company;
            $codeString = $code->code;
            echo $codeString;

            try{
                DB::table('user_company')->insert([
                    'user_id' => Auth::user()->id,
                    'company_id' => $company->id
                ]);
                $code->delete();
            } catch (QueryException $e) {

            }
            Session::put('message', 'Added successfuly');
            Session::put('class', ' green');
            return Redirect::to('user');
        } else {
            return view('errors.404');
        }
    }

    public function getId() {
        return Auth::user()->id;
    }
    public function load($id = 0) {

    }

    public function create(Request $request) {

    }

    public function update(Request $request) {

    }

    public function delete($id) {

    }
}
