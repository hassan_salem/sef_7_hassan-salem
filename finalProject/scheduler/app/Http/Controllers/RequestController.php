<?php
namespace App\Http\Controllers;


use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use App\Request as R;
use Auth;
use Illuminate\Http\Request;
use DB;
use Redirect;
use Session;
class RequestController extends Controller
{
    use Authorizable;


    /**
     * get requests sent by the user.
     * or requests sent by specific user.
     *
     * @param null $userId
     * @return array
     */
    public function getRequests($userId = null) {
        /**
         * if user id provided
         * so return requests sent for this user.
         *
         * if not return requests sent by loged in user.
         */
        if($userId){
            $userRequests = DB::table('requests')
                ->where('requests.with_user_id', '=', $userId)
                ->join('users', 'users.id', '=', 'requests.user_id')
                ->get(['requests.user_id', 'requests.with_user_id', 'date', 'status', 'requests.created_at as cat', 'users.name', 'requests.id as rid']);
        } else {
            $userRequests = DB::table('requests')
                ->where('requests.user_id', '=', Auth::user()->id)
                ->join('users', 'users.id', '=', 'requests.with_user_id')
                ->get(['requests.user_id', 'requests.with_user_id', 'date', 'status', 'requests.created_at as cat', 'users.name', 'requests.id as rid']);
        }

        $result = array();

        foreach ($userRequests as $request) {
            $object = array();

            /**
             * get the initial label for the request.
             */
            $labelFrom = DB::table('user_timelabel')
                ->where('user_timelabel.user_id', '=', $request->user_id)
                ->where('user_timelabel.date', '=', $request->date)
                ->join('timelabels', 'timelabels.id', '=', 'user_timelabel.timelabel_id')
                ->get(['user_timelabel.id as sid', 'user_timelabel.timelabel_id', 'timelabels.label']);

            /**
             * get the aimed label.
             */
            $labelTo = DB::table('user_timelabel')
                ->where('user_timelabel.user_id', '=', $request->with_user_id)
                ->where('user_timelabel.date', '=', $request->date)
                ->join('timelabels', 'timelabels.id', '=', 'user_timelabel.timelabel_id')
                ->get(['user_timelabel.id as sid', 'user_timelabel.timelabel_id', 'timelabels.label']);

            /**
             * fill the results
             * in an associative array.
             */
            if($labelFrom && $labelTo){
                $object['date'] = $request->date;
                $object['labelFrom'] = $labelFrom[0]->label;
                $object['labelto'] = $labelTo[0]->label;
                $object['sidFrom'] = $labelFrom[0]->sid;
                $object['sidTo'] = $labelTo[0]->sid;
                $object['labelIdFrom'] = $labelFrom[0]->timelabel_id;
                $object['labelIdTo'] = $labelTo[0]->timelabel_id;
                $object['status'] = $request->status;
                $object['created_at'] = $request->cat;
                $object['request_id'] = $request->rid;
                $object['withuser'] = $request->name;
                array_push($result, $object);
            }

        }
        return $result;
    }

    /**
     * load the requests data
     * and send them to the user.requests view.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function load() {
        $myRequests = $this->getRequests();
        $requests = $this->getRequests(Auth::user()->id);

        return view('user.requests', ['myRequests' => $myRequests, 'requests'=> $requests]);
    }

    /**
     * create new request.
     * also insert a new record
     * in the firebase used in the notificatin.
     *
     * @param Input $input
     * @return mixed
     */
    public function create(Input $input) {

        $r = new R();
        $r->user_id = Auth::user()->id;
        $r->with_user_id = $input->get('user_id');
        $r->date=$input->get('date');

        $r->save();
        $FIREBASE = "https://schedul.firebaseio.com/";


        /**
         * create the record we need to send for firebase.
         */
        $data = array(
            "toId" => $input->get('user_id')
        );

        $json = json_encode( $data );
        $curl = curl_init();
        curl_setopt( $curl, CURLOPT_URL, $FIREBASE . 'requests' . $r->id . '.json' );
        curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, "PATCH" );
        curl_setopt( $curl, CURLOPT_POSTFIELDS, $json );
        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
        curl_exec( $curl );
        curl_close( $curl );
        Session::put('message', 'Sent successfuly');
        Session::put('class', ' green');
        return Redirect::to('/');
    }

    public function update(Request $request) {
//        echo "<pre>";
//        var_dump($request->all());
//        return;
        if($request->accept == 'true') {

//            echo $request->sidA . ' -> ' . $request->labelIdB ;
//            echo "<br>";
//            echo $request->sidB . ' -> ' . $request->labelIdA;
//            return;

//            $a1 = DB::table('user_timelabel')
//                ->where('id', '=', $request->sidA)->get();
//
//            $a2 = DB::table('user_timelabel')
//                ->where('id', '=', $request->sidB)->get();
//
//            echo "<pre>";
//            var_dump($a1);
//            echo ";;;;;;;";
//            var_dump($a2);
//
//
//            return;
            if($request->sidA && $request->sidB){

                DB::transaction(function() use($request) {

                    DB::table('user_timelabel')
                        ->where('id', '=', $request->sidB)
                        ->update(['timelabel_id' => $request->labelIdA]);

                    DB::table('user_timelabel')
                        ->where('id', '=', $request->sidA)
                        ->update(['timelabel_id' => $request->labelIdB]);


                });



            }

            $r = R::find($request->request_id);
            $r->status = '1';
            $r->save();
            return Redirect::to('/requests');

        } else {
            $r = R::find($request->request_id);
            $r->status = '-1';
            $r->save();
        }
        Session::put('message', 'Updated successfuly');
        Session::put('class', ' green');
       return Redirect::to('requests');
    }

    /**
     * delete request.
     *
     * @param Request $request
     * @return mixed
     */
    public function delete(Request $request) {
        $r = R::find($request->request_id);
        if($r) {
            $r->delete();
        }
        Session::put('message', 'Deleted successfuly');
        Session::put('class', ' green');
        return Redirect::to('/requests');
    }
}
