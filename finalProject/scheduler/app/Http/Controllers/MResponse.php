<?php
/**
 * Created by PhpStorm.
 * User: applepc
 * Date: 6/14/16
 * Time: 2:25 PM
 */

namespace App\Http\Controllers;
use Response;

trait MResponse
{

    /**
     * get data as json with code 200
     * @param $data
     * @return mixed
     */
    public function success($data) {
        return response()->json($data);
    }

    /**
     * return to 403 page
     * with no access.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function notAuthorize() {
        return view('errors.403');
    }

    /*
     * return for 404 page
     * not found
     */
    public function notFound() {
        return view('errors.404');
    }
}