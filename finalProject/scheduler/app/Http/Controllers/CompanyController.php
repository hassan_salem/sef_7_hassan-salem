<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\Input;

use App\Company;

use Auth;

use Redirect;
use Session;
use App\Code;

class CompanyController extends Controller
{
    use MResponse, Authorizable, Helper;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * validation for company
     * used for edit and create.
     *
     * @param $request
     */
    public function companyValidate($request) {
        $this->validate(
            $request,
            array(
                'name' => 'required',
                'description' => 'max:1000'
            )
        );
    }


    /**
     * create access codes for companies.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function codes(Request $request) {
        $company = Company::find($request->company);
        if($company) {
            // check if this company belong to the authenticated user.
            if($company->user_id == Auth::user()->id){
                if($request->numberOfCodes > 30)
                    $request->numberOfCodes = 30;
                // rows to be inserted in the DB
                $rows = array();
                for($i = 1 ; $i <= $request->numberOfCodes; $i++){
                    array_push(
                        $rows,
                        ['code' => $this->generateRandomString(6), 'company_id'=>$request->company]
                    );
                }
                Code::insert($rows);
                Session::put('message', 'Generated successfuly');
                Session::put('class', ' green');
                return Redirect::to('/companies/' . $request->company);
            } else {
                return view('errors.403');
            }
        } else {
            return view('errors.404');
        }

    }


    /**
     * create the view containing the form
     * for edit and for ceate a company.
     *
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function form($id = null) {
        if($id) {
            $company = Company::find($id);
            return $this->authorizeRoute($company, 'company.form', ['company' => $company]);
        } else {
            return view('company.form');
        }
    }

    /**
     * load company profile.
     * or companies table.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function load($id = 0) {
        if($id) {
            $company = Company::find($id);
            $codes = $company->codes()->paginate();
            $users = $company->users;
            $teams = $company->teams()->paginate();
            return $this->authorizeRoute($company, 'company.profile',
                ['company' => $company, 'codes' => $codes, 'users' => $users, 'teams' => $teams]);
        } else {
            $companies = Auth::user()->companies()->paginate();
            return view('company.table', ['companies' => $companies]);
        }
    }

    /**
     * create new company.
     *
     * @param Request $request
     * @return mixed
     */
    public function create(Request $request) {

        $this->companyValidate($request);
        // 100,000, image/jpeg, image/jpg, image/png

        $company = new Company();
        $company->name = $request->name;
        $company->description = $request->description;
        if(Input::file('logo')->isValid() &&
            Input::file('logo')->getSize() <= 100000 &&
            (Input::file('logo')->getMimeType() == 'image/png' ||
                Input::file('logo')->getMimeType() == 'image/jpg' ||
                Input::file('logo')->getMimeType() == 'image/jpeg')) {

            $destinationPath = 'uploads'; // upload path
            $extension = Input::file('logo')->getClientOriginalExtension();
            $fileName = rand(111111,99999999).'.'.$extension;
            Input::file('logo')->move($destinationPath, $fileName);
            $company->logo = $destinationPath . '/' . $fileName;
            //Session::flash('success', 'added');

        } else {

        }
        $company->user_id = Auth::user()->id;

        $company->save();
        Session::put('message', 'Created');
        Session::put('class', ' green');
        return Redirect::to('/companies');

    }

    /**
     * Update company record.
     *
     * @param Request $request
     * @return mixed
     */
    public function update(Request $request) {
        $this->companyValidate($request);
        $company = Company::find($request->id);
        if($this->isOwner($company)) {
            $company->name = $request->name;
            $company->description = $request->description;
            $company->save();
            Session::put('message', 'Updated successfuly');
            Session::put('class', ' green');
            return Redirect::to('/companies');
        } else {
            Session::put('message', 'no access');
            Session::put('class', ' red');
            return Redirect::to('/company');
        }

    }

    public function delete($id) {

    }
}
