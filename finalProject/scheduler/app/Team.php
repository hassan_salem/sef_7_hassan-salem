<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    public function company() {
        return $this->belongsTo('App\Company');
    }

    public function users() {
        return $this->belongsToMany('App\User', 'user_team');
    }

    public function timelabels() {
        return $this->hasMany('App\Timelabel');
    }
}
