@extends('layouts.app')

@section('content')
    <!-- form grid slider -->
    <div class="x_panel" style="">
        <div class="x_title">
            <h2>Schedule</h2>

            <div class="clearfix"></div>
        </div>
        <div class="x_content">

            <form class="form-horizontal form-label-left" enctype="multipart/form-data" method="POST" action="{{ url('/company') }}">
                {!! csrf_field() !!}

                <div  class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="control-label col-md-3 col-sm-3 col-xs-3">Name</label>
                    <div class="col-md-9 col-sm-9 col-xs-9">
                        <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                        @if ($errors->has('name'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>

            </form>

        </div>
    </div>
    <br />
    <br />
    <!-- /form grid slider -->



@endsection