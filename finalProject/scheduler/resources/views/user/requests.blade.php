@extends('layouts.app')

@section('content')
    <link href="{{asset('vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css')}}" rel="stylesheet">
    <div class="">
        <div class="page-title">

        </div>

        <div class="clearfix"></div>

        <div class="row">


            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Recieved requests <small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a>
                                    </li>
                                    <li><a href="#">Settings 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        @if(count($requests) > 0 )
                            <table id="datatable-fixed-header" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>From</th>
                                    <th>To</th>
                                    <th>Date</th>
                                    <th>Sent from</th>
                                    <th></th>
                                </tr>
                                </thead>


                                <tbody>

                                @foreach($requests as $request)
                                    <tr class="@if($request['status'] == '-1') danger @elseif($request['status'] == 1) success @endif">

                                        <td>{{$request['labelFrom']}}</td>
                                        <td>{{$request['labelto']}}</td>
                                        <td>{{$request['date']}}</td>
                                        <td>{{$request['withuser']}}</td>
                                        <td>
                                            @if($request['status'] == '-1')
                                                Rejected
                                            @elseif($request['status'] == '1')
                                                Accepted
                                            @else
                                                <form method="post" action="{{url('/request')}}">
                                                    {{csrf_field()}}
                                                    <input type="hidden" name="_method" value="PUT">
                                                    <input type="hidden" name="accept" value="true">
                                                    <input type="hidden" name="request_id" value="{{$request['request_id']}}" >

                                                    <input type="hidden" name="sidA" value="{{$request['sidFrom']}}"/>
                                                    <input type="hidden" name="labelIdA" value="{{$request['labelIdFrom']}}"/>
                                                    <input type="hidden" name="sidB" value="{{$request['sidTo']}}"/>
                                                    <input type="hidden" name="labelIdB" value="{{$request['labelIdTo']}}"/>

                                                    <input type="hidden" name="timelabel_id" value=""/>
                                                    <input type="hidden" name="suserid" value=""/>
                                                    <button type="submit" class="btn-link">Accept <i class="fa fa-check"></i></button>
                                                </form>

                                                <form method="post" action="{{url('/request')}}">
                                                    {{csrf_field()}}
                                                    <input type="hidden" name="_method" value="PUT">
                                                    <input type="hidden" name="request_id" value="{{$request['request_id']}}" >
                                                    <input type="hidden" name="accept" value="false">
                                                    <button type="submit" class="btn-link">Reject <i class="fa fa-remove"></i></button>
                                                </form>
                                            @endif

                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        @else
                            <h2>No requests found.. !</h2>
                        @endif

                    </div>
                </div>
            </div>

            <!-- ---------------------------------------- -->

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Sent requests <small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a>
                                    </li>
                                    <li><a href="#">Settings 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        @if(count($myRequests) > 0 )
                        <table id="datatable-fixed-header" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>From</th>
                                <th>Date</th>
                                <th>Time Label</th>
                                <th>Sent from</th>
                                <th></th>
                            </tr>
                            </thead>


                            <tbody>

                            @foreach($myRequests as $myRequest)
                                <tr class="@if($myRequest['status'] == '-1') danger @elseif($myRequest['status'] == 1) success @endif">
                                    <td>{{$myRequest['withuser']}}</td>
                                    <td>{{$myRequest['date']}}</td>
                                    <td>{{$myRequest['labelto']}}</td>
                                    <td>{{$myRequest['created_at']}}</td>
                                    <td>
                                        @if($myRequest['status'] == '-1')
                                            Rejected
                                        @elseif($myRequest['status'] == '1')
                                            Accepted
                                        @else
                                            <form method="POST" action="{{url('/request')}}">
                                                {{csrf_field()}}
                                                <input type="hidden" name="_method" value="DELETE"/>
                                                <input type="hidden" name="request_id" value="{{$myRequest['request_id']}}">
                                                <button type="submit" class="btn-link">Delete <i class="fa fa-remove"></i></button>
                                            </form>
                                        @endif

                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        @else
                            <h2>No requests found.. !</h2>
                        @endif

                    </div>
                </div>
            </div>


        </div>
    </div>

@endsection