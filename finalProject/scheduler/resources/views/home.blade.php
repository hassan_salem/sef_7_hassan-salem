@extends('layouts.app')

@section('content')
    <script src="{{asset('js/bootstrap-datepicker.en-AU.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
    <link href="{{asset('build/css/bootstrap-datepicker.css')}}" rel="stylesheet">

    @if(count($teams) > 0)
    <form class="form-horizontal form-label-left" enctype="multipart/form-data" method="POST" action="{{ url('/schedule') }}">
        {!! csrf_field() !!}
        <div class="form-group">
            <input type="hidden" value="0" name="isForm" >
        </div>
        <div class="form-group">
            <select name="teamId" class="form-control">
                @foreach($teams as $team)
                    <option value="{{$team->id}}">{{$team->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <div class="col-md-3">
                <input readonly placeholder="Click to select date" type="text" name="scheduleDate" class="form-control" id="scheduleDate">
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-9">
                <button class="btn btn-success"> SUBMIT </button>
            </div>
        </div>
    </form>
    <script>
        $(document).ready(function() {
            $('#scheduleDate').datepicker({
                daysOfWeekDisabled: "0,2,3,4,5,6",
                daysOfWeekHighlighted: "1",
                todayHighlight: true
            });
        });
    </script>
    @else
        <h1>You are not member of any team.</h1>
    @endif
@endsection
