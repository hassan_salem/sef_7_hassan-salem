@extends('layouts.app')

@section('content')
    <link href="{{asset('vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css')}}" rel="stylesheet">
    <div class="">
        <div class="page-title">

        </div>

        <div class="clearfix"></div>

        <div class="row">


            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Companies <small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a>
                                    </li>
                                    <li><a href="#">Settings 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <table id="datatable-fixed-header" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Logo</th>
                                <th>Description</th>
                                <th></th>
                            </tr>
                            </thead>


                            <tbody>

                            @foreach($companies as $company)
                            <tr>
                                <td><a href="{{url('/companies/' . $company->id)}}">{{$company->name}}</a></td>
                                <td><img src="{{$company->logo}}" width="32" height="32"/></td>
                                <td>{{$company->description}}</td>
                                <td>
                                    <a href="{{url('/companies/' . $company->id)}}">
                                        <strong> View </strong>
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    <a href="{{url('/company/' . $company->id)}}">
                                        <strong>Edit</strong>
                                        <i class="fa fa-edit"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach

                            </tbody>
                        </table>
                        {{$companies->links()}}
                    </div>
                </div>
            </div>


        </div>
    </div>

@endsection