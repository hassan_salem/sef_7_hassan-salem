@extends('layouts.app')

@section('content')
    <br><br><br>
<div class="" role="tabpanel" data-example-id="togglable-tabs">
    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
        <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Company Teams</a>
        </li>
        <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Codes</a>
        </li>
        <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Users</a>
        </li>
    </ul>

    <div id="myTabContent" class="tab-content">
        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">

            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Location</th>
                    <th>...</th>
                </tr>
                </thead>
                <tbody>
                @if ($errors->has('deleteConfirmation'))
                    <span class="help-block alert-danger">
                        <strong>{{ $errors->first('deleteConfirmation') }}</strong>
                    </span>
                @endif
                @foreach($teams as $team)
                    <tr>
                        <td>{{$team->id}}</td>
                        <td><a href="{{url('/teams/' . $team->id)}}">{{$team->name}}</a></td>
                        <td>{{$team->location}}</td>
                        <td>
                            <a href="{{url('/team/' . $team->id)}}">
                                <strong>Edit</strong>
                                <i class="fa fa-edit"></i>
                            </a>
                            <form method="post" action="{{url('/team')}}">
                                {{csrf_field()}}
                                <input type="hidden" name="_method" value="DELETE" />
                                <input type="hidden" name="id" value="{{$team->id}}"/>
                                <label><input type="checkbox" name="deleteConfirmation"> confirm delete</label>
                                <button type="submit" class="btn-link"> Delete<i class="fa fa-remove"></i> </button>

                            </form>
                            <a href="{{url('/teams/' . $team->id)}}">
                                <strong>View</strong>
                                <i class="fa fa-eye"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach


                </tbody>
            </table>
            {{$teams->links()}}
        </div>
        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">

            <form class="form-horizontal form-label-left" enctype="multipart/form-data" method="POST" action="{{ url('/codes') }}">
                {!! csrf_field() !!}

                <input type="hidden" value="{{$company->id}}" name="company" />
                <div  class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="control-label col-md-2 col-sm-2 col-xs-2">Generate : </label>
                    <div class="col-md-8 col-sm-8 col-xs-8">
                        <select name="numberOfCodes" class="form-control">
                            <option value="1">1 code</option>
                            <option value="5">5 codes</option>
                            <option value="10">10 codes</option>
                            <option value="15">15 code</option>
                            <option value="20">20 codes</option>
                            <option value="25">25 code</option>
                            <option value="30">30 codes</option>
                        </select>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-2">
                        <button class="btn btn-success">Generate</button>
                    </div>
                </div>

            </form>
            <!-- start user projects -->
            <table class="data table table-striped no-margin">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Code</th>
                </tr>
                </thead>
                <tbody>
                @foreach($codes as $code)
                    <tr>
                        <td>{{$code->id}}</td>
                        <td>{{$code->code}}</td>
                    </tr>
                @endforeach


                </tbody>
            </table>
            {{$codes->links()}}
            <!-- end user projects -->

        </div>
        <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
            <table class="data table table-striped no-margin">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Created </th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{$user->id}}</td>
                        <td>{{$user->name}}</td>
                        <th>
                            {{$user->created_at->diffForHumans()}}
                        </th>
                    </tr>
                @endforeach


                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection