@extends('layouts.app')

@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Company</h2>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br />
                <form class="form-horizontal form-label-left" enctype="multipart/form-data" method="POST" action="{{ url('/company') }}">
                    {!! csrf_field() !!}
                    @if(!empty($company))
                        <input name="_method" type="hidden" value="PUT">
                        <input type="hidden" name="id" value="{{$company->id}}" >
                    @endif
                    <div  class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Name</label>
                        <div class="col-md-9 col-sm-9 col-xs-9">
                            <input type="text" name="name" class="form-control" value="{{{$company->name or ''}}}{{ old('name') }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }}">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Logo</label>
                        <div class="col-md-9 col-sm-9 col-xs-9">
                            <input type="file" name="logo" class="form-control"  value="{{ old('logo') }}">
                            @if ($errors->has('logo'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('logo') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <!-- description -->
                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Description</label>
                        <div  class="col-md-9 col-sm-9 col-xs-9">
                            <textarea class="form-control" name="description">{{{$company->description or ''}}}{{ old('description') }}</textarea>
                            @if ($errors->has('description'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div> <!-- end of form group description -->


                    <div class="ln_solid"></div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-primary">Cancel</button>
                            <button class="btn btn-success" ng-click="create()" >Submit</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection