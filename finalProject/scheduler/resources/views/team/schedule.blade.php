@extends('layouts.app')

@section('content')
    <!-- form grid slider -->
    <div class="x_panel" style="">
        <div class="x_title">
            <h2>Schedule</h2>

            <div class="clearfix"></div>
        </div>
        <div class="x_content table-responsive">


            <form class="form-horizontal form-label-left" enctype="multipart/form-data" method="POST" action="{{ url('/newSchedule') }}">
                {!! csrf_field() !!}
                <input type="hidden" name="scheduleDate" value="{{$scheduleDate}}" />
                <input type="hidden" name="teamId" value="{{$teamId}}" />
                <table class="table table-striped table-bordered">
                    <thead>
                        <th>Name</th>
                        <th>MON</th>
                        <th>TUE</th>
                        <th>WED</th>
                        <th>THU</th>
                        <th>FRI</th>
                        <th>SAT</th>
                        <th>SUN</th>
                    </thead>
                    @foreach($users as $user)
                    <tbody>
                        <td>{{$user->name}}</td>
                        <td>

                            <select name="label[{{$user->id}}][MON]" class="form-control">
                                @foreach($labels as $label)
                                    <option value="{{$label->id}}">{{$label->label}}</option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <select name="label[{{$user->id}}][TUE]" class="form-control">
                                @foreach($labels as $label)
                                    <option value="{{$label->id}}">{{$label->label}}</option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <select name="label[{{$user->id}}][WED]" class="form-control">
                                @foreach($labels as $label)
                                    <option value="{{$label->id}}">{{$label->label}}</option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <select name="label[{{$user->id}}][THU]" class="form-control">
                                @foreach($labels as $label)
                                    <option value="{{$label->id}}">{{$label->label}}</option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <select name="label[{{$user->id}}][FRI]" class="form-control">
                                @foreach($labels as $label)
                                    <option value="{{$label->id}}">{{$label->label}}</option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <select name="label[{{$user->id}}][SAT]" class="form-control">
                                @foreach($labels as $label)
                                    <option value="{{$label->id}}">{{$label->label}}</option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <select name="label[{{$user->id}}][SUN]" class="form-control">
                                @foreach($labels as $label)
                                    <option value="{{$label->id}}">{{$label->label}}</option>
                                @endforeach
                            </select>
                        </td>
                    </tbody>
                    @endforeach
                </table>

                <div class="ln_solid"></div>

                <div class="form-group">
                    <div class="col-md-9">
                        <button class="btn btn-success" ng-click="create()" >Submit</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
    <br />
    <br />
    <!-- /form grid slider -->



@endsection