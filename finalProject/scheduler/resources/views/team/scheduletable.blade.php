@extends('layouts.app')

@section('content')
        <!-- form grid slider -->
<div class="x_panel" style="">
    <div class="x_title">
        <h2>Schedule: {{$scheduleDate}}</h2>

        <div class="clearfix"></div>
    </div>
    <div class="x_content table-responsive">

        <div class="form-horizontal form-label-left table-responsive">
            {!! csrf_field() !!}
            <input type="hidden" name="scheduleDate" value="{{$scheduleDate}}" />
            <input type="hidden" name="teamId" value="{{$teamId}}" />
            @if($labels)
            <table class="table table-striped table-bordered">
                <thead>
                <th>Name</th>
                <th>MON {{ Carbon\Carbon::parse($scheduleDate)->format('d') }}</th>
                <th>TUE {{ Carbon\Carbon::parse($scheduleDate . ' + 1 days')->format('d') }}</th>
                <th>WED {{ Carbon\Carbon::parse($scheduleDate . ' + 2 days')->format('d') }}</th>
                <th>THU {{ Carbon\Carbon::parse($scheduleDate . ' + 3 days')->format('d') }}</th>
                <th>FRI {{ Carbon\Carbon::parse($scheduleDate . ' + 4 days')->format('d') }}</th>
                <th>SAT {{ Carbon\Carbon::parse($scheduleDate . ' + 5 days')->format('d') }}</th>
                <th>SUN {{ Carbon\Carbon::parse($scheduleDate . ' + 6 days')->format('d') }}</th>
                </thead>
                @foreach($labels as $label)
                    <tbody>
                    <td>{{$label[0]->name}}</td>
                    <td class="text-center text-capitalize text-danger">
                        {{$label[0]->label or 'TA'}}
                        @if(Auth::user()->id != $label[0]->id)
                            <form method="post" action="{{url('/request')}}">
                                {{csrf_field()}}
                                <input type="hidden" name="user_id" value="{{$label[0]->id}}">
                                <input type="hidden" name="user_timelabel_id" value="{{$label[0]->s_id}}" />
                                <input type="hidden" name="date" value="{{$label[0]->date}}" />
                                <button class="btn-link" title="swap with"><i class="fa fa-forward"></i></button>
                            </form>
                        @endif
                    </td>
                    <td class="text-center text-capitalize text-danger">
                        {{$label[1]->label or 'TA'}}
                        @if(Auth::user()->id != $label[0]->id)

                            <form method="post" action="{{url('/request')}}">
                                {{csrf_field()}}
                                <input type="hidden" name="user_id" value="{{$label[0]->id}}">
                                <input type="hidden" name="user_timelabel_id" value="{{$label[1]->s_id}}" />
                                <input type="hidden" name="date" value="{{$label[1]->date}}" />
                                <button class="btn-link" title="swap with"><i class="fa fa-forward"></i></button>
                            </form>
                        @endif
                    </td>
                    <td class="text-center text-capitalize text-danger">
                        {{$label[2]->label or 'TA'}}
                        @if(Auth::user()->id != $label[0]->id)
                            <form method="post" action="{{url('/request')}}">
                                {{csrf_field()}}
                                <input type="hidden" name="user_id" value="{{$label[0]->id}}">
                                <input type="hidden" name="user_timelabel_id" value="{{$label[2]->s_id}}" />
                                <input type="hidden" name="date" value="{{$label[2]->date}}" />
                                <button class="btn-link" title="swap with"><i class="fa fa-forward"></i></button>
                            </form>
                        @endif
                    </td>
                    <td class="text-center text-capitalize text-danger">
                        {{$label[3]->label or 'TA'}}
                        @if(Auth::user()->id != $label[0]->id)
                            <form method="post" action="{{url('/request')}}">
                                {{csrf_field()}}
                                <input type="hidden" name="user_id" value="{{$label[0]->id}}">
                                <input type="hidden" name="user_timelabel_id" value="{{$label[3]->s_id}}" />
                                <input type="hidden" name="date" value="{{$label[3]->date}}" />
                                <button class="btn-link" title="swap with"><i class="fa fa-forward"></i></button>
                            </form>
                        @endif
                    </td>
                    <td class="text-center text-capitalize text-danger">
                        {{$label[4]->label or 'TA'}}
                        @if(Auth::user()->id != $label[0]->id)
                            <form method="post" action="{{url('/request')}}">
                                {{csrf_field()}}
                                <input type="hidden" name="user_id" value="{{$label[0]->id}}">
                                <input type="hidden" name="user_timelabel_id" value="{{$label[4]->s_id}}" />
                                <input type="hidden" name="date" value="{{$label[4]->date}}" />
                                <button class="btn-link" title="swap with"><i class="fa fa-forward"></i></button>
                            </form>
                        @endif
                    </td>
                    <td class="text-center text-capitalize text-danger">
                        {{$label[5]->label or 'TA'}}
                        @if(Auth::user()->id != $label[0]->id)
                            <form method="post" action="{{url('/request')}}">
                                {{csrf_field()}}
                                <input type="hidden" name="user_id" value="{{$label[0]->id}}">
                                <input type="hidden" name="user_timelabel_id" value="{{$label[5]->s_id}}" />
                                <input type="hidden" name="date" value="{{$label[5]->date}}" />
                                <button class="btn-link" title="swap with"><i class="fa fa-forward"></i></button>
                            </form>
                        @endif
                    </td>
                    <td class="text-center text-capitalize text-danger">
                        {{$label[6]->label or 'TA'}}
                        @if(Auth::user()->id != $label[0]->id)
                            <form method="post" action="{{url('/request')}}">
                                {{csrf_field()}}
                                <input type="hidden" name="user_id" value="{{$label[0]->id}}">
                                <input type="hidden" name="user_timelabel_id" value="{{$label[6]->s_id}}" />
                                <input type="hidden" name="date" value="{{$label[6]->date}}" />
                                <button class="btn-link" title="swap with"><i class="fa fa-forward"></i></button>
                            </form>
                        @endif
                    </td>
                    </tbody>
                @endforeach
            </table>
            @else
                <h2>There is no schedule for this date yet</h2>
            @endif

            <div class="ln_solid"></div>

        </div>

    </div>
</div>
<br />
<br />
<!-- /form grid slider -->



@endsection