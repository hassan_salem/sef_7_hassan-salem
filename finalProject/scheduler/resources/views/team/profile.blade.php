@extends('layouts.app')

@section('content')
    <script src="{{asset('js/bootstrap-datepicker.en-AU.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
    <link href="{{asset('build/css/bootstrap-datepicker.css')}}" rel="stylesheet">
    <br><br><br>
    <div class="" role="tabpanel" data-example-id="togglable-tabs">
        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
            <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Team Schedules</a>
            </li>
            <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Users</a>
            </li>
            <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Add</a>
            </li>
            <li role="presentation" class=""><a href="#tab_content4" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Labels</a>
            </li>
        </ul>

        <div id="myTabContent" class="tab-content">
            <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                <form class="form-horizontal form-label-left" enctype="multipart/form-data" method="POST" action="{{ url('/schedule') }}">
                    {!! csrf_field() !!}
                    <input type="hidden" name="teamId" value="{{$teamId}}" />
                    <div class="form-group">
                        <input type="radio" value="0" name="isForm" > View Schedule<br>
                        <input type="radio" value="1" name="isForm" > Create Schedule</br>
                    </div>
                    <div class="form-group">
                        <div class="col-md-3">
                            <input readonly placeholder="Click to select date" type="text" name="scheduleDate" class="form-control" id="scheduleDate">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-9">
                            <button class="btn btn-success"> SUBMIT </button>
                        </div>
                    </div>
                </form>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Location</th>
                        <th>...</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{$user->id}}</td>
                            <td>{{$user->name}}</td>
                            <td></td>
                            <td>
                                <form action="{{url('/teamUser')}}" method="POST">
                                    {{csrf_field()}}
                                    <input name="_method" type="hidden" value="DELETE">
                                    <input type="hidden" name="userId" value="{{$user->id}}">
                                    <input type="hidden" name="teamId" value="{{$teamId}}">
                                    <button class="btn btn-success">Add</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach


                    </tbody>
                </table>
                {{$users->links()}}

            </div>
            <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>...</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($companyUsers as $cUser)
                        <tr>
                            <td>{{$cUser->id}}</td>
                            <td>{{$cUser->name}}</td>
                            <td>{{$cUser->email}}</td>
                            <td>
                                <form action="{{url('/teamUser')}}" method="POST">
                                    {{csrf_field()}}
                                    <input type="hidden" name="userId" value="{{$cUser->id}}">
                                    <input type="hidden" name="teamId" value="{{$teamId}}">
                                    <button class="btn btn-success">Add</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach


                    </tbody>
                </table>
                {{$companyUsers->links()}}
            </div>

            <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="profile-tab">
                <form class="form-horizontal form-label-left" enctype="multipart/form-data" method="POST" action="{{ url('/label') }}">
                    {!! csrf_field() !!}
                    @if($label)
                        <input type="hidden" value="{{$label->id}}" name="labelId">
                        <input type="hidden" name="_method" value="PUT" />
                    @endif
                    <input type="hidden" name="teamId" value="{{$teamId}}"/>
                    <div  class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2 col-sm-2 col-xs-2">Label</label>
                        <div class="col-md-9 col-sm-9 col-xs-9">
                            <input type="text" name="label" class="form-control" value="{{$label->label or ''}}{{ old('label') }}">
                            @if ($errors->has('label'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('label') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div  class="form-group{{ $errors->has('timein') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2 col-sm-2 col-xs-2">Time In</label>
                        <div class="col-md-9 col-sm-9 col-xs-9">
                            <input type="time" name="timein" class="form-control" value="{{$label->timein or ''}}{{ old('timein') }}">
                            @if ($errors->has('timein'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('timein') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div  class="form-group{{ $errors->has('duration') ? ' has-error' : '' }}">
                        <label class="control-label col-md-2 col-sm-2 col-xs-2">Duration</label>
                        <div class="col-md-9 col-sm-9 col-xs-9">
                            <input type="number" name="duration" class="form-control" value="{{$label->duration or ''}}{{ old('duration') }}">
                            @if ($errors->has('duration'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('duration') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="ln_solid"></div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-primary">Cancel</button>
                            <button class="btn btn-success" ng-click="create()" >Submit</button>
                        </div>
                    </div>
                </form>
                <div class="ln_solid"></div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Label</th>
                        <th>Time In</th>
                        <th>Duration</th>
                        <th>...</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($labels as $label)
                        <tr>
                            <td>{{$label->label}}</td>
                            <td>{{$label->timein}}</td>
                            <td>{{$label->duration}}</td>
                            <td>
                                <a href="{{url('/teams/' . $teamId . '/' . $label->id)}}">Edit</a>
                            </td>
                        </tr>
                    @endforeach


                    </tbody>
                </table>
                {{$labels->links()}}
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#scheduleDate').datepicker({
                daysOfWeekDisabled: "0,2,3,4,5,6",
                daysOfWeekHighlighted: "1",
                todayHighlight: true
            });
        });
    </script>
@endsection