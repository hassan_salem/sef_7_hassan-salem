@extends('layouts.app')

@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Company</h2>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br />
                <form class="form-horizontal form-label-left" enctype="multipart/form-data" method="POST" action="{{ url('/team') }}">
                    {!! csrf_field() !!}
                    @if(!empty($team))
                        <input name="_method" type="hidden" value="PUT">
                        <input type="hidden" name="id" value="{{$team->id}}" >
                    @endif
                    <div  class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Name</label>
                        <div class="col-md-9 col-sm-9 col-xs-9">
                            <input type="text" name="name" class="form-control" value="{{{ $team->name or ''}}}{{ old('name') }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('company') ? ' has-error' : '' }}">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Company</label>
                        <div class="col-md-9 col-sm-9 col-xs-9">
                            <select type="file" name="company" class="form-control">
                                @foreach($companies as $company)
                                    <option value="{{$company->id}}">{{$company->name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('company'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('company') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <!-- description -->
                    <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Location</label>
                        <div  class="col-md-9 col-sm-9 col-xs-9">
                            <textarea class="form-control" name="location">{{{$team->location or ''}}}{{ old('location') }}</textarea>
                            @if ($errors->has('location'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('location') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div> <!-- end of form group description -->


                    <div class="ln_solid"></div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-primary">Cancel</button>
                            <button class="btn btn-success" ng-click="create()" >Submit</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection