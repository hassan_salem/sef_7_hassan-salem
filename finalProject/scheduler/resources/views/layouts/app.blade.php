<!DOCTYPE html>
<html lang="en"  ng-app="Scheduler" >
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Scheduler</title>

    <!-- Bootstrap -->
    <link href="{{asset('vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{asset('vendors/iCheck/skins/flat/green.css')}}" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="{{asset('vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet">
    <!-- jQuery -->
    <script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
    <!-- Custom Theme Style -->
    <link href="{{asset('build/css/custom.min.css')}}" rel="stylesheet">




</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="{{url('/')}}" class="site_title"><i class="fa fa-table"></i> <span>Schedule!</span></a>
                </div>

                <div class="clearfix"></div>
                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>Administration</h3>
                        <ul class="nav side-menu">
                            <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{url('/')}}">Dashboard</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-plus"></i> New <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{url('/company')}}">Company</a></li>
                                    <li><a href="{{url('/team')}}">Team</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-eye"></i> Show <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{url('/companies')}}">Companies</a></li>
                                    <li><a href="{{url('/requests')}}">Requests</a></li>
                                </ul>
                            </li>

                        </ul>
                    </div>

                </div>
                <!-- /sidebar menu -->


            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    @if(Session::has('flash'))

                        <div id="message" class="nav toggle" style=" width: 150px;font-weight: bold; color: {{Session::pull('class')}}">
                            {{Session::pull('message') }}
                        </div>
                    @endif



                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="images/img.jpg" alt="">{{Auth::user()->name}}
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="{{url('/user')}}"> Profile</a></li>
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                            </ul>
                        </li>

                        <li role="presentation" class="dropdown">
                            <a href="{{url('/requests')}}" class="dropdown-toggle info-number" aria-expanded="false">
                                <i class="fa fa-send-o"></i>
                                <span id="requestsCount" class="badge bg-green">0</span>
                            </a>

                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                @yield('content')
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->


        <footer>
            <div class="pull-right">
                Scheduler
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>


<!-- Bootstrap -->
<script src="{{asset('vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('vendors/fastclick/lib/fastclick.js')}}"></script>
<!-- NProgress -->
<script src="{{asset('vendors/nprogress/nprogress.js')}}"></script>
<!-- Chart.js -->
<script src="{{asset('vendors/Chart.js/dist/Chart.min.js')}}"></script>

<!-- bootstrap-progressbar -->
<script src="{{asset('vendors/bootstrap-progressbar/bootstrap-progressbar.min.js')}}"></script>
<!-- iCheck -->
<script src="{{asset('vendors/iCheck/icheck.min.js')}}"></script>


<!-- bootstrap-daterangepicker -->
<script src="{{asset('js/moment/moment.min.js')}}"></script>
<script src="{{asset('js/datepicker/daterangepicker.js')}}"></script>

<!-- Custom Theme Scripts -->
<script src="{{asset('build/js/custom.min.js')}}"></script>
<script src="https://cdn.firebase.com/js/client/2.4.2/firebase.js"></script>
<audio id="myAudio">
    <source src="notify.mp3" type="audio/mpeg">
    Your browser does not support the audio element.
</audio>
<script>

    $(document).ready(function() {
        var myFirebaseRef = new Firebase("https://schedul.firebaseio.com/");
        var userId = 0;
        $.get('userId', function(userId) {
            console.log('started');
            myFirebaseRef.limitToLast(1).on('child_added', function(snapshot) {
                var data = snapshot.val();
                console.log(snapshot.val());
                if(data.toId == userId) {
                    myFirebaseRef.child(snapshot.key()).remove();
                    console.log('removed');
                    $('#requestsCount').text((parseInt($('#requestsCount').text()) + 1) + '');
                    var x = document.getElementById("myAudio");
                    x.play();
                }
            });
        });

        setTimeout( function() {
            $('#message').fadeOut("slow");
        }, 3000 );

    });
</script>
</body>
</html>