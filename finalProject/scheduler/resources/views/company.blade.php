@extends('layouts.app')

@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Company</h2>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br />
                <form class="form-horizontal form-label-left">

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Name</label>
                        <div class="col-md-9 col-sm-9 col-xs-9">
                            <input type="text" name="name" class="form-control" ng-model="formData.name">
                            <span class="fa fa-building form-control-feedback right" aria-hidden="true"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Logo</label>
                        <div class="col-md-9 col-sm-9 col-xs-9">
                            <input type="file" name="logo" class="form-control" ng-file="formData.file">
                            <span class="fa fa-picture-o form-control-feedback right" aria-hidden="true"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Type</label>
                        <div class="col-md-9 col-sm-9 col-xs-9">
                            <select class="form-control"  ng-model="formData.type">
                            <option>test</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Description</label>
                        <div class="col-md-9 col-sm-9 col-xs-9">
                            <textarea class="form-control" ng-model="formData.description"></textarea>
                            <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                        </div>
                    </div>

                    <div class="ln_solid"></div>

                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-primary">Cancel</button>
                            <button class="btn btn-success" ng-click="create()" >Submit</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection