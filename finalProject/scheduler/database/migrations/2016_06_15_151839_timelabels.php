<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Timelabels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timelabels', function (Blueprint $table) {
            $table->increments('id');
            $table->time('timein');
            $table->integer('duration');
            $table->string('label');
            $table->integer('team_id')->unsigned();
            $table->softDeletes();
            $table->timestamps();

            $table->index(['timein', 'duration']);
            $table->index(['label', 'team_id']);
            $table->foreign('team_id')->references('id')->on('teams')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('timelabels');
    }
}
