var checkPass = function(password) {
    var total = 0;
    var charlist = "abcdefghijklmnopqrstuvwxyz";
    for (var i = 0; i < password.length; i++) {
        var countone = password.charAt(i);
        var counttwo = (charlist.indexOf(countone));
        counttwo++;
        total *= 17;
        total += counttwo;
    }
    console.log(total);

    if (total == 248410397744610) {
        alert('good job');
    } else {
        alert(
            "Sorry, but the password was incorrect.");
    }
};


function solve(total = 248410397744610) {
    var charlist = "abcdefghijklmnopqrstuvwxyz";

    var result = [];
    while (total > 0) {
        var x = 0;
        while ((total - x) % 17 != 0) {
            x += 1
        }
        total = ((total - x) / 17);
        result.push(charlist.charAt(x - 1));
        x = 0;
    }

    return result.reverse().join('');
}

console.log(solve(248410397744610));