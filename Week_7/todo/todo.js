$(document).ready(function() {

    // open the connection to the database.
    var dbConnection = new DB('db1');
    // current page number.
    var page = 1;
    // number of result in every page.
    var offset = 10;


    // select the first page data from database.
    var dataFromDb = dbConnection.select('todos', 0, offset) || 0;
    for (var i = 0; i < dataFromDb.length; i++) {
        // use Helper todoItem to create an item for todo.
        var item = todoItem(
            dataFromDb[i].title,
            dataFromDb[i].body,
            dataFromDb[i].id,
            dataFromDb[i].dateAdded
        );
        // append created item.
        $('#content').append(item);
    }




    // create on click event for add button.
    $('#add').onClick(function() {
        var data = {};
        data.title = $('#todoTitle').value();
        data.body = $('#todoBody').value();

        if (data.title != '' && data.body != '') {
            // insert data and get the inserted.
            var insertedData = dbConnection.insert('todos', data);

            // add the inserted data at the begining of the content.
            $('#content').insert(
                todoItem(
                    insertedData.title,
                    insertedData.body,
                    insertedData.id,
                    insertedData.dateAdded
                )
            );

            // remove error.
            $('#separator').text('.......');
            $('#separator').class('notError');
        } else {
            // add error
            $('#separator').text('Fields are required.');
            $('#separator').class('error');
        }


    });

    // create event on delete button.
    $('.delete').onClick(function(e) {
        // warn the user if he is sure to delete.
        var prompt = confirm("are you sure you want to delete");
        if (prompt == true) {
            var parent = $(e).parent().parent();
            var id = $(e).attr('value');

            // animate remove on x axis.
            parent.removeX(500, 1000, function() {
                dbConnection.delete('todos', [id]);
                console.log([id]);
            });
        }


        // parent.moveX(1000).slideUp(600, function() {
        //     parent.remove();
        //     dbConnection.delete('todos', [id]);
        // });
    });

    // add on scroll event on document.
    $(document).onScroll(function() {

        // get the document height.
        var pageHeight = $(document).height();
        // get the scroll height.
        var scrollHeight = $(this).scrollBottom();

        // if scroll reached the end of the page.
        if (scrollHeight >= pageHeight) {
            // increment page by one.
            page = parseInt(page) + 1;
            // calculate the page start.
            var pageFrom = (page - 1) * offset;
            // get the data from the database, providing start and offset.
            var dataFromDb = dbConnection.select('todos', pageFrom, offset) || 0;
            // show loading.
            $('#loadingImage').class('shown');

            for (var i = 0; i < dataFromDb.length; i++) {
                var item = todoItem(
                    dataFromDb[i].title,
                    dataFromDb[i].body,
                    dataFromDb[i].id,
                    dataFromDb[i].dateAdded
                );
                $('#content').append(item);
            }
            // hide loading.
            $('#loadingImage').class('hidden');
        }
    });


});