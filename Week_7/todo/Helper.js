/**
 * return item as dom.
 * @param  {string} title
 * @param  {string} body
 * @param  {string} id
 * @param  {string} cDate
 * @return {string}       [description]
 */
function todoItem(title, body, id, cDate) {
    var date = new Date();
    var options = {
        weekday: "long",
        year: "numeric",
        month: "short",
        day: "numeric",
        hour: "2-digit",
        minute: "2-digit"
    };
    var currentDate = cDate || date.toLocaleTimeString("en-us", options);

    var itemElement = '<div class="item"><div class="itemBody"><div class="title">';
    itemElement += title + '</div><div class="date">';
    itemElement += currentDate + '</div><div class="body">';
    itemElement += body + '</div></div><div class="itemControl"><button class="delete test" value="';
    itemElement += id + '">X</button></div></div>';
    return itemElement;
}