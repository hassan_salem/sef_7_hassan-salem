function inArray(value, array) {
    return array.indexOf(value) > -1;
}
/**
 * DB class constructor.
 * @param {string}
 */
var DB = function(dbName) {
    this.dbName = dbName;
}

/**
 * insert data in the database.
 * @param  {string}
 * @param  {string}
 * @return {inserted data.}
 */
DB.prototype.insert = function(table, data) {
    // create table name . as databaseName.tableName
    var tableName = this.dbName + '.' + table;

    // get/set the last id for the table ;
    var lastid = localStorage.getItem(tableName + '.count') || 0;

    // create formated date of today.
    var date = new Date();
    var options = {
        weekday: "long",
        year: "numeric",
        month: "short",
        day: "numeric",
        hour: "2-digit",
        minute: "2-digit"
    };
    var currentDate = date.toLocaleTimeString("en-us", options);

    // if title and body are givven.
    if (data.title != null && data.body != null) {
        // get the data from database.
        var tableData = localStorage.getItem(tableName);

        // increment last id.
        lastid = parseInt(lastid) + 1;

        // set the dateAdded.
        data.dateAdded = currentDate;

        // set id.
        data.id = parseInt(lastid);

        /**
         * if tableData is not empty
         * so parse the data and add the new record
         * at the top of the old one.
         * then update.
         * else create new array and put the data into it
         * then update.
         */
        if (tableData != null) {
            var tableDataAsArray = JSON.parse(tableData);
            tableDataAsArray.unshift(data);
            localStorage.setItem(tableName, JSON.stringify(tableDataAsArray));
        } else {
            var tableDataAsArray = [];
            tableDataAsArray.unshift(data);
            localStorage.setItem(tableName, JSON.stringify(tableDataAsArray));
        }
        localStorage.setItem(tableName + '.count', (lastid));
        return data;

    } else {
        return false;
    }
}

/**
 * select all items in the database.
 * @param  {string}
 * @return {array}
 */
DB.prototype.selectAll = function(table) {
    var tableName = this.dbName + '.' + table;
    var data = localStorage.getItem(tableName);
    if (data != null) {
        var dataAsArray = JSON.parse(data);
        return dataAsArray;
    }
    return null;
}

/**
 * select acting as sql query : limit 0, 10
 * used for pagination.
 * @param  {string}
 * @param  {int}
 * @param  {int}
 * @return {array}
 */
DB.prototype.select = function(table, start = 0, offset = 10) {
    // select all the data from the database.
    var data = this.selectAll(table) || [];
    // init the length to zero
    var datalength = 0;
    // if data exists update the length to the actual length.
    if (data != null) {
        datalength = data.length;
    }

    /**
     * if the count of records is more than the offset
     * so give me only the number of records I want.
     * else get all records.
     * @return {[type]}
     */

    var startIndex = start;
    var endIndex = start + offset;
    if (endIndex > data.length - 1) {
        endIndex = data.length - 1;
    }
    var returnedData = [];

    for (var i = start; i < start + offset; i++) {
        if (typeof(data[i]) != 'undefined')
            returnedData.push(data[i]);
    }
    console.log(returnedData);
    return returnedData;
}

/**
 * delete record/records 
 * by providing array of ids.
 * @param  {string}
 * @param  {array}
 * @return {boolean}
 */
DB.prototype.delete = function(table, ids) {
    var tableName = this.dbName + '.' + table;
    // get data or empty array if no data.
    var tableData = this.selectAll(table) || [];
    for (var i = 0; i < tableData.length; i++) {
        if (inArray(tableData[i].id, String(ids))) {
            // remove the id from ids 
            ids.splice(ids.indexOf(tableData[i].id), 1)
                // remove the element from the data array.
            tableData.splice(i, 1);
        }
        // if we removed all ids.
        if (ids.length == 0) {
            // update the table data.
            localStorage.setItem(tableName, JSON.stringify(tableData));
            return true;
        }
    }

    return false;
}
