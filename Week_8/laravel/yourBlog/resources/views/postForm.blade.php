@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="">
                <div class="panel panel-default">
                    <div class="panel-heading">Add Post</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="@if(isset($post)) {{action('PostsController@update', ['id' => $post->id]) }} @else  {{action('PostsController@store')}} @endif">
                            {!! csrf_field() !!}
                            <input type="hidden" name="postIdEdit" value="{{$post->id}}">
                            @if(isset($post->id))
                                {!! method_field('patch') !!}
                            @endif

                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label class="col-md-2 control-label">Title</label>

                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="title" value="@if(old('title')) {{old('title')}} @elseif(isset($post->title)) {{$post->title}} @endif">

                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('thumbnail') ? ' has-error' : '' }}">
                                <label class="col-md-2 control-label">Image</label>

                                <div class="col-md-8">
                                    <input type="link" class="form-control" name="thumbnail" value="@if(old('thumbnail')) {{old('thumbnail')}} @elseif(isset($post->thumbnail)) {{$post->thumbnail}} @endif">

                                    @if ($errors->has('thumbnail'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('thumbnail') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                                <label class="col-md-2 control-label">Category</label>

                                <div class="col-md-8">

                                    <select class="form-control" id="sel2" name="categoryId">
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}" @if(old('category')) @if($category->id == old('category')) selected @endif  @elseif(isset($post->title)) @if($category->id == $post->categoryId) selected @endif  @endif >{{$category->title}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('category'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('category') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('post') ? ' has-error' : '' }}">
                                <label class="col-md-2 control-label">post</label>

                                <div class="col-md-8">
                                    <textarea class="form-control" name="post" id="postEditor">@if(old('post')) {{old('post')}} @elseif(isset($post->post)) {{$post->post}} @endif</textarea>
                                    @if ($errors->has('post'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('post') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-plus-circle"></i>Add
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'postEditor' );

    </script>
@endsection
