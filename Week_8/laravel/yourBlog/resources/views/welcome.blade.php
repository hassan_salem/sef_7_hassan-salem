@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @if(isset($category->title))
            <h3>{{$category->title}}</h3>
        @endif

        <hr>
        @foreach($posts as $post)

        <div class="row">
            <div class="col-sm-4"><a href="/posts/{{$post->id}}" class=""><img src="{{$post->thumbnail}}" class="img-responsive img-responsive" style="height:210px; width: 370px;"></a>
            </div>
            <div class="col-sm-8">
                <h3 class="title"><a href="/posts/{{$post->id}}">{{$post->title}}</h3></a>
                <p class="text-muted">
                    <span class="glyphicon glyphicon-calendar"></span> {{ date('F d, Y @ m:s', strtotime($post->created_at)) }}
                    <a @if(Auth::User()) class="like" postId ="{{$post->id}}" @endif href="javascript:void(0)"><span class="glyphicon glyphicon-heart"></span><span id="likeCount{{$post->id}}">{{$post->likes->count()}}</span> </a>
                    <a href="javascript:void(0)"><span class="glyphicon glyphicon-comment"></span> {{$post->comments->count()}} </a>
                    @if(Auth::user())
                        @if(Auth::user()->id == $post->userId)
                            <a href="/posts/{{$post->id}}/edit">Edit</a>
                        @endif
                    @endif

                </p>
                <hr>
                <p>
                    {!! str_limit($post->post, 500) !!}
                </p>
                <hr>
                <p class="text-muted">By <a href="#">{{$post->user->name}}</a>@if(isset(Auth::getUser()->id)) @if(Auth::getUser()->id == $post->userId), <a href="/posts/{{$post->id}}/edit">Edit</a> @endif @endif</p>

            </div>
        </div>
        <hr>
        @endforeach


            <ul class="pagination" >
                @for($i = 1 ; $i <= $postsCount ; $i++)
                <li><a href="/category/{{$category->id}}/{{$i}}">{{$i}}</a></li>
                @endfor
            </ul>

        <script>
            $(document).ready(function(){
                $('.like').on('click', function(){
                    var id = $(this).attr('postId');
                    $.get('/like/' + id ,function(response){
                        $('#likeCount' + id).text(response);
                    })
                });


            });
        </script>
</div>
@endsection
