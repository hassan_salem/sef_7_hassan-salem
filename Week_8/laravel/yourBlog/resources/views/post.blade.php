@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <!-- Blog Post Content Column -->
            <div class="center-block" style="width: 80%;">

                <!-- Blog Post -->

                <!-- Title -->
                <h1>{{$post->title}}</h1>
                <a @if(Auth::User()) class="like" postId ="{{$post->id}}" @endif  href="javascript:void(0)"><span class="glyphicon glyphicon-heart"></span><span id="likeCount{{$post->id}}">{{$post->likes->count()}}</span> </a>
                <a href="#"><span class="glyphicon glyphicon-comment"></span> {{$post->comments->count()}} </a>

                <!-- Author -->
                <p class="lead">
                    by <a href="#">{{$post->user->name}}</a>
                    @if(Auth::user())
                        @if(Auth::user()->id == $post->userId)
                            <a href="/posts/{{$post->id}}/edit">Edit</a>
                        @endif
                    @endif
                </p>

                <hr>

                <!-- Date/Time -->
                <p><span class="glyphicon glyphicon-time"></span> Created at:  {{ date('F d, Y @ m:s', strtotime($post->created_at)) }}</p>

                <hr>

                <!-- Preview Image -->
                <img class="img-responsive" src="{{$post->thumbnail}}" alt="">

                <hr>

                {!! $post->post !!}

                <hr>

                <!-- Blog Comments -->

                @if(Auth::user())
                <!-- Comments Form -->
                <div class="well form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
                    <h4>Leave a Comment:</h4>
                    <form  method="POST" action="{{action('CommentsController@store')}}">
                        {!! csrf_field() !!}
                        <input type="hidden" name="postId" value="{{$post->id}}" />
                        <div class="form-group">
                            <textarea class="form-control" rows="3" name="comment">@if(old('comment')) {{old('comment')}} @endif</textarea>
                        </div>
                        @if ($errors->has('comment'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('comment') }}</strong>
                                    </span>
                        @endif
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-btn fa-plus-circle"></i>Add
                        </button>
                    </form>
                </div>

                <hr>
                @endif

                <!-- Posted Comments -->
                @foreach($comments as $comment)
                    <!-- Comment -->
                    <div class="media">
                        <a class="pull-left" href="#">
                            <img style="width: 50px; height: 50px;" class="media-object img-circle" src="{{$comment->user->profileImage}}" alt="">
                        </a>
                        <div class="media-body">
                            <h4 class="media-heading">{{$comment->user->name}}
                                <small> {{ date('F d, Y @ m:s', strtotime($comment->created_at)) }}</small>
                            </h4>
                            {{$comment->comment}}
                        </div>
                    </div>
                @endforeach





            </div>


        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('.like').on('click', function(){
                var id = $(this).attr('postId');
                $.get('/like/' + id ,function(response){
                    $('#likeCount' + id).text(response);
                })
            });


        });
    </script>
@endsection
