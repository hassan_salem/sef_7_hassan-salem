<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('title');
            $table->text('post');
            $table->string('thumbnail')->nullable();
            $table->integer('userId')->unsigned()->nullable();
            $table->integer('categoryId')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('userId')->references('id')->on('users')->onDelete('set null');
            $table->foreign('categoryId')->references('id')->on('categories')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('posts');
    }
}
