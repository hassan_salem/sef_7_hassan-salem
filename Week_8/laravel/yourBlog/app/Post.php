<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function user(){
        return $this->belongsTo('App\User', 'userId');
    }

    public function likes(){
        return $this->hasMany('App\Like', 'postId');
    }

    public function comments(){
        return $this->hasMany('App\Comment', 'postId');
    }
}
