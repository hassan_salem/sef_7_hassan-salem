<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Comment;

use App\Http\Requests;

use Illuminate\Support\Facades\Auth;

class CommentsController extends Controller
{
    public function store(Request $request){

        if(Auth::user()){
            $this->validate($request, ['comment' => 'required|min:4|max:1000']);

            $comment = new Comment();
            $comment->comment = $request->comment;
            $comment->postId = $request->postId;
            $comment->userId = Auth::getUser()->id;
            $comment->save();
            return redirect()->route('posts.show', ['id'=>$request->postId]);

        }else{
            return redirect()->route('posts.show', ['id'=>$request->postId]);
        }



    }
}
