<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;

use App\Category;
use App\Post;

class CategoriesController extends Controller
{
    public function create(){
        if(Auth::user()){
            return  view('categoryForm');
        }
    }



    public function store(Request $request){
        $this->validate($request, array('title' => 'required|min:3|max:255'));

        $category = new Category();
        $category->title = $request->title;

        $category->save();

        return redirect()->route('categories.show', $category->id);
    }

    public function show($id, $page=1, $offset=10){

    }

    public function showPage($id, $page){
        $category = Category::find($id);
        $skip = ($page - 1) * 3;
        $posts = $category->posts()->skip($skip)->take(3)->get();
        if(isset($posts)){
            return view('welcome', ['posts' => $posts, 'postsCount' => ceil($category->posts()->count()/3), 'category' => $category]);
        }
    }
}
