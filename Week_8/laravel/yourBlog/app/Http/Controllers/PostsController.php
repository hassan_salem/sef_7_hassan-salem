<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Post;
use App\Category;
use App\User;

class PostsController extends Controller
{
    public function like($id){
        if(Auth::user()){

        }
    }

    public function comment(Request $request, $id){

    }

    public function create(){
        if(Auth::user()){
            $categories = Category::all();

            return view('postForm', ['categories'=> $categories]);
        }else{
            return view('errors.noaccess');
        }
    }

    public function store(Request $request){
        $this->validate($request, array(
            'title' => 'required|max:255',
            'post' => 'required|min:3'
        ));

        $post = new Post();
        $post->title = $request->title;
        $post->post = $request->post;
        $post->thumbnail = $request->thumbnail;
        $post->userId = Auth::user()->id;
        $post->categoryId = $request->categoryId;

        $post->save();

        return redirect()->route('posts.show', $post->id);
    }

    public function edit($id){
        if(Auth::user()){
            $post = Post::find($id);
            if(isset($post)){
                if($post->userId == Auth::getUser()->id){
                    return view('postForm', ['post' => $post, 'categories' => Category::all()]);
                }else{
                    // no access to edit this post
                    return view('errors.403', ['error' => 'You do not have access to edit this post']);
                }
            }else{
                // no post with this id
                return view('errors.403', ['error' => 'There is no post with this id']);
            }
        }else{
            // you are not loggedin
            return view('errors.403', ['error' => 'You are not logedin']);

        }

    }


    public function update(Request $request, $id){
        $this->validate($request, array(
            'title' => 'required|max:255',
            'post' => 'required|min:3'
        ));
        if((Auth::User())){
                $post = Post::find($id);
            if(Auth::User()->id == $post->userId && isset($post)){

                $post->title = $request->title;
                $post->post = $request->post;
                $post->thumbnail = $request->thumbnail;
                $post->categoryId = $request->categoryId;
                $post->save();
                return redirect()->route('posts.edit', $post->id);
            }else{
                // no access
                echo ' no access ';
            }
        }else{
            // not logedin
            echo ' not loged in';
        }


    }

    public function destroy($id){

    }

    public function show($id){
        $post = Post::find($id);
        if(isset($post)){
            $cc = $post->comments;
            return view('post', ['post' => $post, 'comments' => $cc]);
        }else{
            return view('errors.503');
        }

    }

    public function index($page = 1){
        $category = Category::first();
        $skip = ($page - 1) * 3;

        $posts = $category->posts()->orderBy('id', 'DESC')->skip($skip)->take(3)->get();
        if(isset($posts)){
            return view('welcome', ['posts' => $posts, 'postsCount' => ceil($category->posts()->count()/3), 'category' => $category]);
        }
    }

}
