<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Like;
use App\Post;
use App\Http\Requests;

class LikesController extends Controller
{
    public function like($id){

        $post = Post::find($id);
        if($post && Auth::User()){
            $like = new Like();
            $like->userId = Auth::User()->id;
            $like->postId = $post->id;
            if($like->save()){
                return $post->likes->count();
            }else{
                //$like == Like::where(['userId' => $Auth::User()->id, 'postId' => $id]);
                echo "hi";
            }
        }

    }
}
