<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'PostsController@index');

/**
 * post functions
 */




/**
 * user functions
 */

Route::get('/user',function(){

});



Route::auth();

Route::get('/home', 'HomeController@index');

Route::get('/category/{id}/{page}', 'CategoriesController@showPage');

Route::resource('posts', 'PostsController');

//Route::post('/posts', 'PostsController@store');

Route::resource('categories', 'CategoriesController');

Route::resource('comments', 'CommentsController');

Route::get('/like/{id}', 'LikesController@like');

