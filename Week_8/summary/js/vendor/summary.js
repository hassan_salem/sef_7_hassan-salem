$(document).ready(function() {

    // open database connection.
    var db = new DB('summarize');


    $('#submitSearch').onClick(function() {
        // change the layout.
        $('#searchForm').removeClass('beforeSearch');
        $('#searchForm').class('afterSearch');

        // get and summarise the data
        $.ajax('getContent.php?url=' + $('#url').value(), {
            afterCallback: function(data) {

                // parse the article body.
                var text = "";
                $('div p', data).each(function(index, el) {
                    text += el.innerText;
                });

                // parse the title.
                var title = $('div h3', data).text();

                // make an ajax request to the apiCall.php.
                $.ajax('apiCall.php', {

                    afterCallback: function(data) {
                        // remove loading
                        $('#loading').removeClass('displayBlock');
                        $('#loading').class('displayNone');

                        var jsonObject = JSON.parse(data);
                        console.log(jsonObject);

                        // get the result from the api.
                        var result = '';
                        var sentences = jsonObject['sentences'];
                        for (var i = 0; i < sentences.length; i++) {
                            result += '<p>' + sentences[i] + '</p>';
                        }
                        $('#result').text('<h3>' + title + '</h3>' + result);


                        db.insert('articles', {
                            title: title,
                            body: result
                        }, true);

                    },
                    // send data to the url.
                    data: {
                        title: title,
                        text: text
                    },
                    type: 'POST',
                });




            },
            beforeCallback: function() {
                // view the loading.
                $('#loading').removeClass('displayNone');
                $('#loading').class('displayBlock');
            },
        });
        return false;
    });

    $('#history').onClick(function() {
        // select all history from the database.
        var historyData = db.selectAll('articles') || [];
        var htmlResult = '';
        for (var i = 0; i < historyData.length; i++) {
            htmlResult += '<div><button class="item" data="' + historyData[i].body + '">' +
                '<h3>' + historyData[i].title + '</h3></button>' +
                '</div>';
        }
        // change the layout.
        $('#searchForm').removeClass('beforeSearch');
        $('#searchForm').class('afterSearch');
        // add result to page
        $('#result').text(htmlResult);

    });

    $('.item').onClick(function(e) {
        // get the data from the item.
        var data = $(e).attr('data');
        // get title from the item.
        var title = $(e).text();
        // add data to the result popup window.
        $('#popResultData').text('<h3>' + title + '</h3>' + data);
        // show the popup window.
        $('#popWrapper').removeClass('displayNone');
        $('#popWrapper').class('displayBlock');
    });

    $('#popClose').onClick(function() {
        // hide the popup window.
        $('#popWrapper').removeClass('displayBlock');
        $('#popWrapper').class('displayNone');
    });

});