<?php
$apiUrl = 'https://api.aylien.com/api/v1/summarize';

// data need to send to the api.
$postData = array(
	'text' => $_POST['text'],
	'title' => $_POST['title']
	);

// curl handle.
$curl = curl_init();

// set curl header options.
// send app id and app key.
curl_setopt($curl, CURLOPT_HTTPHEADER, 
	array(
		"X-AYLIEN-TextAPI-Application-Key:78701b0a0574ec6a273273dbd5e4bd39",
		"X-AYLIEN-TextAPI-Application-ID:679b3456",
		"Accept: */*",
		"accept-encoding: gzip, deflate"
	)
);

// set curl url option.
curl_setopt($curl, CURLOPT_URL, $apiUrl);
// enable post for curl.
curl_setopt($curl, CURLOPT_POST, true);
// set curl post fields. by build a url encoded query from the postData array.
curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($postData)); 
// tell curl to return the requested page.
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
// execute curl, and put the response in $result.
$result = curl_exec($curl);
// close handle.
curl_close($curl);
// print results.
echo $result;