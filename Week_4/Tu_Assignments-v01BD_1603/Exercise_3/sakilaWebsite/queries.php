<?php
/**
 * queries array holds all the queries as string.
 * every index is a query from the assignment.
 * @var array
 */
$queries = array(
    'select
	    actor.first_name, count(actor.first_name)
	from
	    actor,
	    film,
	    film_actor
	where
	    actor.actor_id = film_actor.actor_id
	        and film.film_id = film_actor.film_id
	group by actor.first_name',

    'select
	    language.name, film.title, count(language.name) as count
		from
		    film,
		    language
		where
		    film.language_id = language.language_id
		        and film.release_year = "2006"
		group by language.name
		order by count DESC
		limit 3',

    'select
    country.country, count(*) as count
	from
	    customer,
	    address,
	    country,
	    city
	where
	    city.country_id = country.country_id
	        and customer.address_id = address.address_id
	        and address.city_id = city.city_id
	group by country.country
	order by count DESC
	limit 3',

    'select
	    address2
	from
	    address
	where
	    char_length(address2) > 0
	order by address2 ASC',

    'select
    concat_ws(" ", actor.first_name, actor.last_name) as "actor name",
    film.title,
    film.release_year
from
    actor,
    film,
    film_actor
where
    film_actor.actor_id = actor.actor_id
        and film_actor.film_id = film.film_id
        and film.description like " % crocodile % "
        and film.description like " % shark % "
order by actor.last_name',

    'select SQL_CALC_FOUND_ROWS
    category.name, count(category.name) as count
from
    film_category,
    film,
    category
where
    film_category.category_id = category.category_id
        and film_category.film_id = film.film_id
group by category.name
having count between 55 and 65
UNION ALL select
    category.name, count(category.name) as count
from
    film_category,
    film,
    category
where
    film_category.category_id = category.category_id
        and film_category.film_id = film.film_id
        and FOUND_ROWS() = 0
group by category.name
having (count < 55 or count > 65)
order by count ASC',

    'select
    concat_ws(" ", first_name, last_name)
from
    actor
where
    actor.first_name = (select
            actor.first_name
        from
            actor
        where
            actor.actor_id = "8")
union all select
    concat_ws(" ", first_name, last_name)
from
    customer
where
    customer.first_name = (select
            actor.first_name
        from
            actor
        where
            actor.actor_id = "8")',
    'select
    table1.store_id,
    table1.payment_date,
    table1.average as "month average",
    table1.subtotal as "month total",
    year(table2.payment_date) as "year",
    table2.average as "year average",
    table2.subtotal as "year total"
from
    (select
        store.store_id,
            payment.payment_date,
            avg(payment.amount) as average,
            sum(payment.amount) as subtotal
    from
        payment, store, staff, rental
    where
        payment.rental_id = rental.rental_id
            and payment.staff_id = staff.staff_id
            and staff.store_id = store.store_id
    group by store.store_id , month(payment.payment_date) , year(payment.payment_date)) as table1
        right join
    (select
        store.store_id,
            payment.payment_date,
            avg(payment.amount) as average,
            sum(payment.amount) as subtotal
    from
        payment, store, staff, rental
    where
        payment.rental_id = rental.rental_id
            and payment.staff_id = staff.staff_id
            and staff.store_id = store.store_id
    group by store.store_id , year(payment.payment_date)) as table2 ON table1.store_id = table2.store_id
        and year(table1.payment_date) = year(table2.payment_date)',
);
