<?php
// configuration file to handle the database connection.
require_once "config.php";

// file contain all function that we will use.
require_once "functions.php";

// file contain all queries that we will use.
require_once "queries.php";

// horizontal rule.
echo "<hr>";

// initialize the query var with the first query in the query array.
$query = $queries[0];

// check if my query index is in the array of queries.
if ($_GET['query'] > count($queries) || $_GET['query'] < 0) {
    // if query is not in range of the array halt the execution of the page.
    die("not in range ");
} else {
    // if its in the range. so use it from the queries array.
    $query = $queries[$_GET['query']];
}
// print the result.
echo queryToTable($connection, $query);

$connection->close();