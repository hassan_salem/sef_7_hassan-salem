<?php

function queryToTable(&$connectionHandler, $query) {
    // query the database.
    $result = $connectionHandler->query($query);

    $table = "";
    // check if there are any rows.
    if ($result->num_rows > 0) {

        $table = "<table border='1'><tr>";

        // get the header of a table.
        while ($tableInfo = mysqli_fetch_field($result)) {
            $table .= "<th>{$tableInfo->name}</th>";
        }
        // close table row tag.
        $table .= "</tr>";

        // read all rows from the result.
        while ($row = $result->fetch_assoc()) {
            // open new table row.
            $table .= "<tr>";
            foreach ($row as $cell) {
                // add table data to row.
                $table .= "<td>{$cell}</td>";
            }
            // close table row.
            $table .= "</tr>";
        }

        // close table.
        $table .= "</table>";

        return $table;
    } else {
        return "No data";
    }
}

?>