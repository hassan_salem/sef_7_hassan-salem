<?php

$serverName = "localhost";
$userName = "phpuser";
$password = "compl3xPassw0rd";
$dataBaseName = "sakila";

/**
 * Open a new connection to the database server.
 * @var mysqli
 */
$connection = new mysqli(
    $serverName,
    $userName,
    $password,
    $dataBaseName
);

// halt execution and return error message if connection failed.
if (!$connection) {
    die("Connection failed with error: " . mysqli_connect_error());
} else {
    echo "Welcom to Sakila";
}

?>