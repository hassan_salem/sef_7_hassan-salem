use proceduresDB;



select t2.id1, max(count),t2.an

from 
	(SELECT t1.p1id as id1, 
	t1.p2id as id2, 
	t1.an ,
	COUNT(*) as count

	FROM 

		(select 
		proc1.start_time as p1st,
		proc2.start_time as p2st,
		proc1.end_time as p1et,
		proc2.end_time as p2et,
		proc1.anest_name as an,
		proc1.proc_id as p1id,
		proc2.proc_id as p2id
		from procedures as proc1,
		procedures as proc2
		where proc2.anest_name = proc1.anest_name
		AND proc2.start_time < proc1.end_time
		AND proc1.start_time <= proc2.start_time) as 
	t1, 
	procedures AS proc3

	where proc3.anest_name = t1.an AND proc3.start_time <= t1.p2st
	AND t1.p2st < proc3.end_time AND t1.p2st < t1.p1et AND t1.p1st <= t1.p2st



	GROUP BY t1.p1id, t1.p2id) as t2

group by id1;



