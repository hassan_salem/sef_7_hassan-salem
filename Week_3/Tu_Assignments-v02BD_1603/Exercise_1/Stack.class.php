<?php

/**
* 
*/
class Stack
{
	// $size ;	
	private $index  = -1 ;
	private $items = array();

	function __construct()
	{
		
	}

	function push($item) 
	{
		$this->index ++ ;
		$this->items[$this->index] = $item;
	}

	function pop() 
	{	
		return ($this->items[$this->index--]);
	}

	function isEmpty() 
	{
		return $this->index == -1 ;
	}

	function peek()
	{
		$item = ($this->items[$this->index]);
		return $item ;
	}

}