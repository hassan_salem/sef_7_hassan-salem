<?php
/**
 * database class is to handle database operations
 * CRUD and else
 */
class Database {

    /**
     * database name holds the name of
     * created database
     * @var [type]
     */
    private $databaseName;

    /**
     * database physical location on the drive.
     * and its chosen to be = data
     * @var [string]
     */
    private $dataLocation = 'data';

    /**
     * handle the location of the database we are working on.
     * @var [type]
     */
    private $databaseLocation;

    /**
     * user may change the location of the database created or retrieved.
     * @param string location for the physical databases
     *
     */
    function __construct($databaseName, $dataLocation = null) {
        $databaseName = str_replace('"', '', $databaseName);
        if ($dataLocation) {
            $this->dataLocation = $dataLocation;
        }

        $this->databaseLocation = $this->dataLocation . '/' . $databaseName;
        $this->databaseName = $databaseName;
    }

    /**
     * if the user ask for the list of the databases he own
     * listDbs will use getDirContant from helper class.
     * for more info about getDirContent please go to helper class
     * @return string list of databases and corrosponding tables.
     */
    public function listDbs() {
        return Helper::getDirContant($this->dataLocation);
    }

    /**
     * init is used to initialize the database.
     * and create a virtual link with it.
     * even if it is not exists.
     * @return string expected location
     */
    function init() {
        return $this->databaseLocation;
    }

    /**
     * if database is exists it will return the location of it
     * else it will create the database then return the location.
     * @return physical location to connect to the database.
     */
    public function open() {
        if (file_exists($this->databaseLocation)) {
            return $this->databaseLocation;
        } else {
            $this->createDb();
            return $this->databaseLocation;
        }

    }

    /**
     * delete the database and all its content if
     * the database is exists.
     * @return void
     */
    public function delete() {
        if (file_exists($this->databaseLocation)) {
            shell_exec('rm ' . $this->databaseLocation . ' -r');
            return "deleted";
        } else {
            return "database not exists";
        }
    }

    /**
     * this function is to create a database
     * if the database folder does not exists
     * so it will create a folder with read and write access
     * just for the owner of the directory 0666.
     * @param string
     * @return
     */
    public function createDb() {
        $this->databaseLocation = $this->dataLocation . '/' . $this->databaseName;
        if (!file_exists($this->databaseLocation)) {
            mkdir($this->databaseLocation, 0705, true);
            return "database created";
        } else {
            return "database exist";
        }

    }

    /**
     * create table object
     * @param  string $tableName table name
     * @param  array $columns   columns
     * @return Table object
     *            object of a table dddd
     */
    public function createTable($tableName, $columns) {
        $tableName = str_replace('"', '', $tableName);
        $tables = $this->getTables();
        //echo $tableName;
        if (in_array($tableName, $tables)) {
            return "table exists";
        } else {
            $table = new Table($this);
            $table->init($tableName);
            return $table->createTable($columns);

        }
    }

    /**
     * check if file exists
     * if yes delete else return not exists.
     * @param  [string] $tableName
     * @return [type]            [description]
     */
    public function deleteTable($tableName) {
        if (!file_exists($this->databaseLocation)) {
            return "table not exists";
        }
        $table = new Table($this);
        $table->init($tableName);
        echo $table->deleteTable();
    }

    /**
     * [addRow description]
     * @param [type] $tableName [description]
     * @param [type] $columns   [description]
     */
    public function addRow($tableName, $columns) {
        $table = new Table($this);
        $table->init($tableName);
        return ($table->insert($columns));
    }

    /**
     * retrieve rows with query,
     *   d
     * @param  string $tableName [description]
     * @param  string $id        query
     * @return string            list
     */
    public function getRow($tableName, $id) {
        $table = new Table($this);
        $table->init($tableName);
        return $table->selectFromAll($id);
    }

    /**
     * delete row from a table object
     * @param  string $tableName [description]
     * @param  int $id        [description]
     * @return void            [description]
     */
    public function deleteRow($tableName, $id) {
        $table = new Table($this);
        $table->init($tableName);
        $table->delete($id);
    }

    /**
     * this function is to retrieve all tables
     * from the opened database.
     * @return [type] [description]
     */
    public function getTables() {
        $dbLocation = $this->databaseLocation;
        if (file_exists($dbLocation)) {
            $tables = scandir($dbLocation);
            $result = array();
            foreach ($tables as $table) {
                if ($table != "." && $table != "..") {
                    array_push($result, $table);
                }
            }
            return $result;
        }
        return array();
    }

}