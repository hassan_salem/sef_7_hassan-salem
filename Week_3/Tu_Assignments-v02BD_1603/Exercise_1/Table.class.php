<?php

/**
 * Table object will handle all operations about table
 * as delete update and else
 */
class Table {

    private $database = 'data/hassanTest';
    private $tableLocation;
    private $tableName;
    private $tableSize;
    private $maxRows = 50;
    private $columns = null;

    private $directory = "";

    function __construct($database) {
        $this->database = $database->init();
    }

    /**
     * initialize the object to be ready
     * for handling all table instruction
     * by making a virtual link to the data file.
     * @param  [type] $name [description]
     * @return [type]       [description]
     */
    public function init($name) {
        $this->directory = $this->database . '/' . $name;
        $this->tableName = $name;
    }

    /**
     * here where engine create tables
     * first it will remove all qouts from names
     * of the columns then check if
     * the physical data is exists to return that its exists.
     * or to create it if not.
     * creation proccess of the table :
     * 1- create folder with the table name.
     * 2- create file to handle the structure of the table
     *
     * and fill it with 0 and line of the columns.
     *
     * @param  [type] $columns [description]
     * @return [type]          [description]
     */
    public function createTable($columns) {
        $columns = Helper::removeQoutFromArray($columns);
        if (!Helper::isNotNull($columns)) {
            return "column cannot be null";
        }
        if (!file_exists($this->directory)) {
            mkdir($this->database . '/' . $this->tableName);
            $this->directory = $this->database . '/' . $this->tableName;
        }

        if (!file_exists($this->directory . '/' . $this->tableName)) {

            file_put_contents(
                $this->directory . '/' . $this->tableName,
                "0\n" . implode(',', $columns),
                FILE_APPEND
            );

            $this->tableSize = 0;
            $this->columns = $columns;
        } else {
            $data = file(
                $this->directory . '/' . $this->tableName,
                FILE_SKIP_EMPTY_LINES
            );
            $this->tableSize = $data[0];
            $this->columns = $data[1];

        }
        return "table created";
    }

    /**
     * delete table is to physically delete
     * data files and folders from the drive.
     * @return [type] [description]
     */
    public function deleteTable() {
        $tableName = $this->directory;
        if (!file_exists($tableName)) {
            return "Table $tableName not exists";
        }
        if (file_exists($tableName)) {
            shell_exec('rm ' . $tableName . ' -r');
            return "Table $this->tableName deleted ";
        } else {
            return "Table $this->tableName not exists";
        }
    }

    /**
     * instert used to insert data into an opened table.
     * insert is depends on the id of the given data
     * to descide where to put the data, or to create a
     * data file for it or not.
     *
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function insert($data) {
        $id = trim($data[0]);
        if ($id > 0) {

        } else {
            return "ID should be integer";
        }

        if (!Helper::isNotNull($data)) {
            return "no column can accept null";
        }
        $dataFileName = $this->tableName . $this->getTablePostStr($id);
        $dataFileName = $this->directory . '/' . $dataFileName;
        $written = false;
        $content = file($this->directory . '/' . $this->tableName, FILE_SKIP_EMPTY_LINES);
        //return 'AAAAA' . implode(',', $content) . ' ' . implode(',', $data);
        if (count(explode(',', $content[1])) != count($data)) {
            return "arguments not fit into the table structure";
        }
        //echo $dataFileName;
        if (!file_exists($dataFileName)) {
            if (!file_exists($this->directory)) {
                return "table not exists";
            }

            file_put_contents($dataFileName, implode(',', $data) . "\n");
            return "";
        } else {
            $fileContent = file($dataFileName, FILE_SKIP_EMPTY_LINES);
            //$fileContent = explode('\n', $fileContent);
            $newFile = "";
            $inserted = false;
            for ($i = 0; $i < count($fileContent); $i++) {

                $idFromTable = trim(
                    substr(
                        $fileContent[$i],
                        0,
                        strpos($fileContent[$i], ',')
                    )
                );

                if ($id < $idFromTable && $written == false) {
                    $newFile .= implode(',', $data) . "\n";
                    $newFile .= $fileContent[$i];
                    $written = true;
                    //echo "middle";
                } elseif ($id == $idFromTable) {
                    return "doplicate key";
                } elseif (($i == count($fileContent) - 1) && $written == false) {
                    $newFile .= $fileContent[$i] . "";
                    $newFile .= implode(',', $data) . "\n";
                    //echo "last";
                } else {
                    $newFile .= $fileContent[$i] . "";
                }

                //var_dump($newFile);
            }

            file_put_contents($dataFileName, $newFile);
            return 'Data written to the table';

        }
    }

    public function select($id) {
        $dataFileName = $this->directory . '/' . $this->tableName . $this->getTablePostStr($id);
        if (!file_exists($dataFileName)) {
            return "not found";
        } else {
            $fileContent = file($dataFileName, FILE_SKIP_EMPTY_LINES);
            foreach ($fileContent as $line) {
                $idFromTable = substr($line, 0, strpos($line, ','));
                if ($id == $idFromTable) {
                    return explode(',', $line);
                }
            }
            return "not found";
        }

    }

    public function selectFromAll($query) {
        $database = scandir($this->directory);
        $result = array();
        foreach ($database as $table) {

            $tableContent = file($this->directory . '/' . $table, FILE_SKIP_EMPTY_LINES);
            //var_dump($tableContent);
            foreach ($tableContent as $content) {
                if (strpos($content, "$query") !== false || trim($query) == "*") {
                    array_push($result, $content . "\n");
                }
            }
        }
        return $result;

    }

    public function delete($id) {
        $dataFileName = $this->directory . '/' . $this->tableName . $this->getTablePostStr($id);
        $newFile = "";
        $edited = false;
        if (file_exists($dataFileName)) {
            $fileContent = file($dataFileName);
            for ($i = 0; $i < count($fileContent); $i++) {
                $idFromTable = substr(
                    $fileContent[$i],
                    0,
                    strpos($fileContent[$i], ',')
                );

                if (trim($idFromTable) != $id) {
                    $newFile .= $fileContent[$i];
                } else {
                    $edited = true;
                }
            }
            if ($edited) {
                file_put_contents($dataFileName, $newFile);
            }
        }
    }

    private function getTablePostStr($id) {
        $poststr = (int) ($id / $this->maxRows) + 1;
        return $poststr;
    }
}