<?php
require_once 'Command.class.php';
require_once 'Database.class.php';
require_once 'Cmd.class.php';
require_once 'Helper.class.php';
require_once 'Table.class.php';
require_once 'Stack.class.php';

// $command = new Command("CREATE, TA   BL E, 'test' ");
// $command->getOperationName();

//$x = new Table('test');
//$x->init('test', array('id', 'name', 'password'));
//$x->deleteTable();

//var_dump($x->select(9999));

//$command = new Command('CREATE,TABLE,"abcDBB",COLUMNS,"id","name","dddd"');
//$command = new Command('CREATE,DATABASE,"abcDBB1"');
// $command = new Command('DELETE,ROW,"49"');
// $command->execute('hassanTest', 'test');
//$c = new Command('DELETE,TABLE,"TBLNAME",FROM,"BBC"');
// $db = new Database('BBC');
// $db->open();
// $db->deleteTable('HI');
//$c = new Cmd('ADD,"1","hassan","salem",INTO,"table",FROM,"db"');
//$c = new Cmd('CREATE,TABLE,"TABLE2",COLUMNS,"ID","NAME","GRADE",INTO,"FIRSTTEST"');
//$c = new Cmd('CREATE,TABLE,"TABLENAME",COLUMNS,"ID","NAME",INTO,"DBNAME"');
//$c = new Cmd('DELETE,ROW,"2",FROM,"TBLNAME",INTO,"DB"');
//$c = new Cmd('GET,"ID",FROM,"DATABASE",INTO,"TABLE"');
//$c = new Cmd('GET,"TBL",FROM,"DB",INTO,"table"');
//$c = new Cmd('ADD,"9","HASSAN",INTO,"TBLNAME",FROM,"DB"');
//var_dump($c->execute());

//exit();
//

$welcom = '
	Welcome to YourSQL Engine.

	type HELP for for getting help,
	also you can type clear in order to
	clean the screen input.
++++++++++++++++++++++++++++++++++++++++++
';
echo $welcom;
while (true) {

    $handle = fopen("php://stdin", "r");
    $command = fgets($handle);
    $command = trim(preg_replace('/\s\s+/', ' ', $command));
    $cmd = new Cmd(str_replace("\n", '', $command));

    echo $cmd->execute() . "\n";

    if ($command == 'clear') {
        echo $welcom;
    }
}
