<?php

/**
 * This class is recieving the command from the user
 * then parse it to know what to do.
 * it should run on a handle of opened database
 * then return the result.
 */
class Command {

    /**
     * array storing all accepted operations
     * @var array
     */
    private $acceptedOperations = array(
        'CREATE',
        'DELETE',
        'ADD',
        'GET',
    );

    /**
     * $expected operation is what the command expect
     * CREATE >> expect TABLE or DATABASE; TABLE expect tableName then COLOMNS names
     * DELETE >> expect RECORD, DATABASE, or TABLE
     * ADD       >> expect array of records and if there exist a table name
     *              else it will add to the last created table.
     *
     *
     * @var [Array of Strings]
     */
    private $expectedOperation = array(
        'CREATE' => array('TABLE', 'DATABASE'),
        'DELETE' => array('ROW', 'DATABASE', 'TABLE'),
        'ADD' => array('string'),
        'TABLE' => array('string'),
        'COLUMNS' => array('string'),
        'GET' => array('string'),
    );

    /**
     * $operation is string holds the first word in every command.
     * @var String
     */
    private $operation;

    private $commandParts;
    /**
     * The command sent by whatever.
     * @var [type]
     */
    private $command;

    private $commandLine;
    private $commandError = null;

    /**
     * The object have to get arguments on creation.
     * array_map used to make a callback function to all
     * elements of the given array.
     * @param String
     * @param Database
     */
    function __construct($command, $database = null) {
        $this->commandParts = explode(",", $command);
        $this->command = $command;

    }

    public function execute($database = null, $dataTable = "") {

        $firstOperation = strtoupper(self::cleanWhiteSpaces($this->commandParts[0]));

        /**
         * check if the first operation is accepted in our implementation.
         * if not we have to print error.
         */
        if (in_array($firstOperation, $this->acceptedOperations)) {

            /**
             * check if the operation is not add or ADD
             * to performe clean white spaces on whole command
             * because it might contain text as "SEF Instructor"
             * so we just trim to delete spaces from front and behind.
             */
            if ($firstOperation != "ADD") {
                array_map(
                    'self::cleanWhiteSpaces',
                    $this->commandParts
                );

                if ($this->expectedOperation[$firstOperation][0] != 'string') {
                    $secondOperation = $this->commandParts[1];
                    if (in_array($secondOperation,
                        $this->expectedOperation[$firstOperation])) {
                        switch ($firstOperation) {
                        case 'CREATE':
                            if ($secondOperation == 'DATABASE') {
                                $cmd = $this->command;
                                $dbName = substr(
                                    $cmd, strpos($cmd, ',"') + 2
                                );
                                $dbName = str_replace('"', '', $dbName);

                                $db = new Database($dbName);
                                $db->open();
                                return $db;

                            } elseif ($secondOperation == 'TABLE') {

                                $cmd = $this->command;
                                $tableName = substr(
                                    $cmd,
                                    strpos($cmd, ',"') + 2,
                                    strpos($cmd, '",') - strpos($cmd, ',"') - 2
                                );
                                $columns = preg_replace('/\"+/', '', substr(
                                    $cmd,
                                    strpos($cmd,
                                        '",COLUMNS,"'
                                    ) + 10,
                                    strpos($cmd, ',INTO') - strpos($cmd, ',COLUMNS') - 10
                                ));
                                $myDataBase = substr(
                                    $cmd,
                                    strpos($cmd, ',INTO,') + 7
                                );
                                $myDataBase = trim(preg_replace('/\s\s+/', ' ', $myDataBase));
                                $myDataBase = str_replace('"', '', $myDataBase);
                                if (strpos($columns, ',') > 0) {
                                    $columns = explode(',', $columns);
                                    if ($myDataBase) {
                                        if (count($columns) > 0) {
                                            $db = new Database($myDataBase);
                                            $db->open();
                                            $db->createTable($tableName, $columns);
                                        } else {
                                            echo "please specify columns names";
                                        }

                                    } else {
                                        echo "you have to pass a database before";
                                    }
                                }
                            }
                            break;
                        // when user wants to delete
                        case 'DELETE':
                            if ($secondOperation == 'DATABASE') {
                                $cmd = $this->command;
                                $dbName = substr(
                                    $cmd, strpos($cmd, ',"') + 2
                                );
                                $dbName = str_replace('"', '', $dbName);
                                $dbName = trim(preg_replace('/\s\s+/', ' ', $dbName));
                                $db = new Database($dbName);
                                $db->open();
                                $db->delete();
                            } elseif ($secondOperation == 'TABLE') {
                                $cmd = $this->command;
                                $tableName = substr(
                                    $cmd,
                                    strpos($cmd, 'TABLE') + 7,
                                    strpos($cmd, 'FROM') - strpos($cmd, 'TABLE') - 9
                                );
                                $myDbName = substr(
                                    $cmd,
                                    strpos($cmd, 'FROM') + 6
                                );
                                $myDbName = str_replace('"', '', $myDbName);
                                $myDbName = trim(preg_replace('/\s\s+/', ' ', $myDbName));
                                $tableName = trim(
                                    preg_replace('/\s\s+/', ' ', $tableName));

                                $db = new Database($myDbName);
                                $db->open();
                                echo $db->deleteTable($tableName);

                            } elseif ($secondOperation == 'ROW') {
                                $id = substr(
                                    $this->command,
                                    strpos($this->command, ',ROW,') + 6,
                                    strpos($this->command, ',FR') - strpos($this->command, 'RW,')
                                );
                                echo $id;
                                exit();

                                $db = new Database($database);
                                $db->open();
                                $db->deleteRow($dataTable, $id);
                            }
                            break;

                        }
                    } else {
                        return " unexpected variable ";
                    }
                } else {
                    switch ($firstOperation) {
                    case 'GET':
                        $data = substr(
                            $this->command,
                            strpos($this->command, ',"') + 2
                        );
                        $data = str_replace('"', '', $data);
                        $db = new Database($database);
                        $db->open();
                        var_dump($db->getRow($dataTable, $data));
                        break;
                    }
                }

            } else {
                array_map(
                    'trim',
                    $commandParts
                );
            }

        } else {
            $this->printError('commandNotFound', $firstOperation);
            return false;
        }
    }

    function getOperationName() {
        var_dump($this->command);
    }

    /**
     * function to remove all whitespaces if they are
     * at the start of the string or in between of two strings.
     * @param  String
     * @return string
     */
    private static function cleanWhiteSpaces(&$string) {
        return preg_replace("/\s+/", "", $string);
    }

    /**
     * function to print errors for user
     * @param  String
     * @return String error description
     */
    private function printError($errorDescription, $command = '') {
        switch ($errorDescription) {
        case 'commandNotFound':
            echo "command $command not found";
            break;

        default:

            break;
        }
    }
}