<?php
/**
 *
 */
class Cmd {

    /**
     * here where Cmd knows what are the accepted
     * commands and instructions.
     * @var array
     */
    private $accepted = array(
        'CREATE', 'DELETE', 'ADD', 'GET',
    );

    /**
     * Cmd read command word by word in order
     * to execute, and every command he read
     * he expect input or another command for the
     * previous command.
     * expected used to keep track of what we expect
     * the next command to be ;)
     * @var array
     */
    private $expected = array(
        'CREATE' => ['DATABASE', 'TABLE'],
        'DELETE' => ['DATABASE', 'TABLE', 'ROW'],
        'ADD' => ['STRING'],
        'TABLE' => ['STRING'],
        'COLUMNS' => ['STRING'],
        'DATABASE' => ['STRING'],
        'ROW' => ['STRING'],
        'STRING' => ['STRING', 'COLUMNS', 'eof', 'FROM', 'INTO'],
        'GET' => ['STRING'],
        'FROM' => ['STRING'],
        'INTO' => ['STRING'],
    );

    /**
     * just string to show the user how he can
     * use the engine.
     * @var string
     */
    private $help = '
|-----------CREATE NEW DATABASE--------------------------|
| CREATE,DATABASE,"DATABASE_NAME"                        |
|                                                        |
|------------CREATE NEW TABLE----------------------------|
| CREATE,TABLE,"TABLE_NAME",INTO,"DATABASE_NAME"         |
|                                                        |
|------------DELETE DATABASE-----------------------------|
| DELETE,DATABASE,"DATABASE_NAME"                        |
|                                                        |
|------------DELETE DATA TABLE---------------------------|
| DELETE,TABLE,"TABLE_NAME",FROM,"DATABASE_NAME"         |
|                                                        |
|------------ADD ROW TO DATA TABLE-----------------------|
| ADD,"ARG1","ARG2",INTO,"TABLE_NAME",FROM,"DB_NAME"     |
|                                                        |
|------------DELETE ROW FROM TABLE-----------------------|
| DELETE,ROW,"ROW_ID",FROM,"TABLE_NAME",INTO,"DB_NAME"   |
|                                                        |
|------------QUERY TABLE---------------------------------|
| GET,"QUERY",FROM,"TABLE_NAME",INTO,"DATABASE_NAME"     |
|                                                        |
|------------LIST ALL DATABASES AND Tables---------------|
| LIST DATABASES, or just use ls                         |
|________________________________________________________|
';

    /**
     * yemkin asta3milon bas ma elon 3azi :D
     * @var array
     */
    private $expectedCmdLength = array(
        'CREATEDATABASE' => 1,
        'CREATETABLE' => 5,
        'DELETEDATABASE' => 1,
        'DELETETABLE' => 3,
        'DELETEROW' => 5,
        'GET' => 5,
    );

    /**
     * data is an array holds all instructions
     * typed by the user.
     * @var array(strings)
     */
    private $data;

    /**
     * when user create the Cmd object
     * he has to feed him with the command he want
     * just push eof into it in for parsing to know
     * that he walk on all commands.
     * @param type [void] $cmd [command sent by user]
     */
    function __construct($cmd) {
        $data = Helper::getValsFromString($cmd);
        $this->data = array_filter($data);
        array_push($this->data, 'eof');
    }

    /**
     * execute took the command then start parsing it
     * but first execute check if there are any system
     * commands like help or ls.
     *
     * @return [result] result as string from engine.
     */
    function execute() {

        /**
         * if user typed help, or HELP
         * Cmd will print him the help manual.
         */
        if (strtoupper($this->data[0]) == 'HELP') {
            // send clear command to terminal.
            system('clear');

            return $this->help;
        }
        if (strtoupper($this->data[0]) == 'CLEAR') {
            system('clear');
            return '';
        }

        /**
         * if user typed LIST DATABASES, or ls
         * Cmd will bring all data tables and their
         * tables with structure.
         */
        if (strtoupper($this->data[0]) == "LIST DATABASES" ||
            strtoupper($this->data[0]) == "LS") {
            system('clear');
            $db = new Database(null);
            return $db->listDbs();
        }

        /**
         * Cmd will check if the first command is allowed
         * by checking its occurance in the accepted commands
         * array.
         */
        if (!in_array($this->data[0], $this->accepted)) {
            return "Not allowed instruction";
        }

        /**
         * this loop is to check if every command
         * and its next command are accepted
         * and also related to eachother.
         * for example CREATE only accept instruction
         * so if you typed CREATE,"ABC" ; "ABC" is a string
         * and create expect instruction. so it will return :
         * not allowed instruction and give the user a hint
         * to let him know what are the allowed instructions
         * @var integer
         */
        for ($i = 0; $i < count($this->data); $i++) {

            if (count($this->data) <= 2) {
                return "command error";
            }
            // the next instruction.
            $cmd = $this->data[$i + 1];

            // first or previous instruction.
            $cmdPrev = $this->data[$i];

            // check if instruction is a string.
            if (strpos($cmdPrev, '"') !== false) {
                $cmdPrev = 'STRING';
            }
            if (strpos($cmd, '"') !== false) {
                $cmd = 'STRING';
            }

            // if we reach the last instruction so exit the loop.
            if (trim($cmd) == 'eof') {
                break;
            }

            // put the expected instructions in array.
            $expected = $this->expected[$cmdPrev];

            /**
             * here we check if the next instruction
             * is accepted by the first one.
             */
            if (!$this->inArray($cmd, $expected)) {
                return "not allowed instruction $cmd try " .
                implode(' or ', $expected);
            }

        }

        /**
         * Cmd now know if the instructions are ok
         * and every command has an accepted template
         * or form.
         * now the time to execute the instructions.
         */
        switch ($this->data[0]) {
        /**
             * if first instruction is GET
             * so i expect 3 values from GET.
             * now we know it is in correct format
             * but we have to check if the data is sent correctly
             * and there is no missing arguments.
             * GET, FROM and INTO accept string after them..
             * I used my method to get the string after instruction.
             * getInstruction('instructionName',instructions)...
             * for more info go to the description of the function
             */
        case 'GET':

            $get = $this->getInstruction('GET', $this->data);
            $frm = $this->getInstruction('FROM', $this->data);
            $into = $this->getInstruction('INTO', $this->data);
            // if all data is sent, we execute the command.
            // else we return missing arguments.
            if (count($get) > 0 &&
                count($frm) > 0 &&
                count($into) > 0) {
                $db = new Database($into[0]);
                $result = $db->getRow($frm[0], $get[0]);
                $result = implode('', $result);
                if ($result == '') {
                    $result = "Not found";
                }
                return $result;
            } else {
                return "missing arguments...\n" .
                    'try: "GET,"ID",FROM,"TABLE",INTO,"DATABASE"';
            }
            break;

        /**
             * create not like GET,
             * because its expect 2 values.
             * DATABASE, OR TABLE.
             */
        case 'CREATE':
            // if the next instruction is DATABASE
            if ($this->data[1] == 'DATABASE') {
                // get database name
                $dbName = $this->getInstruction(
                    'DATABASE', $this->data
                );
                if (count($dbName) > 0) {
                    $db = new Database($dbName[0]);
                    return $db->createDb();
                    //return "$dbName[0] CREATED";
                } else {
                    return "missing arguements\n" .
                        'try: CREATE,DATABASE,"DATABASE_NAME"';
                }
            } elseif ($this->data[1] == 'TABLE') {
                // get table name.
                $tableName = $this->getInstruction(
                    'TABLE', $this->data);
                // get columns names.
                $columns = $this->getInstruction(
                    'COLUMNS', $this->data
                );
                // get database name.
                $dbName = $this->getInstruction(
                    'INTO', $this->data
                );
                // check if there is no missing arguments.
                if (count($tableName) > 0 &&
                    count($columns) > 0 &&
                    count($dbName) > 0) {
                    $db = new Database($dbName[0]);
                    return $db->createTable($tableName[0], $columns);
                    //return "$tableName[0] CREATED";
                } else {
                    return "missing arguments\n" .
                        'try: CREATE,TABLE,"TABLE_NAME",COLUMNS,"C1","C2","C3",INTO,"DATABASE_NAME"';
                }
            }
            break;

        /**
             * delete accept 3 type of instructions after it
             * delete database, table, row.
             */
        case 'DELETE':
            if ($this->data[1] == 'DATABASE') {
                $dbName = $this->getInstruction(
                    'DATABASE', $this->data
                );

                if (count($dbName) > 0) {
                    $db = new Database($dbName[0]);
                    return $db->delete();
                    //return "$dbName[0] Deleted";
                } else {
                    return "missing arguements\n" .
                        'try: DELETE,DATABASE,"DATABASE_NAME"';
                }
            } elseif ($this->data[1] == 'TABLE') {
                $tableName = $this->getInstruction(
                    'TABLE', $this->data);
                $dbName = $this->getInstruction(
                    'FROM', $this->data
                );
                if (count($tableName) > 0 &&
                    count($dbName) > 0) {
                    $db = new Database($dbName[0]);
                    $db->deleteTable($tableName[0]);
                    return "delete table";
                } else {
                    return "missing arguements\n" .
                        'try: DELETE,TABLE,"TABLE_NAME",FROM,"DATABASE_NAME"';
                }
            } elseif ($this->data[1] == 'ROW') {
                $id = $this->getInstruction(
                    'ROW', $this->data
                );
                $from = $this->getInstruction(
                    'FROM', $this->data
                );
                $dbName = $this->getInstruction(
                    'INTO', $this->data
                );
                if (count($id) > 0 &&
                    count($from) > 0) {
                    $db = new Database($dbName[0]);
                    $db->deleteRow($from[0], $id[0]);
                    return "row deleted";
                } else {
                    return "missing arguements\n" .
                        'try: DELETE,ROW,"ID",FROM,"TABLE_NAME",INTO,"DATABASE_NAME"';
                }
            }
            break;
        case 'ADD':
            $data = $this->getInstruction('ADD', $this->data);
            $table = $this->getInstruction('INTO', $this->data);
            $dbName = $this->getInstruction('FROM', $this->data);
            if (count($data) > 0 &&
                count($table) > 0 &&
                count($dbName) > 0) {
                $db = new Database($dbName[0]);
                return $db->addRow($table[0], $data);
                //return implode(',', $data) . " added";
            } else {
                return "missing arguments\n" .
                    'try: ADD,"ARG1","ARG2","ARG3",INTO,"TABLE_NAME",FROM,"DATABASE_NAME"';
            }
            break;
        default:

            break;
        }
    }

    /**
     * check if the string is in an array
     * I puted it in a function only
     * for typing reasons...
     */
    private function inArray($data, $array) {
        foreach ($array as $value) {
            //echo $data . $value;
            if ((string) trim($data) == (string) trim($value)) {
                return true;
            }
        }
        return false;
    }

    /**
     * get instruction loop through all instruction
     * and when it find the instruction we gave to it
     * it keep track of all non instruction items after it
     * till it reach one, so it return our data in an array.
     * adf
     * @param  string $instruction the instruction we want
     * @param  array $cmd         all instructions sent
     * @return array              return all instructions as an array
     */
    private function getInstruction($instruction, $cmd) {
        $result = array();
        for ($i = 0; $i < count($cmd); $i++) {
            $tmpRes = array();
            // check if the instruction send is not string.
            if ($this->isInstruction($cmd[$i]) &&
                $cmd[$i] === $instruction) {

                for ($j = $i + 1; $j < count($cmd); $j++) {
                    if ($this->isInstruction($cmd[$j])) {
                        break;
                    }
                    array_push($tmpRes, Helper::removeQoutFromArray($cmd[$j]));
                }
                return ($tmpRes);
            }

        }
        //return array($instruction => $result);
    }

    /**
     * isInstruction used to check if the command word is
     * an instruction or just a string.
     * the idea is to find if its starts or ends with
     * a double qout.
     * @param  string  $instruction instruction to check
     * @return boolean              if its a string will return false.
     */
    private function isInstruction($instruction) {
        if (strpos($instruction, '"') !== false) {
            return false;
        }
        return true;
    }

}