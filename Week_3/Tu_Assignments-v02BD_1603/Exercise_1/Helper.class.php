<?php
/**
 * helper is a class of stetic methods
 * we will use them along the project.
 */
class Helper {

    /**
     * check if file is exists then delete it.
     */
    public static function deleteFile($fileName) {
        if (file_exists($fileName)) {
            unlink($fileName);
        }
    }

    public static function getValsFromString($str) {
        //$res = split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)", $str);
        $res = preg_split('~(?:\'[^\']*\'|"[^"]*"|)\K(,|$)~', $str);
        for ($i = 0; $i < count($res); $i++) {
            Helper::cleanWhiteSpaces($res[$i]);
        }
        return $res;
    }

    public static function removeQout($str) {
        return str_replace('"', '', $str);
    }

    public static function removeQoutFromArray($arr) {
        if (!is_array($arr)) {
            return str_replace('"', '', $arr);
        }
        $array = array();
        foreach ($arr as $ar) {
            array_push($array, str_replace('"', '', $ar));
        }
        return $array;
    }
    public static function cleanWhiteSpaces(&$str) {
        $str = trim(preg_replace('/\s+/', ' ', $str));
    }

    public static function getDirContant($dir_path) {
        // Test if dir_path is not empty and the path does not ends with dot '.'
        if (isset($dir_path) && substr($dir_path, -1) != ".") {
            if (is_dir($dir_path)) {
                $directory = opendir($dir_path); // initialize a directory handle to read it.
                while (($f = readdir($directory))) {
                    // read handled directory content.
                    if (is_dir($dir_path) &&
                        substr($f, -1) != ".") {
                        echo " >  " . $f . "\n";
                        $dirs = scandir($dir_path . '/' . $f);
                        foreach ($dirs as $dir) {
                            if (substr($dir, -1) != ".") {
                                echo "   |__" . $dir;
                                $fileContent = file(
                                    $dir_path . '/' . $f . '/' . $dir . '/' . $dir
                                );
                                echo "\n   | |__ " . $fileContent[1] . "\n";
                            }

                        }
                    }

                }
            }
        }
    }

    public static function isNotNull($array) {
        foreach ($array as $item) {
            if (trim($item) === '') {
                return false;
            }
        }
        return true;
    }

}